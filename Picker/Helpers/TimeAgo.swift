
import Foundation


public func timeAgoSince(_ date: Date) -> String {
    
    let calendar = Calendar.current
    let now = Date()
    let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
    let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
    
    if let year = components.year, year >= 2 {
        return String(year)  + " " + "years".localized()
    }
    
    if let year = components.year, year >= 1 {
        return "1 year".localized()
    }
    
    if let month = components.month, month >= 2 {
        return String(month) + " " +  "months".localized()
    }
    
    if let month = components.month, month >= 1 {
        return "1 month".localized()
    }
    
    if let week = components.weekOfYear, week >= 2 {
        return String(week) + " " + "weeks".localized()
    }
    
    if let week = components.weekOfYear, week >= 1 {
        return "1 week".localized()
    }
    
    if let day = components.day, day >= 2 {
        return String(day) + " " + "days".localized()
    }
    
    if let day = components.day, day >= 1 {
        return "1 day".localized()
    }
    
    if let hour = components.hour, hour >= 2 {
        return String(hour) + " " + "hours".localized()
    }
    
    if let hour = components.hour, hour >= 1 {
        return "1 hour".localized()
    }
    
    if let minute = components.minute, minute >= 2 {
        return String(minute) + " " + "min".localized()
    }
    
    if let minute = components.minute, minute >= 1 {
        return "1 min".localized()
    }
    
    if let second = components.second, second >= 3 {
        return String(second) + " " + "seconds".localized()
    }
    
    return "Just now".localized()
    
}

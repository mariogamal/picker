//
//  VideoPlayer.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/31/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit



class VideoPlay: UIView {
    
    private var player : AVPlayer!
    private var playerObserver: Any?
    private var playerLayer : AVPlayerLayer!
    
    init() {
        super.init(frame: .zero)
        initializePlayerLayer()
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializePlayerLayer()
        self.autoresizesSubviews = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializePlayerLayer()
        
    }
    
    deinit {
        guard let observer = playerObserver else { return }
        NotificationCenter.default.removeObserver(observer)
    }
    
    
    private func initializePlayerLayer() {
        
        playerLayer = AVPlayerLayer()
        playerLayer.backgroundColor = UIColor.black.cgColor
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.layer.addSublayer(playerLayer)
        playerLayer.frame = self.bounds
    }
    
    func playVideoWithURL(url: URL) {
        
        NotificationCenter.default.removeObserver( NSNotification.Name.AVPlayerItemDidPlayToEndTime)
        
        player = AVPlayer(url: url)
        player.isMuted = false
        playerLayer.player = player
        
        player.play()
        
        loopVideo(videoPlayer: player)
    }
    
    func toggleMute() {
        player.isMuted = !player.isMuted
    }
    
    func isMuted() -> Bool
    {
        return player.isMuted
    }
    
    func loopVideo(videoPlayer: AVPlayer) {
        
        playerObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { notification in
            videoPlayer.seek(to: CMTime.zero)
            videoPlayer.play()
        }
        
    }
    
    func pause() {
        player.pause()
    }
    
}

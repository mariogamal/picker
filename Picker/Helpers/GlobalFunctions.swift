//
//  GlobalFunctions.swift
//  Picker
//
//  Created by Vavisa Mac on 10/22/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit

class GlobalFunctions{
    
    static func updateFollowButtons(profile_user_id: Int, tabBarController: UITabBarController?){
        
        if let appTabBar = tabBarController as? appTabBarController{
            if let notificationNav = appTabBar.viewControllers![3] as? UINavigationController{
                if let notificationVC = notificationNav.viewControllers.first as? NotificationViewController{
                    if notificationVC.notifications.count > 0{
                        for notification in notificationVC.notifications{
                            if notification.type == "follow"{
                                // Update notification follow button if user follow from profile
                                if notification.user_id == profile_user_id{
                                    
                                    let row = notificationVC.notifications.index { (n) -> Bool in
                                        n.user_id == profile_user_id && n.type == "follow"
                                    }
                                    if let row = row{
                                        if notificationVC.didLoad{
                                            if let cell = notificationVC.tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? NotificationFollowTableViewCell{
                                               
                                                // Remove indicator fron superview
                                                if let indicator =  cell.followBtn.subviews.last as? UIActivityIndicatorView{
                                                    indicator.removeFromSuperview()
                                                }
                                                
                                                if cell.followBtn.titleLabel?.text == "Follow".localized(){
                                                    cell.followBtn.backgroundColor = AppColors.shared.red
                                                    cell.followBtn.setTitle("Unfollow".localized(), for: .normal)
                                                }else{
                                                    cell.followBtn.backgroundColor = AppColors.shared.blue
                                                    cell.followBtn.setTitle("Follow".localized(), for: .normal)
                                                }
                                            }
                                        }
                                        
        
                                    }
                                    
                                }
                            }
                        }
                        
                    }
                }
                
            }
            
            if let searchNav = appTabBar.viewControllers![0] as? UINavigationController{
                if let searchVC = searchNav.viewControllers.first as? SearchViewController{
                    if searchVC.users.count > 0{
                        
                        let index = searchVC.users.index { (u) -> Bool in
                            return u.id == profile_user_id
                        }
                    
                        if let index = index{
                            if let userCell = searchVC.usersCollectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? SearchUserCollectionViewCell{
                                // Remove indicator fron superview
                                if let indicator =  userCell.followBtn.subviews.last as? UIActivityIndicatorView{
                                    indicator.removeFromSuperview()
                                }
                                
                                
                                if userCell.followBtn.titleLabel?.text == "Follow".localized(){
                                    userCell.followBtn.backgroundColor = AppColors.shared.red
                                    userCell.followBtn.setTitle("Unfollow".localized(), for: .normal)
                                }else{
                                    userCell.followBtn.backgroundColor = AppColors.shared.blue
                                    userCell.followBtn.setTitle("Follow".localized(), for: .normal)
                                }
                            }
                        }
                        
                    }
                }
                
            }
            
            // Change profile follow text in all views EXECPT user profile
            for i in 0...3{
                if let controller = appTabBar.viewControllers?[i]{
                    if let nav = controller as? UINavigationController{
                        if let profileVC = nav.viewControllers.last as? ProfileViewController{
                            
                            // Remove indicator fron superview
                            if let indicator =  profileVC.editFollowBtn.subviews.last as? UIActivityIndicatorView{
                                indicator.removeFromSuperview()
                            }
                            
                            if profileVC.editFollowBtn.titleLabel?.text == "Follow".localized(){
                                profileVC.editFollowBtn.setTitle("Unfollow".localized(), for: .normal)
                            }else if profileVC.editFollowBtn.titleLabel?.text == "Unfollow".localized(){
                                profileVC.editFollowBtn.setTitle("Follow".localized(), for: .normal)
                            }
                        }
                    }
                }
            }
            
            
            
            // Update user profile so following and followers counter will be updated
            if let profileNav = appTabBar.viewControllers![4] as? UINavigationController{
                if let profileVC = profileNav.viewControllers.first as? ProfileViewController{
                    profileVC.handleRefresh()
                }
                
                // Follow list update
                if let followListVC = profileNav.viewControllers.last as? FollowListTableViewController{
                    let index = followListVC.followers.index { (u) -> Bool in
                        return u.id == profile_user_id
                    }
                    
                    if let index = index{
                        if let cell = followListVC.listTableView.cellForRow(at: IndexPath(row: index, section: 0)) as? FollowListTableViewCell{
                           
                            // Remove indicator fron superview
                            if let indicator =  cell.followBtn.subviews.last as? UIActivityIndicatorView{
                                indicator.removeFromSuperview()
                            }
                            
                            
                            if cell.followBtn.titleLabel?.text == "Follow".localized(){
                                cell.followBtn.backgroundColor = AppColors.shared.red
                                cell.followBtn.setTitle("Unfollow".localized(), for: .normal)
                            }else{
                                cell.followBtn.backgroundColor = AppColors.shared.blue
                                cell.followBtn.setTitle("Follow".localized(), for: .normal)
                            }
                        }
                    }
                }
            }
            
            
        }
    }
    
    
    static func updatePicked(profile_user_id: Int, tabBarController: UITabBarController?){
        
        if let appTabBar = tabBarController as? appTabBarController{
            if let profileNav = appTabBar.viewControllers![4] as? UINavigationController{
                if let profileVC = profileNav.viewControllers.first as? ProfileViewController{
                    profileVC.handleRefresh()
                }
            }
            
            if let homeNav = appTabBar.viewControllers![1] as? UINavigationController{
                if let homeVC = homeNav.viewControllers.first as? HomeViewController{
                    if homeVC.didAppears{
                        homeVC.handleRefresh()
                    }
                }
            }
        }
    }
    
    static func updateTimelinePicked(profile_user_id: Int, tabBarController: UITabBarController?){
        
        if let appTabBar = tabBarController as? appTabBarController{
            // Update Followers count
//            if let profileNav = appTabBar.viewControllers![4] as? UINavigationController{
//                if let profileVC = profileNav.viewControllers.first as? ProfileViewController{
//                    profileVC.handleRefresh()
//                }
//            }
            
            if let homeNav = appTabBar.viewControllers![1] as? UINavigationController{
                if let homeVC = homeNav.viewControllers.first as? HomeViewController{
                    if homeVC.didAppears{
                        homeVC.handleRefresh()
                    }
                }
            }
        }
    }
    
    static func updateUserProfile(profile_user_id: Int, tabBarController: UITabBarController?){
        
        if let appTabBar = tabBarController as? appTabBarController{
             //Update Followers count
            if let profileNav = appTabBar.viewControllers![4] as? UINavigationController{
                if let profileVC = profileNav.viewControllers.first as? ProfileViewController{
                    profileVC.handleRefresh()
                }
            }
        
        }
    }
    
    
    static func addIndicatorToFollowBtn(followBtn: UIButton){
        let followIndicator = UIActivityIndicatorView()
        followIndicator.style = .white
        followIndicator.startAnimating()
        followBtn.addSubview(followIndicator)
        
        followIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            followIndicator.leadingAnchor.constraint(equalTo: followBtn.leadingAnchor),
            followIndicator.trailingAnchor.constraint(equalTo: followBtn.trailingAnchor),
            followIndicator.topAnchor.constraint(equalTo: followBtn.topAnchor),
            followIndicator.bottomAnchor.constraint(equalTo: followBtn.bottomAnchor)
            ])
    }
    
    
    static func addIndicatorToAllFollowButtons(profile_user_id: Int, tabBarController: UITabBarController?){
        if let appTabBar = tabBarController as? appTabBarController{
            if let notificationNav = appTabBar.viewControllers![3] as? UINavigationController{
                if let notificationVC = notificationNav.viewControllers.first as? NotificationViewController{
                    if notificationVC.notifications.count > 0{
                        for notification in notificationVC.notifications{
                            if notification.type == "follow"{
                                // Update notification follow button if user follow from profile
                                if notification.user_id == profile_user_id{
                                    
                                    let row = notificationVC.notifications.index { (n) -> Bool in
                                        n.user_id == profile_user_id && n.type == "follow"
                                    }
                                    if let row = row{
                                        if notificationVC.didLoad{
                                            if let cell = notificationVC.tableView.cellForRow(at: IndexPath(row: row, section: 0)) as? NotificationFollowTableViewCell{
                                                addIndicatorToFollowBtn(followBtn: cell.followBtn)
                                            }
                                        }
                                        
                                    }
                                    
                                }
                            }
                        }
                        
                    }
                }
                
            }
            
            if let searchNav = appTabBar.viewControllers![0] as? UINavigationController{
                if let searchVC = searchNav.viewControllers.first as? SearchViewController{
                    if searchVC.users.count > 0{
                        
                        let index = searchVC.users.index { (u) -> Bool in
                            return u.id == profile_user_id
                        }
                        
                        if let index = index{
                            if let cell = searchVC.usersCollectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? SearchUserCollectionViewCell{
                                addIndicatorToFollowBtn(followBtn: cell.followBtn)
                            }
                        }
                        
                    }
                }
                
            }
            
            // Change profile follow text in all views EXECPT user profile
            for i in 0...3{
                if let controller = appTabBar.viewControllers?[i]{
                    if let nav = controller as? UINavigationController{
                        if let profileVC = nav.viewControllers.last as? ProfileViewController{
                            addIndicatorToFollowBtn(followBtn: profileVC.editFollowBtn)
                        }
                    }
                }
            }
            
            // Update user profile so following and followers counter will be updated
            if let profileNav = appTabBar.viewControllers![4] as? UINavigationController{
                if let profileVC = profileNav.viewControllers.first as? ProfileViewController{
                    profileVC.handleRefresh()
                }
                
                
                // Follow list update
                if let followListVC = profileNav.viewControllers.last as? FollowListTableViewController{
                    let index = followListVC.followers.index { (u) -> Bool in
                        return u.id == profile_user_id
                    }
                    
                    if let index = index{
                        if let cell = followListVC.listTableView.cellForRow(at: IndexPath(row: index, section: 0)) as? FollowListTableViewCell{
                            addIndicatorToFollowBtn(followBtn: cell.followBtn)
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    static func RefreshHomeAndNotification(tabBarController: UITabBarController?){
        
        if let appTabBar = tabBarController as? appTabBarController{
            if let notificationNav = appTabBar.viewControllers![3] as? UINavigationController{
                if let notificationVC = notificationNav.viewControllers.first as? NotificationViewController{
                    if notificationVC.didLoad{
                        notificationVC.handleRefresh()
                    }
                }
                
            }
           
            if let homeNav = appTabBar.viewControllers![1] as? UINavigationController{
                if let homeVC = homeNav.viewControllers.first as? HomeViewController{
                    if homeVC.didAppears{
                        homeVC.handleRefresh()
                    }
                }
            }
            
        }
    }
    
    
    
    
    
    
    
}

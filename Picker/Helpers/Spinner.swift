//
//  Spinner.swift
//  JasmineStore
//
//  Created by omar mohammed on 10/19/18.
//  Copyright © 2018 omar mohammed. All rights reserved.
//

import UIKit

class Spinner {
    
    static var shared = Spinner()
    
    let blackView:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return v
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        i.startAnimating()
        i.style = .white
        return i
    }()
    
    func startAnimating(){
        if let window = UIApplication.shared.keyWindow{
            window.addSubview(blackView)
            blackView.translatesAutoresizingMaskIntoConstraints = false
            blackView.leadingAnchor.constraint(equalTo: window.leadingAnchor).isActive = true
            blackView.trailingAnchor.constraint(equalTo: window.trailingAnchor).isActive = true
            blackView.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
            blackView.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
            
            blackView.addSubview(activityIndicator)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.centerXAnchor.constraint(equalTo: blackView.centerXAnchor).isActive = true
            activityIndicator.centerYAnchor.constraint(equalTo: blackView.centerYAnchor).isActive = true
            
            blackView.alpha = 0
            UIView.animate(withDuration: 0.2) {
                self.blackView.alpha = 1
            }
        }
    }
    
    func stopAnimating(){
        blackView.removeFromSuperview()
    }
}

//
//  Extentions.swift
//  Ostathi
//
//  Created by Vavisa - iMac 2 on 9/28/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SwiftMessages
import AVFoundation

extension UIView {
    
    func dropShadow() {
        
        self.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 4
        
    }
    
    func dropDarkShadow() {
        
        self.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 2
        
    }
    
    func dropShadowLow() {
        
        self.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
    }
    func removeShadow() {
        self.layer.shadowColor = UIColor.clear.cgColor
    }
}

extension UITextField {
    
    /// set icon of 20x20 with left padding of 8px
    func setLeftIcon(_ icon: UIImage) {
        
        let padding = 20
        let size = 25
        
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
        let iconView  = UIImageView(frame: CGRect(x: padding/2, y: 0, width: size, height: size))
        iconView.image = icon
        outerView.addSubview(iconView)
        
        leftView = outerView
        leftView?.tintColor = AppColors.shared.lightGray
        leftViewMode = .always
    }
}

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}


extension UIResponder {
    
    func next<T: UIResponder>(_ type: T.Type) -> T? {
        return next as? T ?? next?.next(type)
    }
}


extension UITableViewCell {
    var tableView: UITableView? {
        return next(UITableView.self)
    }
    
    var indexPath: IndexPath? {
        return tableView?.indexPath(for: self)
    }
}

extension UICollectionViewCell {
    var collectionView: UICollectionView? {
        return next(UICollectionView.self)
    }
    
    var indexPath: IndexPath? {
        return collectionView?.indexPath(for: self)
    }
}

extension UIViewController{

    func showSaved(){
        
        let savedLabel = UILabel()
        savedLabel.text = "Saved".localized()
        savedLabel.backgroundColor = .white
        savedLabel.textColor = AppColors.shared.darkGray
        savedLabel.font = UIFont(name: "Raleway-Medium", size: 16)
        savedLabel.textAlignment = .center
        savedLabel.clipsToBounds = true

       
        view.addSubview(savedLabel)
        savedLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(100)
            make.height.equalTo(50)
        }
        savedLabel.layer.cornerRadius = 12
        savedLabel.alpha = 0
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            savedLabel.alpha = 1
        }, completion: nil)
        
       
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (timer) in
            savedLabel.alpha = 0
        }
        
    }
    
    
    
    
    func showMessage(body:String){
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.success)
        view.bodyLabel?.font = UIFont(name: "Raleway-Medium", size: 17)
        view.titleLabel?.isHidden = true
        view.iconImageView?.isHidden = true
        view.button?.isHidden = true
        view.configureContent(body: body)

        // Show the message.
        var config = SwiftMessages.Config()

        // Slide up from the bottom.
        config.presentationStyle = .bottom

        // displays over the status bar while UIWindowLevelNormal displays under.
        //config = .window(windowLevel: UIWindowLevelStatusBar)


        SwiftMessages.show(config: config, view: view)
    }
    
    func showMessageTop(body:String){
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureTheme(.error)
        view.bodyLabel?.font = UIFont(name: "Raleway-Medium", size: 15)
        view.titleLabel?.isHidden = true
        view.iconImageView?.isHidden = true
        view.button?.isHidden = true
        view.configureContent(body: body)
        
        // Show the message.
        var config = SwiftMessages.Config()
        
        // Slide up from the bottom.
        config.presentationStyle = .top
        
  
        SwiftMessages.show(config: config, view: view)
    }
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
            
        }
        
    }
    
    func randomAlphaNumericString(length: Int) -> String {
        let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let allowedCharsCount = UInt32(allowedChars.count)
        var randomString = ""
        
        for _ in 0..<length {
        let randomNum = Int(arc4random_uniform(allowedCharsCount))
        let randomIndex = allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)
        let newCharacter = allowedChars[randomIndex]
        randomString += String(newCharacter)
        }
        
        return randomString
    }
    
    
}









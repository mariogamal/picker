//
//  HeroIdFactory.swift
//  Picker
//
//  Created by Omar mohammed on 7/23/19.
//  Copyright © 2019 Omar basaleh. All rights reserved.
//

import Foundation

struct HeroIdFactory{
    static var id = 0
    
    static func generateNewId(){
        HeroIdFactory.id += 1
    }
}

//
//  Constants.swift
//  NetworkingLayer
//
//  Created by Vavisa - iMac 2 on 4/8/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct K {
    struct ProductionServer {
//        static let baseURL = "http://192.168.1.13:8000/api"
        
        static let baseURL = "http://picker.vavisa-kw.com/api"
       
    }
    
    struct APIParameterKey {
        static let password = "password"
        static let email = "email"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
    case authentication = "Basic YXBpQEVuZ2luZWVyczpBUElARW5naW5lZXJzXzEyMzY1NA=="
}

//
//  APIClient.swift
//  NetworkingLayer
//
//  Created by Vavisa - iMac 2 on 4/8/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import Alamofire



class APIClient {
    
    static func register(name:String, username: String,email:String, password: String ,profile_image: String, date_of_birth: String, token: String, completion:@escaping (Int, String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(0, "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.register(name: name, username: username, email: email, password: password, profile_image: profile_image, date_of_birth: date_of_birth, token: token)).responseJSON { (response) in
            
            print(response)
            
            
            
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion(0, "Missing required data".localized())
                    case -2:
                        completion(0, "Email registered before".localized())
                    case -3:
                        completion(0, "Username registered before".localized())
                    case -4:
                        completion(0, "Email is not valid".localized())
                    default:
                        completion(err,nil)

                    }
                }
                
            }else{
                completion(0, "Error occurred".localized())
            }
        }
    }
    
    
    static func login(email: String, password: String, token:String, completion:@escaping (User, String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(User(), "No Internet connection".localized())
            return
        }
        
        
        
        Alamofire.request(APIRouter.login(email: email, password: password, token: token)).responseJSON { (response) in
            
            
            print(response)
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    if err == -1{
                        completion(User() , "Email or password are not correct".localized())
                    }
                }else{
                    do{
                        let user = try JSONDecoder().decode(User.self, from: response.data!)
                        completion(user, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                }
            }else{
                completion(User(), "error")
            }
        }
    }
    
    
    static func logout(token: String, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.logout(token: token)).responseJSON { (response) in
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Token Not found".localized())
                    default:
                        completion(nil)
                        
                    }
                }
                
            }else{
                completion( "error")
            }
        }
    }
    
    
    static func changePassword(user_id: Int, old_password: String, new_password:String, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.changePassword(user_id: user_id, old_password: old_password, new_password: new_password)).responseJSON { (response) in
            
            print(response)
            
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("User not found".localized())
                    case -3:
                        completion("Old password is incorrect".localized())
                    default:
                        completion(nil)
                        
                    }
                }
                
            }else{
                completion( "error")
            }
        }
    }
    
    
    static func forgotPassword(email: String, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.forgotPassword(email: email)).responseJSON { (response) in
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("User not found".localized())
                    default:
                        completion(nil)
                        
                    }
                }
                
            }else{
                completion( "error")
            }
        }
    }
    
    
    static func uploadPicked(userId: Int, photo: String,thumb:String, is_video: String, location: String, tag: String,caption:String, comments_are_public: Bool,mentioned_usernames:[String],  completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        
        Alamofire.request(APIRouter.uploadPicked(userId: userId, photo: photo, thumb:thumb, is_video: is_video, location: location, tag: tag,caption: caption, comments_are_public: comments_are_public, mentioned_usernames: mentioned_usernames)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("User not found".localized())
                    default:
                        completion(nil)
                    }
                }
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    static func savePickedGroup(picked_group_id: Int, completion:@escaping ( String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion( "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.savePickedGroup(picked_group_id: picked_group_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Picked group not found".localized())
                    default:
                        completion(nil)
                        
                    }
                }
                
            }else{
                completion("Error occurred".localized())
            }
        }
    }
    
    
    static func addTag(user_id: Int, name: String, location: String, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.addTag(user_id: user_id, name: name, location: location)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("User not found".localized())
                    default:
                        completion(nil)
                    }
                }
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    static func getUserTags(user_id: Int, location: String, completion:@escaping (UserTags, String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(UserTags(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getUserTags(user_id: user_id, location: location)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion(UserTags(), "Missing required data".localized())
                    case -2:
                        completion(UserTags(), "User not found".localized())
                    default:
                        completion(UserTags(), "Error")
                    }
                }else{
                    
                    do{
                        let userTags = try JSONDecoder().decode(UserTags.self, from: response.data!)
                        completion(userTags, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion(UserTags(), "error".localized())
            }
        }
    }
    
    
    static func getUserPicked(user_id: Int, target_user_id: Int, completion:@escaping ([PickedGroup], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([PickedGroup](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getUserPicked(user_id: user_id, target_user_id: target_user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([PickedGroup](), "Missing required data".localized())
                    case -2:
                        completion([PickedGroup](), "User not found".localized())
                    default:
                        completion([PickedGroup](), "Error")
                    }
                }else{
                    
                    do{
                        let pickedGroups = try JSONDecoder().decode([PickedGroup].self, from: response.data!)
                        completion(pickedGroups, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([PickedGroup](), "error".localized())
            }
        }
    }
    
    static func getPicked(user_id: Int, picked_id: Int, completion:@escaping (PickedGroup, String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(PickedGroup(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getPicked(user_id: user_id, picked_id: picked_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion(PickedGroup(), "Missing required data".localized())
                    case -2:
                        completion(PickedGroup(), "Picked not found".localized())
                    default:
                        completion(PickedGroup(), "Error")
                    }
                }else{
                    
                    do{
                        let picked_group = try JSONDecoder().decode(PickedGroup.self, from: response.data!)
                        completion(picked_group, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion(PickedGroup(), "error".localized())
            }
        }
    }
    
    static func getPickedGroup(user_id: Int, picked_group_id: Int, completion:@escaping (PickedGroup, String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(PickedGroup(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getPickedGroup(user_id: user_id, picked_group_id: picked_group_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion(PickedGroup(), "Missing required data".localized())
                    case -2:
                        completion(PickedGroup(), "Picked not found".localized())
                    default:
                        completion(PickedGroup(), "Error")
                    }
                }else{
                    
                    do{
                        let pickedGroup = try JSONDecoder().decode(PickedGroup.self, from: response.data!)
                        completion(pickedGroup, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion(PickedGroup(), "error".localized())
            }
        }
    }
    
    
    
    
    
    static func getUserTimeline(user_id: Int, completion:@escaping ([Timeline], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([Timeline](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getUserTimeline(user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([Timeline](), "Missing required data".localized())
                    case -2:
                        completion([Timeline](), "User not found".localized())
                    default:
                        completion([Timeline](), "Error")
                    }
                }else{
                    
                    do{
                        let timeline = try JSONDecoder().decode([Timeline].self, from: response.data!)
                        completion(timeline, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([Timeline](), "error".localized())
            }
        }
    }
    
    
    static func getUserNotifications(user_id: Int, completion:@escaping ([NotificationStruct], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([NotificationStruct](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getUserNotifications(user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([NotificationStruct](), "Missing required data".localized())
                    default:
                        completion([NotificationStruct](), "Error")
                    }
                }else{
                    
                    do{
                        let notifications = try JSONDecoder().decode([NotificationStruct].self, from: response.data!)
                        completion(notifications, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([NotificationStruct](), "error".localized())
            }
        }
    }
    
    static func deleteNotification(notification_id: Int, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.deleteNotification(notification_id: notification_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Notification not found".localized())
                    default:
                        completion(nil)
                    }
                }
                    
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    static func seeNotification(user_id: Int, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.seeNotifications(user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("User not found".localized())
                    default:
                        completion(nil)
                    }
                }
                
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    static func tapNotification(notification_id: Int, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.tapNotification(notification_id: notification_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    default:
                        completion(nil)
                    }
                }
                
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    
    
    static func deletePicked(picked_id: Int, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.deletePicked(picked_id: picked_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Picked not found".localized())
                    default:
                        completion(nil)
                    }
                }
                
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    
    
    
    static func getUserProfile(user_id: Int, target_user_id: Int, completion:@escaping (UserProfile, String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(UserProfile(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getUserProfile(user_id: user_id, target_user_id: target_user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion(UserProfile(), "Missing required data".localized())
                    case -2:
                        completion(UserProfile(), "User not found".localized())
                    default:
                        completion(UserProfile(), "Error")
                    }
                }else{
                    
                    do{
                        let userProfile = try JSONDecoder().decode(UserProfile.self, from: response.data!)
                        completion(userProfile, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion(UserProfile(), "error".localized())
            }
        }
    }
    
    
    static func getUserIdFromUsername(username: String, completion:@escaping (Int , String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(0, "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getUserIdFromUsername(username: username)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion(0, "Missing required data".localized())
                    case -2:
                        completion(0, "User not found".localized())
                    default:
                        completion(err, nil)
                    }
                }
            }else{
                completion(0, "error".localized())
            }
        }
    }
    
    
    static func updateUserProfile(user_id: Int, username: String, name: String, date_of_birth: String, bio: String, profile_image: String, cover_image: String,  completion:@escaping (UserProfile, String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(UserProfile(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.updateUserProfile(user_id: user_id, username: username, name: name, date_of_birth: date_of_birth, bio:bio, profile_image: profile_image, cover_image: cover_image)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion(UserProfile(), "Missing required data".localized())
                    case -2:
                        completion(UserProfile(), "Username registered before".localized())
                    case -3:
                        completion(UserProfile(), "User not found".localized())
                    default:
                        completion(UserProfile(),"Error".localized())
                        
                    }
                }else{
                    
                    do{
                        let userProfile = try JSONDecoder().decode(UserProfile.self, from: response.data!)
                        completion(userProfile, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                }
                
            }else{
                completion(UserProfile(), "Error occurred".localized())
            }
        }
    }
    
    
    static func getFollowing(user_id: Int, completion:@escaping ([Follower], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([Follower](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getFollowing(user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([Follower](), "Missing required data".localized())
                    default:
                        completion([Follower](), "Error")
                    }
                }else{
                    
                    do{
                        let followings = try JSONDecoder().decode([Follower].self, from: response.data!)
                        completion(followings, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([Follower](), "error".localized())
            }
        }
    }
    
    
    static func getFollowers(user_id: Int, completion:@escaping ([Follower], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([Follower](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getFollowers(user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([Follower](), "Missing required data".localized())
                    default:
                        completion([Follower](), "Error")
                    }
                }else{
                    
                    do{
                        let followers = try JSONDecoder().decode([Follower].self, from: response.data!)
                        completion(followers, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([Follower](), "error".localized())
            }
        }
    }
    
    
    static func follow(user_id: Int, follow_id: Int, completion:@escaping ( String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion( "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.follow(user_id: user_id, follow_id: follow_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Users id are not correct".localized())
                    case -3:
                        completion("Blocked User".localized())
                    case -4:
                        completion("Cannot follow yourself".localized())
                    default:
                        completion(nil)
                        
                    }
                }
                
            }else{
                completion("Error occurred".localized())
            }
        }
    }
    
    
    static func acceptFollower(user_id: Int, follow_id: Int, completion:@escaping ( String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion( "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.acceptFollower(user_id: user_id, follow_id: follow_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Users id are not correct".localized())
                    case -3:
                        completion("Blocked User".localized())
                    case -4:
                        completion("Cannot follow yourself".localized())
                    default:
                        completion(nil)
                        
                    }
                }
                
            }else{
                completion("Error occurred".localized())
            }
        }
    }
    
    static func blockUser(user_id: Int, blocked_id: Int, completion:@escaping ( String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion( "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.blockUser(user_id: user_id, blocked_id: blocked_id)).responseJSON { (response) in
            
            print(response)
           
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Can not block yourself".localized())
                    default:
                        completion(nil)
                    }
                }
                
            }else{
                completion("Error occurred".localized())
            }
        }
    }
    
    
    static func getBlockedUsers(user_id: Int, completion:@escaping ([UserShort], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([UserShort](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getBlockedUsers(user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion([UserShort](), "Can not block yourself".localized())
                    default:
                        completion([UserShort](), "Error occurred".localized())
                    }
                }else{
                    
                    do{
                        let user = try JSONDecoder().decode([UserShort].self, from: response.data!)
                        completion(user, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                }
                
            }else{
                completion([UserShort](), "Error occurred".localized())
            }
        }
    }
    
    static func search(user_id: Int, keyword: String, completion:@escaping ( SearchResult,String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(SearchResult(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.search(user_id: user_id, keyword: keyword)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                
                if let err = response.result.value as? Int{
                    switch err {
                    case -1:
                        completion(SearchResult(), "Missing required data".localized())
                    default:
                        completion(SearchResult(), "Error occurred".localized())
                        
                    }
                }
                
                do{
                    let searchResult = try JSONDecoder().decode(SearchResult.self, from: response.data!)
                    completion(searchResult, nil)
                }
                catch{
                    print(error)
                }
                
            }else{
                completion(SearchResult(), "Error occurred".localized())
            }
        }
    }
    
    static func getPickedComments(user_id: Int, picked_id: Int, completion:@escaping ([Comment], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([Comment](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getPickedComments(user_id: user_id, picked_id: picked_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([Comment](), "Missing required data".localized())
                    case -2:
                        completion([Comment](), "Picked not found".localized())
                    case -3:
                        completion([Comment](), "User not found".localized())
                    default:
                        completion([Comment](), "Error")
                    }
                }else{
                    
                    do{
                        let comments = try JSONDecoder().decode([Comment].self, from: response.data!)
                        completion(comments, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([Comment](), "error".localized())
            }
        }
    }
    
    
    static func addComment(user_id: Int, picked_id: Int, message: String, mentioned_usernames: [String], completion:@escaping ( Comment,String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(Comment(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.addComment(user_id: user_id, picked_id: picked_id, message: message, mentioned_usernames: mentioned_usernames)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion(Comment(), "Missing required data".localized())
                    case -2:
                        completion(Comment(), "Picked not found".localized())
                    case -3:
                        completion(Comment(), "User not found".localized())
                    default:
                        completion(Comment(), "Error")
                    }
                }else{
                    
                    do{
                        let comment = try JSONDecoder().decode(Comment.self, from: response.data!)
                        completion(comment, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion(Comment(), "error".localized())
            }
        }
    }
    
    
    static func deleteComment(comment_id: Int, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.deleteComment(comment_id: comment_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Comment not found".localized())
                    default:
                        completion(nil)
                    }
                }
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    static func getCommentReplies(user_id: Int, comment_id: Int, completion:@escaping ([Reply], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([Reply](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getCommentReplies(user_id: user_id, comment_id: comment_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([Reply](), "Missing required data".localized())
                    case -2:
                        completion([Reply](), "Comment not found".localized())
                    case -3:
                        completion([Reply](), "User not found".localized())
                    default:
                        completion([Reply](), "Error")
                    }
                }else{
                    
                    do{
                        let replies = try JSONDecoder().decode([Reply].self, from: response.data!)
                        completion(replies, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([Reply](), "error".localized())
            }
        }
    }
    
    
    static func addReply(user_id: Int, comment_id: Int, message: String, mentioned_usernames: [String], completion:@escaping ( Reply,String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion(Reply(), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.addReply(user_id: user_id, comment_id: comment_id, message: message, mentioned_usernames: mentioned_usernames)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion(Reply(), "Missing required data".localized())
                    case -2:
                        completion(Reply(), "comment not found".localized())
                    case -3:
                        completion(Reply(), "User not found".localized())
                    default:
                        completion(Reply(), "Error")
                    }
                }else{
                    
                    do{
                        let reply = try JSONDecoder().decode(Reply.self, from: response.data!)
                        completion(reply, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion(Reply(), "error".localized())
            }
        }
    }
    
    
    static func deleteReply(reply_id: Int, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.deleteReply(reply_id: reply_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Reply not found".localized())
                    default:
                        completion(nil)
                    }
                }
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    static func reportSpam(user_id: Int,pickd_id: Int, notes: String, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.reportSpam(picked_id: pickd_id, user_id: user_id, notes: notes)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion("Missing required data".localized())
                    case -2:
                        completion("Picked not found".localized())
                    default:
                        completion(nil)
                    }
                }
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    static func viewPicked(pickd_id: Int){
        
        Alamofire.request(APIRouter.viewPicked(picked_id: pickd_id)).responseJSON { (response) in
            
        }
        
    }
    
    
    static func likePicked(user_id: Int,pickd_id: Int, completion:@escaping (String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion("No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.likePicked(picked_id: pickd_id, user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let jsonResponce = response.result.value as? [String: Any]{
                   print(jsonResponce)
                }
            }else{
                completion("error".localized())
            }
        }
    }
    
    
    
    static func getMostViewedPickedGroups(user_id: Int, completion:@escaping ([PickedGroup], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([PickedGroup](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getMostViewedPickedGroups(user_id: user_id)).responseJSON { (response) in
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([PickedGroup](), "Missing required data".localized())
                    case -2:
                        completion([PickedGroup](), "User not found".localized())
                    default:
                        completion([PickedGroup](), "Error")
                    }
                }else{
                    
                    do{
                        let pickedGroups = try JSONDecoder().decode([PickedGroup].self, from: response.data!)
                        completion(pickedGroups, nil)
                    }
                    catch{
                        print(error)
                    }
                }
            }else{
                completion([PickedGroup](), "error".localized())
            }
        }
    }
    
    static func searchForHashtags(keyword: String, completion:@escaping ([String], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([String](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.searchForHashtags(keyword: keyword)).responseJSON { (response) in
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([String](), "Missing required data".localized())
                    default:
                        completion([String](), "Error")
                    }
                }else{
                    
                    do{
                        let hashtags = try JSONDecoder().decode([String].self, from: response.data!)
                        completion(hashtags, nil)
                    }
                    catch{
                        print(error)
                    }
                }
            }else{
                completion([String](), "error".localized())
            }
        }
    }
    
    
    static func getHashtagPickedGroups(user_id: String, hashtag: String, completion:@escaping ([PickedGroup], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([PickedGroup](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getHashtagPickedGroups(user_id: user_id, hashtag: hashtag)).responseJSON { (response) in
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([PickedGroup](), "Missing required data".localized())
                    case -2:
                        completion([PickedGroup](), "User not found".localized())
                    default:
                        completion([PickedGroup](), "Error")
                    }
                }else{
                    
                    do{
                        let res = try JSONDecoder().decode([PickedGroup].self, from: response.data!)
                        completion(res, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                }
            }else{
                completion([PickedGroup](), "error".localized())
            }
        }
    }
    
    static func getLikesList(picked_id: Int, user_id: Int, completion:@escaping ([UserShort], String?)->()){
        
        if !Reachability.isConnectedToNetwork(){
            completion([UserShort](), "No Internet connection".localized())
            return
        }
        
        Alamofire.request(APIRouter.getLikesList(picked_id: picked_id, user_id: user_id)).responseJSON { (response) in
            
            print(response)
            
            if response.result.isSuccess{
                if let err = response.result.value as? Int{
                    switch err{
                    case -1:
                        completion([UserShort](), "Missing required data".localized())
                    case -2:
                        completion([UserShort](), "User not found".localized())
                    default:
                        completion([UserShort](), "Error")
                    }
                }else{
                    
                    do{
                        let users = try JSONDecoder().decode([UserShort].self, from: response.data!)
                        completion(users, nil)
                    }
                    catch{
                        print(error)
                    }
                    
                    
                }
            }else{
                completion([UserShort](), "error".localized())
            }
        }
    }
    
    
    
}

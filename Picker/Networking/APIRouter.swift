//
//  APIRouter.swift
//  NetworkingLayer
//
//  Created by Vavisa - iMac 2 on 4/8/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import Alamofire


protocol APIConfiguration: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
}

enum APIRouter: URLRequestConvertible {
    
    case register(name:String, username: String, email:String,  password: String ,profile_image: String, date_of_birth: String, token: String)
    case login(email:String, password:String, token: String)
    case logout(token: String)
    case changePassword(user_id: Int, old_password: String, new_password: String)
    case uploadPicked(userId: Int, photo: String, thumb: String, is_video: String, location: String, tag: String,caption:String, comments_are_public:Bool, mentioned_usernames: [String])
    case addTag(user_id: Int, name: String , location: String)
    case getUserTags(user_id: Int, location:String)
    case getUserPicked(user_id: Int, target_user_id: Int)
    case getPicked(user_id: Int, picked_id: Int)
    case getPickedGroup(user_id: Int, picked_group_id: Int)
    case getUserProfile(user_id: Int, target_user_id: Int)
    case getUserTimeline(user_id: Int)
    
    case follow(user_id: Int, follow_id: Int)
    case acceptFollower(user_id: Int, follow_id: Int)
    case getFollowing(user_id: Int)
    case getFollowers(user_id: Int)
    case blockUser(user_id: Int, blocked_id: Int)
    case getBlockedUsers(user_id: Int)
    
    case updateUserProfile(user_id: Int, username: String, name: String, date_of_birth: String, bio: String, profile_image: String, cover_image: String)
    case search(user_id: Int, keyword: String)
    
    
    case addComment(user_id: Int, picked_id: Int, message: String, mentioned_usernames:[String])
    case deleteComment(comment_id: Int)
    case getPickedComments(user_id: Int, picked_id: Int)
    
    case addReply(user_id: Int, comment_id: Int, message: String, mentioned_usernames:[String])
    case deleteReply(reply_id: Int)
    case getCommentReplies(user_id: Int, comment_id: Int)
    
    case getUserNotifications(user_id: Int)
    case deleteNotification(notification_id: Int)
    case seeNotifications(user_id: Int)
    case tapNotification(notification_id: Int)
    
    case savePickedGroup(picked_group_id: Int)
    case deletePicked(picked_id: Int)
    case getUserIdFromUsername(username: String)
    
    case reportSpam(picked_id: Int, user_id: Int, notes: String)
    
    // New api
    case likePicked(picked_id: Int, user_id: Int)
    case getMostViewedPickedGroups(user_id: Int)
    case getLikesList(picked_id: Int, user_id: Int)
    case viewPicked(picked_id: Int)
    case getHashtagPickedGroups(user_id: String, hashtag: String)
    case searchForHashtags(keyword: String)
    case forgotPassword(email: String)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        return .post
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .login:
            return "/login"
        case .logout:
            return "/logout"
        case .register:
            return "/register"
        case .changePassword:
            return "changePassword"
        case .uploadPicked:
            return "/uploadPicked"
        case .addTag:
            return "/addTag"
        case .getUserTags:
            return "/getUserTags"
        case .getUserPicked:
            return "/getUserPicked"
        case .getPicked:
            return "getPicked"
        case .getPickedGroup:
            return "getPickedGroup"
        case .getUserProfile:
            return "/getUserProfile"
        case .getUserTimeline:
            return "/getUserTimeline"
        case .updateUserProfile:
            return "/updateUserProfile"
        case .follow:
            return "/follow"
        case .acceptFollower:
            return "/acceptFollower"
        case .getFollowing:
            return "/getFollowing"
        case .getFollowers:
            return "/getFollowers"
        case .blockUser:
            return "/blockUser"
        case .search:
            return "/search"
        case .addComment:
            return "/addComment"
        case .deleteComment:
            return "/deleteComment"
        case .getPickedComments:
            return "/getPickedComments"
        case .addReply:
            return "/addReply"
        case .deleteReply:
            return "/deleteReply"
        case .getCommentReplies:
            return "/getCommentReplies"
        case .getBlockedUsers:
            return "/getBlockedUsers"
        case .getUserNotifications:
            return "/getUserNotifications"
        case .deleteNotification:
            return "/deleteNotification"
        case .seeNotifications:
            return "/seeNotifications"
        case .tapNotification:
            return "/tapNotification"
        case .savePickedGroup:
            return "/savePickedGroup"
        case .deletePicked:
            return "/deletePicked"
        case .getUserIdFromUsername:
            return "/getUserIdFromUsername"
        case .reportSpam:
            return "/reportSpam"
        case .likePicked:
            return "/likePicked"
        case .getMostViewedPickedGroups:
            return "/getMostViewedPickedGroups"
        case .getLikesList:
            return "/getLikesList"
        case .viewPicked:
            return "/viewPicked"
        case .getHashtagPickedGroups:
            return "/getHashtagPickedGroups"
        case .searchForHashtags:
            return "/searchForHashtags"
        case .forgotPassword:
            return "/forgotPassword"
        }
    }
    
    
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .login(let email, let password, let token):
            return ["email": email, "password": password, "token": token]
            
        case .register(let name, let username, let email,
                     let password, let profile_image,let date_of_birth, let token):
            
            return ["name":name,"username": username ,"email": email,
                    "password":password, "profile_image": profile_image,"date_of_birth":date_of_birth ,
                    "token": token]
        case .logout(let token):
            return ["token": token]
        case .changePassword(let user_id, let old_password, let new_password):
            return ["user_id": user_id, "old_password": old_password, "new_password": new_password]
        case .uploadPicked(let user_id, let photo, let thumb, let is_video, let location, let tag, let caption, let comments_are_public, let mentioned_usernames):
            return ["user_id":user_id, "photo": photo , "thumb":thumb , "is_video": is_video,
                    "location": location, "tag": tag, "caption": caption,
                    "comments_are_public": comments_are_public , "mentioned_usernames": mentioned_usernames]
        case .addTag(let user_id, let name, let location):
            return ["user_id":user_id, "name":name, "location":location]
        case .getUserTags(let user_id, let location):
            return ["user_id":user_id, "location":location]
        case .getUserPicked(let user_id, let target_user_id):
            return ["user_id":user_id, "target_user_id":target_user_id]
        case .getUserProfile(let user_id, let target_user_id):
            return ["user_id":user_id, "target_user_id":target_user_id]
        case .getUserTimeline(let user_id):
            return ["user_id":user_id]
        case .updateUserProfile(let user_id, let username, let name, let date_of_birth, let bio,  let profile_image, let cover_image):
            return ["user_id":user_id, "username":username , "name":name,
                    "date_of_birth":date_of_birth,"bio":bio, "profile_image":profile_image,
                    "cover_image":cover_image]
        case .follow(let user_id, let follow_id):
            return ["user_id":user_id, "follow_id":follow_id]
        case .acceptFollower(let user_id, let follow_id):
            return ["user_id":user_id, "follow_id":follow_id]
        case .getFollowing(let user_id):
            return ["user_id":user_id]
        case .getFollowers(let user_id):
            return ["user_id":user_id]
        case .blockUser(let user_id, let blocked_id):
            return ["user_id":user_id, "blocked_id":blocked_id]
        case .getBlockedUsers(let user_id):
             return ["user_id":user_id]
        case .search(let user_id, let keyword):
            return ["user_id":user_id, "keyword":keyword]
        case .addComment(let user_id, let picked_id, let message, let mentioned_usernames):
             return ["user_id":user_id, "picked_id":picked_id, "message":message, "mentioned_usernames":mentioned_usernames]
        case .deleteComment(let comment_id):
            return ["comment_id": comment_id]
        case .getPickedComments(let user_id, let picked_id):
            return ["user_id":user_id, "picked_id":picked_id]
        case .addReply(let user_id, let comment_id, let message, let mentioned_usernames):
            return ["user_id":user_id, "comment_id":comment_id, "message":message, "mentioned_usernames":mentioned_usernames]
        case .deleteReply(let reply_id):
            return ["reply_id": reply_id]
        case .getCommentReplies(let user_id, let comment_id):
            return ["user_id":user_id, "comment_id":comment_id]
        case .getUserNotifications(let user_id):
            return ["user_id":user_id]
        case .deleteNotification(let notification_id):
            return ["notification_id":notification_id]
        case .savePickedGroup(let picked_group_id):
            return ["picked_group_id": picked_group_id]
        case .deletePicked(let picked_id):
            return ["picked_id": picked_id]
        case .getPicked(let user_id, let picked_id):
            return ["user_id": user_id, "picked_id": picked_id]
        case .getPickedGroup(let user_id, let picked_group_id):
            return ["user_id": user_id, "picked_group_id": picked_group_id]
        case .seeNotifications(let user_id):
            return ["user_id": user_id]
        case .tapNotification(let notification_id):
            return ["notification_id": notification_id]
        case .getUserIdFromUsername(let username):
            return ["username": username]
        case .reportSpam(let picked_id, let user_id, let notes):
            return ["user_id": user_id, "picked_id": picked_id, "notes": notes]
        case .likePicked(let picked_id, let user_id):
            return ["user_id": user_id, "picked_id": picked_id]
        case .getMostViewedPickedGroups(let user_id):
            return ["user_id": user_id]
        case .getLikesList(let picked_id, let user_id):
            return ["user_id": user_id, "picked_id": picked_id]
        case .viewPicked(let picked_id):
            return ["picked_id": picked_id]
        case .getHashtagPickedGroups(let user_id, let hashtag):
            return ["user_id": user_id, "hashtag": hashtag]
        case .searchForHashtags(let keyword):
            return ["keyword": keyword]
        case .forgotPassword(let email):
            return ["email": email]
        }
        
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        
        var urlRequest:URLRequest!
        
        // Parameters
        if let parameters = parameters {
            do {
                if method == .get{
                    var dataString = ""
                    for parameter in parameters{
                        dataString.append(parameter.key + "=" + (parameter.value as! String))
                        dataString.append("&")
                    }
                    var urlString = url.absoluteString
                    urlString += path
                    urlString += "?"
                    urlString += dataString
                    if let url = URL(string: urlString){
                        urlRequest = URLRequest(url: url)
                    }
                }else{
                    urlRequest = URLRequest(url: url.appendingPathComponent(path))
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                }
                
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        

        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        urlRequest.setValue(ContentType.authentication.rawValue, forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
        
        
        return urlRequest
    }
}




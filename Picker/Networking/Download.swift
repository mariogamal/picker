//
//  Download.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 3/25/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit

class Download{
    
    var picked: PickedStruct
    init(picked: PickedStruct) {
        self.picked = picked
    }
    
    // Download service sets these values:
    var task:URLSessionDownloadTask?
    var isDownloading = false
    var resumeData: Data?
    
    // Download delegate sets this value:
    var progress: Float = 0
    
}


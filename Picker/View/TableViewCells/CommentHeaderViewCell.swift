//
//  ReplyHeaderView.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 3/3/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import SwiftRichString
import ActiveLabel

protocol CommentHeaderViewCellDelegate{
    func replyPressed(comment:Comment)
    func mentionPressed(username: String)
    func profilePresses(userId: Int)
    func morePressed(comment:Comment, cell: CommentHeaderViewCell)
}

class CommentHeaderViewCell: UITableViewHeaderFooterView {

    var delegate:CommentHeaderViewCellDelegate?
    
    var comment:Comment!{
        didSet{
            
            self.message.text = comment.message
            
            timeLabel.text = comment.readable_time
            
            self.usernameLabel.text = comment.name
            
            
            if let urlString = comment?.profile_image{
                let url = URL(string: urlString)
                self.profileImage.kf.setImage(with: url)
            }
            
            if comment.replies.count > 0{
                viewRepliesLabel.text =  "-" + "View replies".localized() + " " + "(" + String(comment.replies.count) + ")"

            }else{
                viewRepliesLabel.text = ""
            }
            
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupViews()
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = .white
        viewRepliesLabel.text = ""
        moreBtn.isHidden = false
        
    }
    
    @objc func profileTapped(){
        guard let userId = comment?.user_id else{return}
        delegate?.profilePresses(userId: userId)
    }
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-bold", size: 18)
        label.text = "-"
        return label
    }()
    
    let timeLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-medium", size: 13)
        label.text = "-"
        return label
    }()
    
    lazy var message: ActiveLabel = {
        let label = ActiveLabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-Semibold", size: 16)
        label.numberOfLines = 0
        label.text = "-"
        label.mentionColor = AppColors.shared.blue
        label.enabledTypes = [.mention]
        label.handleMentionTap({ (username) in
            self.delegate?.mentionPressed(username: username)
        })
        return label
    }()
    
    lazy var replyBtn:UIButton = {
        let btn = UIButton()
        let attributedTitle = NSAttributedString(string: "Reply".localized(),
                                                 attributes: [NSAttributedString.Key.foregroundColor : AppColors.shared.gray,
                                                              NSAttributedString.Key.font:UIFont(name: "Raleway-SemiBold", size: 15)! ])
        btn.setAttributedTitle(attributedTitle, for: .normal)
        btn.setTitleColor(AppColors.shared.lightGray, for: .normal)
        btn.addTargetClosure(closure: { (btn) in
            self.delegate?.replyPressed(comment: self.comment!)
        })
        return btn
    }()
    
    let viewRepliesLabel:UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-bold", size: 12)
        return label
    }()
    
    lazy var moreBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = AppColors.shared.darkGray
        btn.setImage(#imageLiteral(resourceName: "ic_more_vert_36pt"), for: .normal)
        btn.setTitleColor(AppColors.shared.blue, for: .normal)
        btn.layer.borderColor = AppColors.shared.blue.cgColor
        btn.addTargetClosure { (_) in
            self.delegate?.morePressed(comment:self.comment  , cell: self)
        }
        return btn
    }()
    
    func setupViews(){

        contentView.backgroundColor = .white
        
        addSubview(profileImage)
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        profileImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16).priority(999)
            make.top.equalToSuperview().inset(8)
            make.width.height.equalTo(40)
        }
        
        addSubview(usernameLabel)
        usernameLabel.isUserInteractionEnabled = true
        usernameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage).inset(2)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
        }
        
        addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(usernameLabel)
            make.leading.equalTo(usernameLabel.snp.trailing).offset(4)
        }
        
        
        addSubview(message)
        message.snp.makeConstraints { (make) in
            make.leading.equalTo(usernameLabel)
            make.trailing.equalToSuperview().inset(8)
            make.top.equalTo(usernameLabel.snp.bottom).offset(4)
        }
        
        addSubview(replyBtn)
        replyBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(message)
            make.top.equalTo(message.snp.bottom).offset(4).priority(999) //it will be broken without priority
        }
        
        addSubview(viewRepliesLabel)
        viewRepliesLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(replyBtn)
            make.top.equalTo(replyBtn.snp.bottom).offset(4).priority(999) //it will be broken without priority
            make.bottom.equalToSuperview().offset(-16)
        }
        
        addSubview(moreBtn)
        moreBtn.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().inset(8)
            make.top.equalTo(profileImage)
            make.height.width.equalTo(20)
        }
        
        
    }
    
    
}

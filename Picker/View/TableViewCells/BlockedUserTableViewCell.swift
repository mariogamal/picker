//
//  BlockedUserTableViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 5/21/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit



class BlockedUserTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var userProfile:UserShort?{
        didSet{
            if let profileUrl = userProfile?.profile_image{
                if let url = URL(string:profileUrl){
                    profileImage.kf.setImage(with: url)
                }
            }
            
            usernameLabel.text = userProfile?.username
        }
    }
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-bold", size: 18)
        return label
    }()
    
    
    
    func setupViews(){
        addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16).priority(999)
            make.top.equalToSuperview().offset(16)
            make.width.height.equalTo(40)
        }
        
        addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileImage)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
        }
        
       
    }
    
    
}


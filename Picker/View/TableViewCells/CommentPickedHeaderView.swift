//
//  CommentHeaderView.swift
//  Picker
//
//  Created by Vavisa Mac on 10/15/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import SwiftRichString
import ActiveLabel


protocol CommentPickedHeaderViewDelegate{
    func pickedTapped(picked_group: PickedGroup)
    func showComments()
    func showLikes()
}

class CommentPickedHeaderView: UITableViewHeaderFooterView {
    
    var delegate:CommentPickedHeaderViewDelegate?
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupViews()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = .white
    }
    
    var picked_group:PickedGroup!{
        didSet{
            if let picked = picked_group.picked_group.first{
                if picked.is_video{
                    if let thumbUrl = picked.thumb_url{
                        if let url = URL(string: thumbUrl){
                            pickedImageView.kf.setImage(with: url)
                        }
                    }
                }else{
                    if let url = URL(string: picked.url){
                        pickedImageView.kf.setImage(with: url)
                    }
                }
            }
            
            locationLabel.text = picked_group.picked_group.first?.location
            viewCountLabel.text = String(picked_group.picked_group.first?.view_count ?? 0)
            tagLabel.text = picked_group.picked_group.first?.tag
            captionLabel.text = picked_group.picked_group.first?.caption
            if picked_group.picked_group.count>0{
                locationImage.isHidden = false
            }
            
            
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let pickedImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = UIColor.groupTableViewBackground
        return iv
    }()
    
    let viewCountImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "view")
        return iv
    }()
    
    let viewCountLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-bold", size: 17)
        label.numberOfLines = 1
        return label
    }()
    
    let tabsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.horizontal
        //stackView.distribution  = UIStackView.Distribution.fillEqually
        stackView.alignment = UIStackView.Alignment.center
//        stackView.spacing   = 16.0
        return stackView
    }()
    
    
    let tagLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-bold", size: 19)
        label.numberOfLines = 2
        return label
    }()
    
    let locationImage:UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Location")
        iv.isHidden = true
        iv.contentMode = .scaleAspectFit
        iv.tintColor = AppColors.shared.lightGray
        return iv
    }()
    
    let horizontalLine1:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.superLightGray
        return v
    }()
    
    let horizontalLine2:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.superLightGray
        return v
    }()
    
    let verticalLine1:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.superLightGray
        return v
    }()
    
    lazy var commentButton:UIButton = {
        let button = UIButton()
        button.isSelected = true
        button.setImage(#imageLiteral(resourceName: "comment_unselected"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "comment_selected"), for: .selected)
        button.addTarget(self, action: #selector(switchToComments), for: .touchUpInside)
        return button
    }()
    
    lazy var likeButton:UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Like_unselected"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "Like_btn_selected"), for: .selected)
        button.addTarget(self, action: #selector(switchToLikes), for: .touchUpInside)
        return button
    }()
    
    let locationLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = AppColors.shared.lightGray
        label.font =  UIFont(name: "Raleway-Semibold", size: 14)
        label.numberOfLines = 1
        return label
    }()
    
    let captionLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-semibold", size: 16)
        label.numberOfLines = 0
        return label
    }()
    
    @objc func handlePickedImageTapped(){
        delegate?.pickedTapped(picked_group: picked_group)
    }
    
    @objc func switchToComments(){
        if !commentButton.isSelected{
            commentButton.isSelected = true
            likeButton.isSelected = false
            delegate?.showComments()
        }
    }
    
    @objc func switchToLikes(){
        if !likeButton.isSelected{
            likeButton.isSelected = true
            commentButton.isSelected = false
            delegate?.showLikes()
        }
        
    }
    
    
    func setupViews(){
        contentView.backgroundColor = .white
        
        contentView.addSubview(pickedImageView)
        pickedImageView.isUserInteractionEnabled = true
        pickedImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePickedImageTapped)))
        pickedImageView.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().inset(16)
            make.width.equalTo(125)
            make.height.equalTo(175)
        }
        
        contentView.addSubview(viewCountImageView)
        viewCountImageView.snp.makeConstraints { (make) in
            make.leading.equalTo(pickedImageView).offset(4)
            make.top.equalTo(pickedImageView.snp.bottom).offset(4)
            make.width.equalTo(25)
            make.height.equalTo(25)
        }
        
        contentView.addSubview(viewCountLabel)
        viewCountLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(viewCountImageView.snp.trailing).offset(16)
            make.centerY.equalTo(viewCountImageView)
        }
        
        contentView.addSubview(tagLabel)
        tagLabel.snp.makeConstraints { (make) in
            make.top.equalTo(pickedImageView).offset(4)
            make.leading.equalTo(pickedImageView.snp.trailing).offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        contentView.addSubview(locationImage)
        locationImage.snp.makeConstraints { (make) in
            make.top.equalTo(tagLabel.snp.bottom).offset(4)
            make.leading.equalTo(pickedImageView.snp.trailing).offset(8)
            make.height.width.equalTo(20)
        }
        
        contentView.addSubview(locationLabel)
        locationLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(locationImage.snp.trailing).offset(4)
            make.centerY.equalTo(locationImage)
        }
        
        contentView.addSubview(captionLabel)
        captionLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(pickedImageView.snp.trailing).offset(8)
            make.top.equalTo(locationImage.snp.bottom).offset(8)
            make.bottom.equalToSuperview().offset(-4).priority(1)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        
        
      
        tabsStackView.addArrangedSubview(commentButton)
        tabsStackView.addArrangedSubview(likeButton)
        commentButton.snp.makeConstraints { (make) in
            make.width.equalTo(likeButton.snp.width)
        }
        
        for (index, _) in tabsStackView.arrangedSubviews.enumerated() {
            let separator = UIView()
            separator.widthAnchor.constraint(equalToConstant: 1).isActive = true
            separator.backgroundColor = AppColors.shared.superLightGray
            tabsStackView.insertArrangedSubview(separator, at: index + 1)
            separator.heightAnchor.constraint(equalTo: tabsStackView.heightAnchor, multiplier: 0.6).isActive = true
        }

        
        contentView.addSubview(tabsStackView)
        tabsStackView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(50)
        }
        
        
        
        contentView.addSubview(horizontalLine1)
        horizontalLine1.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        contentView.addSubview(horizontalLine2)
        horizontalLine2.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalTo(tabsStackView)
            make.height.equalTo(1)
        }
    }
    
    
}

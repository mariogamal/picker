//
//  NotificationTableViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/19/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import Kingfisher


protocol NotificationFollowTableViewCellDelegate{
    func acceptBtnPressed(targetUserId: Int, cell:NotificationFollowTableViewCell)
    func followBtnPressed(targetUserId: Int, cell:NotificationFollowTableViewCell)
    func profileTapped(cell:NotificationFollowTableViewCell)
}

class NotificationFollowTableViewCell: UITableViewCell {

    var notificationFollowDelegate: NotificationFollowTableViewCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.isUserInteractionEnabled = true
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.backgroundColor = .white
    }
    
    @objc func profileTapped(){
        notificationFollowDelegate?.profileTapped(cell: self)
    }
    
    var notification:NotificationStruct!{
        didSet{
            
            usernameLabel.text = notification.name
            
            
            if let profileImageUrl = notification.profile_image {
                let url = URL(string:profileImageUrl)
                profileImage.kf.setImage(with: url)
            }
            
            self.timeLabel.text = notification.readable_date
            
            
            if notification.is_accepted{
                print("isAccepted")
                self.acceptBtn.layer.borderColor = AppColors.shared.red.cgColor
                self.acceptBtn.setTitleColor(AppColors.shared.red, for: .normal)
                self.acceptBtn.setTitle("Unaccept".localized(), for: .normal)
            }else{
                print("NOT Accepted")
                self.acceptBtn.layer.borderColor = AppColors.shared.blue.cgColor
                self.acceptBtn.setTitleColor(AppColors.shared.blue, for: .normal)
                self.acceptBtn.setTitle("Accept".localized(), for: .normal)
            }
        
            
            if notification.is_follow{
                print("ISFOLLOW")
                self.followBtn.backgroundColor = AppColors.shared.red
                self.followBtn.setTitle("Unfollow".localized(), for: .normal)
            }else{
                print("NOT FOLLOW")
                self.followBtn.backgroundColor = AppColors.shared.blue
                self.followBtn.setTitle("Follow".localized(), for: .normal)
            }
            
            
            
            
            if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                messageLabel.text = notification.message_ar
            }else{
                messageLabel.text = notification.message_en
            }
            
            if !(notification?.is_tapped)!{
                self.backgroundColor = UIColor(white: 0.95, alpha: 1)
            }
            
        }
    }
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-Bold", size: 18)
        label.numberOfLines = 2
        return label
    }()
    
    let messageLabel:UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-semibold", size: 16)
        return label
    }()
    
    let timeLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-regular", size: 11)
        return label
    }()
    
    lazy var followBtn:FillButton = {
        let btn = FillButton()
        btn.titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 13)
        btn.addTargetClosure(closure: { (_) in
            self.notificationFollowDelegate?.followBtnPressed(targetUserId: (self.notification?.user_id)!,cell: self)
        })
        return btn
    }()
    
    lazy var acceptBtn:LineButton = {
        let btn = LineButton()
        btn.titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 13)
        btn.addTargetClosure(closure: { (_) in
            self.notificationFollowDelegate?.acceptBtnPressed(targetUserId: (self.notification?.user_id)!, cell: self)
        })
        return btn
    }()
    
    @objc func acceptFollower(){
        //guard let userId = Auth.auth().currentUser?.uid, let followerId = userProfile?.id else {return}
//        Service.shared.acceptFollower(userId: userId, followerId: followerId) { (error) in
//
//        }
    }
    
    
    func setupViews(){
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        
        addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(16)
            make.width.height.equalTo(45)
        }
        
        addSubview(followBtn)
        followBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-8)
            make.width.equalTo(75)
            make.height.equalTo(25)
        }
        
        addSubview(acceptBtn)
        acceptBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(followBtn)
            make.trailing.equalTo(followBtn.snp.leading).offset(-4)
            make.width.equalTo(75)
            make.height.equalTo(25)
        }
        
        addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage).offset(4)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
            make.trailing.equalTo(acceptBtn.snp.leading).offset(-4)
        }
        
        
        
        addSubview(messageLabel)
        messageLabel.snp.makeConstraints { (make) in
            make.top.equalTo(usernameLabel.snp.bottom).offset(4)
           make.leading.equalTo(usernameLabel)
            make.bottom.equalToSuperview().offset(-24)
        }
        
        addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(messageLabel)
            make.leading.equalTo(messageLabel.snp.trailing).offset(8)
        }
        
        addSubview(followBtn)
        followBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.trailing.equalToSuperview().offset(-8)
            make.width.equalTo(75)
            make.height.equalTo(25)
        }
        
        addSubview(acceptBtn)
        acceptBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(followBtn)
            make.trailing.equalTo(followBtn.snp.leading).offset(-4)
            make.width.equalTo(75)
            make.height.equalTo(25)
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}

protocol NotificationCommentTableViewCellDelegate{
    func profileTapped(cell:NotificationCommentTableViewCell)
    func commentTapped(cell:NotificationCommentTableViewCell)
}

class NotificationCommentTableViewCell:NotificationFollowTableViewCell{
    
    var notificationCommentDelegate: NotificationCommentTableViewCellDelegate?
   
   
    override func profileTapped() {
        notificationCommentDelegate?.profileTapped(cell: self)
    }
    
    @objc func commentTapped(){
        notificationCommentDelegate?.commentTapped(cell: self)
    }
    
    override func setupViews() {
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(commentTapped)))
        messageLabel.numberOfLines = 2
        
        addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(16)
            make.width.height.equalTo(45)
        }
        
        addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage).offset(4)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
        }
        
        addSubview(messageLabel)
        messageLabel.snp.makeConstraints { (make) in
            make.top.equalTo(usernameLabel.snp.bottom).offset(4)
            make.leading.equalTo(usernameLabel)
            make.trailing.equalToSuperview().offset(-8)
            make.bottom.equalToSuperview().offset(-24)
        }
        
        addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(usernameLabel)
            make.leading.equalTo(usernameLabel.snp.trailing).offset(8)
        }
        
       
    }
}


protocol NotificationPickedTableViewCellDelegate{
    func profileTapped(cell:NotificationPickedTableViewCell)
    func commentTapped(cell:NotificationPickedTableViewCell)
}

class NotificationPickedTableViewCell:NotificationCommentTableViewCell{
    
    var notificationPickedDelegate: NotificationPickedTableViewCellDelegate?
    
    
    override func profileTapped() {
        notificationPickedDelegate?.profileTapped(cell: self)
    }
    
    override func commentTapped() {
        notificationPickedDelegate?.commentTapped(cell: self)
    }
    
}



protocol NotificationReplyTableViewCellDelegate{
    func profileTapped(cell:NotificationReplyTableViewCell)
    func commentTapped(cell:NotificationReplyTableViewCell)
}

class NotificationReplyTableViewCell:NotificationCommentTableViewCell{
    
    var notificationReplyDelegate: NotificationReplyTableViewCellDelegate?
    
    
    override func profileTapped() {
        notificationReplyDelegate?.profileTapped(cell: self)
    }
    
    override func commentTapped() {
          notificationReplyDelegate?.commentTapped(cell: self)
    }
    
}


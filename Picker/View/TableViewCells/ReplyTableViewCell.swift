//
//  CommentTableViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 2/18/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import SwiftRichString
import ActiveLabel

protocol ReplyTableViewCellDelegate{
    func morePressed(reply:Reply, cell: ReplyTableViewCell)
    func replyProfilePresses(userId: Int)
    func replyMentionPressed(username: String)
    func replyToReplyPressed(reply: Reply, comment: Comment)
}


class ReplyTableViewCell: UITableViewCell {

    var delegate:ReplyTableViewCellDelegate?
    
    
    var reply:Reply!{
        didSet{
            
            self.message.text = reply.message
            
            timeLabel.text = reply.readable_time
            
            self.usernameLabel.text = reply.name
        
            
            
            let url = URL(string: reply.profile_image)
            self.profileImage.kf.setImage(with: url)
            
        }
    }
    
    var comment:Comment!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        clipsToBounds = true
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        moreBtn.alpha = 1
        backgroundColor = .white
    }
    
   
    
    let titleLabel = UILabel()
    
    
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-Bold", size: 16)
        label.text = "-"
        return label
    }()
    
    let timeLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-regular", size: 11)
        label.text = "-"
        return label
    }()
    
    lazy var message:ActiveLabel = {
        let label = ActiveLabel()
        label.enabledTypes = [.mention]
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-semibold", size: 15)
        label.numberOfLines = 0
        label.mentionColor = AppColors.shared.blue
        label.text = "-"
        label.handleMentionTap({ (username) in
            self.delegate?.replyMentionPressed(username: username)
        })
        return label
    }()
    
    lazy var replyBtn:UIButton = {
        let btn = UIButton()
        let attributedTitle = NSAttributedString(string: "Reply".localized(),
                                                 attributes: [NSAttributedString.Key.foregroundColor : AppColors.shared.gray,
                                                              NSAttributedString.Key.font:UIFont(name: "Raleway-SemiBold", size: 15)! ])
        btn.setAttributedTitle(attributedTitle, for: .normal)
        btn.setTitleColor(AppColors.shared.lightGray, for: .normal)
        btn.addTargetClosure(closure: { (btn) in
            self.delegate?.replyToReplyPressed(reply: self.reply, comment: self.comment)
        })
        return btn
    }()
    
    
    let replyCount:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.lightGray
        label.font =  UIFont(name: "Raleway-semiBold", size: 15)
        label.numberOfLines = 0
        label.text = ""
        return label
    }()
    
    lazy var moreBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = AppColors.shared.darkGray
        btn.setImage(#imageLiteral(resourceName: "ic_more_vert_36pt"), for: .normal)
        btn.setTitleColor(AppColors.shared.blue, for: .normal)
        btn.layer.borderColor = AppColors.shared.blue.cgColor
        btn.addTargetClosure { (_) in
            self.delegate?.morePressed(reply: self.reply!, cell: self)
        }
        return btn
    }()
    
    
    
    
    @objc func profileTapped(){
        delegate?.replyProfilePresses(userId: (reply?.user_id)!)
    }
    
    func setupViews(){
        
        message.handleMentionTap { (username) in
            self.delegate?.replyMentionPressed(username: username)
        }
        
        usernameLabel.font = UIFont(name: "Raleway-Semibold", size: 17)
        timeLabel.font = UIFont(name: "Raleway-Medium", size: 12)
        message.font = UIFont(name: "Raleway-Medium", size: 16)
        
        
        contentView.backgroundColor = .white
        
        addSubview(profileImage)
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        profileImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48).priority(999)
            make.top.equalToSuperview().inset(16)
            make.width.height.equalTo(30)
        }
        
        addSubview(usernameLabel)
        usernameLabel.isUserInteractionEnabled = true
        usernameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage).inset(2)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
        }
        
        addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(usernameLabel)
            make.leading.equalTo(usernameLabel.snp.trailing).offset(4)
        }
        
        
        addSubview(message)
        message.snp.makeConstraints { (make) in
            make.leading.equalTo(usernameLabel)
            make.trailing.equalToSuperview().inset(8)
            make.top.equalTo(usernameLabel.snp.bottom).offset(4)
        }
        
        addSubview(replyBtn)
        replyBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(message)
            make.top.equalTo(message.snp.bottom).offset(4).priority(999) //it will be broken without priority
            make.bottom.equalToSuperview().inset(16)
        }
        
        addSubview(moreBtn)
        moreBtn.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().inset(8)
            make.top.equalTo(profileImage)
            make.height.width.equalTo(20)
        }
        
    }
}

//
//  customButton.swift
//  custom camera
//
//  Created by Omar Mohammed on 7/20/17.
//  Copyright © 2017 Omar Mohammed. All rights reserved.
//

import UIKit

class BlueShadowButton: UIButton {
    
    var shadowLayer: CAShapeLayer!
    
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 16)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height/2).cgPath
            shadowLayer.fillColor = AppColors.shared.blue.cgColor
            
            shadowLayer.shadowColor = UIColor(white: 0, alpha: 0.1).cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0, height: 2)
            shadowLayer.shadowOpacity = 1
            shadowLayer.shadowRadius = 10
            
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
}

class FillButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitleColor(UIColor.white, for: .normal)
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height/2
    }
    
    
    
    
}


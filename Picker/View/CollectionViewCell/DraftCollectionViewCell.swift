//
//  DraftCollectionViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 4/26/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//


import UIKit
import Kingfisher
import Hero
import RealmSwift



class DraftCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        
    }
    
    var delegate:ProfilePickedCollectionViewCellDelegate?
    
    func configure(draftPicked:DraftPicked){
        self.locationLabel.text = draftPicked.location
        self.tagLabel.text = draftPicked.tag
        thumbImage.hero.id = draftPicked.url
    }
    
    
    
    
    let thumbImage:UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = true
        v.backgroundColor = UIColor.groupTableViewBackground
        return v
    }()
    
    let tagLabel:UILabel = {
        let label = UILabel()
        label.dropShadowLow()
        label.numberOfLines = 2
        label.textColor = .white
        label.text = label.text?.uppercased()
        label.font =  UIFont(name: "Raleway-SemiBold", size: 15)
        return label
    }()
    
    let timeLabel:UILabel = {
        let label = UILabel()
        label.dropShadowLow()
        label.textColor = .white
        label.font =  UIFont(name: "Raleway-SemiBold", size: 13)
        return label
    }()
    
    let locationImage:UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = #imageLiteral(resourceName: "Location")
        iv.tintColor = AppColors.shared.lightGray
        return iv
    }()
    
    let locationLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = AppColors.shared.lightGray
        label.font =  UIFont(name: "Raleway-Semibold", size: 14)
        label.numberOfLines = 2
        return label
    }()
    
    
    func setupViews(){
        
        addSubview(locationImage)
        locationImage.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().offset(8)
            make.height.width.equalTo(20)
        }
        
        addSubview(locationLabel)
        locationLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(locationImage.snp.trailing).offset(4)
            make.centerY.equalTo(locationImage)
        }
        
       
        addSubview(thumbImage)
        thumbImage.snp.makeConstraints { (make) in
            make.top.equalTo(locationImage.snp.bottom).offset(16)
            make.bottom.left.right.equalToSuperview()
        }
        
        thumbImage.addSubview(tagLabel)
        tagLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(12)
            make.bottom.equalToSuperview().offset(-16)
        }
        
        thumbImage.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(12)
            make.top.equalToSuperview().offset(16)
        }
        
        
        
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.thumbImage.layer.cornerRadius = 5
        
    }
    
}


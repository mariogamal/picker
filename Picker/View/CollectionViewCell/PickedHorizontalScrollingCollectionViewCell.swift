////
////  LocationsHorizontalScrollingCollectionViewCell.swift
////  Picker
////
////  Created by Vavisa - iMac 2 on 11/14/17.
////  Copyright © 2017 Omar basaleh. All rights reserved.
////
//
//import UIKit
//
//
//protocol pickedHorizontalScrollingDelegate{
//    func profileSelected(userId: Int)
//}
//
//class PickedHorizontalScrollingCollectionViewCell: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
//
//    private let pickedCellId = "pickedCellId"
//    var pickedGroups =  [PickedGroup]()
//    var pickedDelegate:pickedHorizontalScrollingDelegate?
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupViews()
//        clipsToBounds = false
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    let titleLabel:UILabel = {
//        let label = UILabel()
//        label.textColor = AppColors.shared.gray
//        label.text = label.text?.uppercased()
//        label.font =  UIFont(name: "Raleway-Regular", size: 20)
//        return label
//    }()
//
//    lazy var collection:UICollectionView = {
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .horizontal
//        let c = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        c.backgroundColor = .clear
//        //c.alwaysBounceHorizontal = true
//        c.delegate = self
//        c.dataSource = self
//        c.showsHorizontalScrollIndicator = false
//        return c
//    }()
//
//
//
//    func setupViews(){
//        addSubview(titleLabel)
//        titleLabel.snp.makeConstraints { (make) in
//            make.top.equalToSuperview().offset(8)
//            make.left.right.equalToSuperview().inset(16)
//        }
//
//        addSubview(collection)
//        collection.snp.makeConstraints { (make) in
//            make.left.right.bottom.equalToSuperview()
//            make.top.equalTo(titleLabel.snp.bottom).offset(16)
//        }
//        collection.register(SearchPickedCollectionViewCell.self, forCellWithReuseIdentifier: pickedCellId)
//    }
//
//
//    //MARK:- collectionView
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return pickedGroups.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let pickedCell = collectionView.dequeueReusableCell(withReuseIdentifier: pickedCellId, for: indexPath) as! SearchPickedCollectionViewCell
//        pickedCell.configure(pickedGroup: pickedGroups[indexPath.item])
//        pickedCell.user = pickedGroups[indexPath.item].user
//        pickedCell.searchDelegate = self
//        pickedCell.thumbImage.layer.borderWidth = 0
//        let count = pickedGroups[indexPath.item].picked_group.count
//        pickedCell.contentCountLabel.text = String(describing:count)
//        let location = pickedGroups[indexPath.item].picked_group.first?.location
//        pickedCell.locationLabel.text = location
//        return pickedCell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 175, height: 300)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 24, left: 24, bottom: 5, right: 24)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 24
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 24
//    }
//}
//
//
//extension PickedHorizontalScrollingCollectionViewCell:SearchPickedCollectionViewCellDelegate{
//    func profileSelected(userId: Int) {
//        pickedDelegate?.profileSelected(userId: userId)
//    }
//
//
//}

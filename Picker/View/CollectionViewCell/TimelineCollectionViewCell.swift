//
//  PickedTimelineCollectionViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/18/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

protocol PickedTimelineDelegate{
    func pickedGroupSelected(pickedGroup:PickedGroup, indexPath:IndexPath)
    func profileSelected(userId: Int)
}

class TimelineCollectionViewCell: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    private let pickedCellId = "pickedCellId"
    var pickedDelegate:PickedTimelineDelegate?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func profileTapped(){
        guard let userId = timeline?.user_id else{return}
        pickedDelegate?.profileSelected(userId: userId)
    }
    
 
    var timeline:Timeline? {
        didSet{
            
            usernameLabel.text = timeline?.name
            
            if let profileImageUrl = timeline?.profile_image{
                let url = URL(string:profileImageUrl)
                profileImage.kf.setImage(with: url)
            }
            
            self.collection.reloadData()
            
        }
    }
    

    let profileImage:RoundedImageViewNoBorder = {
        let iv = RoundedImageViewNoBorder()
        iv.backgroundColor = AppColors.shared.lightGray
        iv.image = #imageLiteral(resourceName: "Profile_placeholder")
        return iv
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    lazy var collection:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let c = UICollectionView(frame: .zero, collectionViewLayout: layout)
        c.backgroundColor = .clear
        //c.alwaysBounceHorizontal = true
        c.delegate = self
        c.dataSource = self
        c.showsHorizontalScrollIndicator = false
        return c
    }()
    
    func setupViews(){
        
        addSubview(profileImage)
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        profileImage.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(16)
            make.height.width.equalTo(35)
        }
        
        addSubview(usernameLabel)
        usernameLabel.isUserInteractionEnabled = true
        usernameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        usernameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileImage)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
        }
        
        addSubview(collection)
        collection.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(profileImage.snp.bottom).offset(4)
        }
        collection.register(TimelinePickedCollectionViewCell.self, forCellWithReuseIdentifier: pickedCellId)
    }
    
    
    //MARK:- collectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeline!.picked_groups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let pickedCell = collection.dequeueReusableCell(withReuseIdentifier: pickedCellId, for: indexPath) as! TimelinePickedCollectionViewCell
        
        pickedCell.configure(pickedGroup: (timeline?.picked_groups[indexPath.item])!)
        if let count = timeline?.picked_groups[indexPath.item].picked_group.count{
            pickedCell.contentCountLabel.text = String(describing:count)
        }
        if let location = timeline?.picked_groups[indexPath.item].picked_group.first?.location{
            pickedCell.locationLabel.text = location
        }
        return pickedCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 135, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 24, left: 24, bottom: 5, right: 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 24
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 24
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if pickedDelegate != nil{
            pickedDelegate?.pickedGroupSelected(pickedGroup: (timeline?.picked_groups[indexPath.item])!, indexPath: indexPath)
        }
    }
}

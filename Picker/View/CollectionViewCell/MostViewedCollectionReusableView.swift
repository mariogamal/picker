//
//  MostViewedCollectionReusableView.swift
//  Picker
//
//  Created by Omar mohammed on 2/11/19.
//  Copyright © 2019 Omar basaleh. All rights reserved.
//

import UIKit

class MostViewedCollectionReusableView: UICollectionReusableView {
    
    var label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        label.textColor = .black
        label.font = UIFont(name: "Raleway-bold", size: 19)
        label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(16)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}

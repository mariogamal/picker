//
//  TermsAndConditionsViewController.swift
//  Picker
//
//  Created by Vavisa Mac on 9/18/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {

    var text = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Terms & Conditions".localized()
        setupViews()
//        view.backgroundColor = .white
//
//        let path = Bundle.main.path(forResource: "Terms", ofType: "txt")
//        let data = try! String.init(contentsOfFile: path!)
       
        text.append("We believe that all users have the right to create their content whether visual, audible, or written, and share these ideas and information in our platform instantly both publicly and privately.\n\n")
        
        text.append("In order to protect and insure this service, we have introduced a number of restrictions on both content and activity. The former and latter paragraphs represent the terms of use of Picker platform. All users must observe the terms and policies elaborated below, failing to do so will result in operating procedures like:\n\n")
        
        text.append("1- Restrict your ability in posting new material, or interacting with other users temporarily.\n")
        text.append("2- Deactivate your account(s) permanently.\n\n")
        
        text.append("Terms and conditions:\n\n")
        text.append("1- Username Misuse:\n\n")
        text.append("- Selling and buying Picker usernames is prohibited.\n")
        text.append("- Impersonation is prohibited is all forms like:\n")
        text.append("a. Creating accounts to prevent others from using them.\n")
        text.append( "b. Creating accounts with an intention to sell them.\n")
        text.append("- It is forbidden to impersonate individuals, groups, or organization in a misleading and confusing way for the users, and in a way that might result in such misperception.\n\n")
            
            text.append("2- Intellectual Property\n\n")
            text.append("Picker has the right to deactivate any account that publishes materials that belong to other individuals, or use a certain trademark/logo that might mislead others from knowing ownership and copyrights.\n\n")
            text.append("3- Sexual Content\n\n")
            text.append("Picker is not designed to broadcast explicit sexual materials. In case of reporting such incident, we have the right to deactivate the account or adopt other disciplinary procedures.\n\n")
        text.append("4- Violent Content\n\n")
        text.append("- Posting violent scenes or arbitrary intimidating material that might cause fear to other users is not allowed. In case of posting for news reports or documentaries, the user must provide enough information to help others understand the content.")
        
        text.append("- Posting threats, willing to cause harm to someone, desires to kill a person or a group of people is prohibited and that includes promoting terrorism.\n\n")
        text.append("5 - Hate Content\n\n")
        text.append("Posting any material that promotes violence against a group pf people based on skin color, gender, ethnicity, disability, status, or sexual identity can risk account deactivation or other disciplinary procedures.\n\n")
        text.append("6- While Using Picker\n\n")
        text.append("- It’s prohibited to gain unauthorized access to Picker’s private backend systems, details, and servers and tamper with them at any time.\n")
        text.append("- Obstruction of reaching any user or network host is prohibited as well as sending viruses and malwares or any burden to Picker’s backend systems.\n")
        text.append("- It is forbidden to post any harmful content that aims at harming other users or their devices or jeopardize their privacy.\n\n")
        
        text.append("**These terms and conditions are subjected to change occasionally, and we only preserve the right to do so and announce the changes when due.\n")
        
        textView.text = text
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(handleCancel))
    }
    
    
    @objc func handleCancel (){
        dismiss(animated: true, completion: nil)
    }

    let textView: UITextView = {
        let tv = UITextView()
        tv.isEditable = false
        tv.isSelectable = false
        tv.font = UIFont(name: "Raleway-semibold", size: 16)
        tv.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        return tv
    }()
    
    func setupViews(){
    
        view.addSubview(textView)
        textView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
   

}

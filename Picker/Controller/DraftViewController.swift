//
//  DraftViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 4/26/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import RealmSwift
import AVFoundation

class DraftViewController: UIViewController {

    var draftPickeds = [DraftPicked]()
    private let draftCellId = "draftCellId"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        collectionView.register(DraftCollectionViewCell.self, forCellWithReuseIdentifier: draftCellId)
        
        let realm = try! Realm()
        let result = realm.objects(DraftPicked.self)
        print(result)
        for draftPicked in result{
            draftPickeds.append(draftPicked)
        }
        
        if result.count == 0{
            let label = UILabel()
            label.text = "Draft is empty".localized()
            label.font = UIFont(name: "Raleway-Medium", size: 18)
            label.textColor = AppColors.shared.gray
            collectionView.addSubview(label)
            label.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
            }
        }else{
            collectionView.reloadData()
        }
        
        
    }

    let blueView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.blue
        return v
    }()
    
    let viewTitle:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = .white
        label.text = "Drafts".localized()
        return label
    }()
    
    let activityIndicator:UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        i.style = .white
        return i
    }()
    
    lazy var backBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "baseline_keyboard_arrow_down_black_36pt"), for: .normal)
//        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
//            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_right_36pt"), for: .normal)
//        }else{
//            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left_36pt"), for: .normal)
//        }
        btn.tintColor = .white
        btn.addTargetClosure(closure: { (btn) in
            self.dismiss(animated: true, completion: nil)
        })
        return btn
    }()
    
    lazy var collectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let c = UICollectionView(frame: .zero, collectionViewLayout: layout)
        c.delegate = self
        c.dataSource = self
        c.backgroundColor = .white
        c.showsVerticalScrollIndicator = false
        c.alwaysBounceVertical = true
        return c
    }()
    
    let emptyLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = AppColors.shared.gray
       
        return label
    }()
    
    
    func setupView(){
        view.backgroundColor = .white
        view.addSubview(blueView)
        blueView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    //iPhone X, XS
                    make.height.equalTo(88)
                    
                case 2688:
                    //iPhone XS Max
                    make.height.equalTo(88)
                    
                case 1792:
                    //iPhone XR
                    make.height.equalTo(88)
                    
                default:
                    make.height.equalTo(64)
                }
            }
        }
        
        blueView.addSubview(viewTitle)
        viewTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(12)
        }
        
        blueView.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(8)
            make.height.width.equalTo(35)
        }
        
        blueView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(8)
            make.height.width.equalTo(35)
        }
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.right.left.bottom.equalToSuperview()
        }
        
    }
    
    
}


extension DraftViewController: UICollectionViewDataSource , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return draftPickeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DraftPreviewViewController()
        if draftPickeds[indexPath.item].is_video{
            vc.pickedType = .video
            vc.videoUrl = URL(string: draftPickeds[indexPath.item].url)
        }else{
            vc.pickedType = .image
            vc.imageUrl = URL(string: draftPickeds[indexPath.item].url)
        }
       
        vc.hero.isEnabled = true
        vc.tagLabel.text = draftPickeds[indexPath.item].tag
        vc.captionTextView.text = draftPickeds[indexPath.item].caption
        vc.commentsEnabled = draftPickeds[indexPath.item].comments_are_public
        vc.location = draftPickeds[indexPath.item].location
        vc.mentionedUsers = Array(draftPickeds[indexPath.item].mentioned_usernames)
        vc.previewDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = view.frame.width/2 - 24
        return CGSize(width: cellWidth, height: cellWidth + 108)// 12+16+48+8+20+4)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: draftCellId, for: indexPath) as! DraftCollectionViewCell
        cell.configure(draftPicked: draftPickeds[indexPath.item])
        if draftPickeds[indexPath.item].is_video{
            if let url = URL(string: draftPickeds[indexPath.row].url){
                let image = getThumbnailFrom(path: url)
                cell.thumbImage.image = image
            }
        }else{
            if let url = URL(string: draftPickeds[indexPath.row].url){
              cell.thumbImage.kf.setImage(with: url)
            }
        }
        

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
}


extension DraftViewController:DraftPreviewViewControllerDelegate{
    
    func delete(url: String) {
        let realm = try! Realm()
        guard let result = realm.objects(DraftPicked.self).filter("url == '" + url + "' ").first else {return}
        
        let index = draftPickeds.index { (draft) -> Bool in
            return result.url == draft.url
        }
    
        try! realm.write {
            realm.delete(result)
        }
        
        // Delete file from local storage
        let fileManager = FileManager.default
        if let url = URL(string: url){
            try? fileManager.removeItem(at: url)
        }
        
        let indexPath = IndexPath(item: index!, section: 0)
        draftPickeds.remove(at: index!)
        collectionView.deleteItems(at: [indexPath])
        
    }
  
}

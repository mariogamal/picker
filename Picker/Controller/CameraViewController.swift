//
//  CameraViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import KDCircularProgress
import CoreLocation
import AVFoundation
import GooglePlaces
import OneSignal
import RealmSwift
import Crashlytics

class CameraViewController: UIViewController, CLLocationManagerDelegate {
    
   
    var captureSession:AVCaptureSession?
    var VideoPreviewLayer:AVCaptureVideoPreviewLayer?
    var capturePhotoOutput:AVCapturePhotoOutput?
    var fileOutput : AVCaptureMovieFileOutput!
    var dataOutput : AVCaptureVideoDataOutput!
    var captureDevice:AVCaptureDevice?
    var videoInput:AVCaptureInput?
    var audioInput:AVCaptureInput?
    var captureConnection:AVCaptureConnection?
    var captureAudio :AVCaptureDevice?
    var photoSettings:AVCapturePhotoSettings?
    
    var sampleBufferGlobal : CMSampleBuffer?
    let writerFileName = "tempVideoAsset.mov"
    var presentationTime : CMTime!
    var outputSettings   = [String: Any]()
    var videoWriterInput: AVAssetWriterInput!
    var assetWriter: AVAssetWriter!
    
    let minimumZoom: CGFloat = 1.0
    let maximumZoom: CGFloat = 3.0
    var lastZoomFactor: CGFloat = 1.0
    
    var isRecording = false
    
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    var placesClient: GMSPlacesClient!
    var deepLinkNotification: NotificationStruct?
    
    func clearDiskCache() {
        let fileManager = FileManager.default
        let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        //let diskCacheStorageBaseUrl = myDocuments.appendingPathComponent("Documents")
        guard let filePaths = try? fileManager.contentsOfDirectory(at: myDocuments, includingPropertiesForKeys: nil, options: []) else { return }
        for filePath in filePaths {
            try? fileManager.removeItem(at: filePath)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let notification = deepLinkNotification{
            deepLinkNotification = nil
            if let tabBar = self.tabBarController as? appTabBarController{
                tabBar.selectNotificationTab()
                
                let nav = tabBarController?.viewControllers![3] as! UINavigationController
                let vc = nav.viewControllers.first as! NotificationViewController
                vc.deepLinkNotification = notification
            }

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        cameraAndMicrophoneAuthorization()
        placesClient = GMSPlacesClient.shared()
        
        photoSettings = AVCapturePhotoSettings()
        photoSettings?.flashMode = .off
        
        self.navigationController?.isNavigationBarHidden = true
        
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        
        //google places api
        updatePlace()
        _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(updatePlace), userInfo: nil, repeats: true)

    }
    
    
    func showWelcome(){
        DispatchQueue.main.async {
            self.welcomeLabel.alpha = 1
            self.messageLabel.alpha = 1
            self.enableCameraAccessBtn.alpha = 1
            self.enableMicrophoneAccessBtn.alpha = 1
            self.enableCameraAccessBtn.isUserInteractionEnabled = true
            self.enableMicrophoneAccessBtn.isUserInteractionEnabled = true
            self.progress.isUserInteractionEnabled = false
            self.switchCameraBtn.isUserInteractionEnabled = false
        }
    }
    
    
    func hideWelcome(){
        welcomeLabel.alpha = 0
        messageLabel.alpha = 0
        enableCameraAccessBtn.alpha = 0
        enableMicrophoneAccessBtn.alpha = 0
        enableCameraAccessBtn.isUserInteractionEnabled = false
        enableMicrophoneAccessBtn.isUserInteractionEnabled = false
        progress.isUserInteractionEnabled = true
        switchCameraBtn.isUserInteractionEnabled = true
    }
    
    func cameraAndMicrophoneAuthorization(){
       
        hideWelcome()
        // Check Camera authorization before preparing camera
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized camera
            let attrebutedString =  NSAttributedString(string: "Camera Access Enabled".localized(),
                                                       attributes: [NSAttributedString.Key.font : UIFont(name: "raleway-regular", size: 17)!,
                                                                    NSAttributedString.Key.foregroundColor : AppColors.shared.gray  ])
            enableCameraAccessBtn.setAttributedTitle(attrebutedString, for: .normal)
            enableCameraAccessBtn.isUserInteractionEnabled = false
            
            
            switch AVAudioSession.sharedInstance().recordPermission {
            case AVAudioSession.RecordPermission.granted:
                prepareCamera(position: .back)
            case AVAudioSession.RecordPermission.denied:
                self.showWelcome()
                break
            case AVAudioSession.RecordPermission.undetermined:
                AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                    self.prepareCamera(position: .back)
                })
            }
            
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    
                    DispatchQueue.main.async {
                        let attrebutedString =  NSAttributedString(string: "Camera Access Enabled".localized(),
                                                                   attributes: [NSAttributedString.Key.font : UIFont(name: "raleway-regular", size: 17)!,
                                                                                NSAttributedString.Key.foregroundColor : AppColors.shared.gray  ])
                        self.enableCameraAccessBtn.setAttributedTitle(attrebutedString, for: .normal)
                        self.enableCameraAccessBtn.isUserInteractionEnabled = false
                        self.showWelcome()
                    }
                    
                    
                    switch AVAudioSession.sharedInstance().recordPermission {
                    case AVAudioSession.RecordPermission.granted:
                        self.prepareCamera(position: .back)
                    case AVAudioSession.RecordPermission.denied:
                        self.showWelcome()
                        break
                    case AVAudioSession.RecordPermission.undetermined:
                        AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                            self.prepareCamera(position: .back)
                        })
                    }
                } else {
                    switch AVAudioSession.sharedInstance().recordPermission {
                    case AVAudioSession.RecordPermission.granted:
                        DispatchQueue.main.async {
                            let attrebutedString =  NSAttributedString(string: "Microphone Access Enabled".localized(),
                                                                       attributes: [NSAttributedString.Key.font : UIFont(name: "raleway-regular", size: 17)!,
                                                                                    NSAttributedString.Key.foregroundColor : AppColors.shared.gray  ])
                            self.enableMicrophoneAccessBtn.setAttributedTitle(attrebutedString, for: .normal)
                            self.enableMicrophoneAccessBtn.isUserInteractionEnabled = false
                            self.showWelcome()
                        }
                        
                    case AVAudioSession.RecordPermission.denied:
                        self.showWelcome()
                        break
                    case AVAudioSession.RecordPermission.undetermined:
                        AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                            if granted{
                                DispatchQueue.main.async {
                                    let attrebutedString =  NSAttributedString(string: "Microphone Access Enabled".localized(),
                                                                               attributes: [NSAttributedString.Key.font : UIFont(name: "raleway-regular", size: 17)!,
                                                                                            NSAttributedString.Key.foregroundColor : AppColors.shared.gray  ])
                                    self.enableMicrophoneAccessBtn.setAttributedTitle(attrebutedString, for: .normal)
                                    self.enableMicrophoneAccessBtn.isUserInteractionEnabled = false
                                    
                                }
                            }
                        })
                    }
                }
            })
        }
    }
    
    @objc func updatePlace(){
        
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    
                    // Using http request to get location in english
                    
                    //AIzaSyB-kFSUsAeOQD8nDgyDhxuqYMpZP2t1HHo -- Omarbasaleh2 account
                    //AIzaSyCzKfICoaHeoGUtK7IWpqoe5fOXTmhxhsE -- Vavisa account
                    
                    let url = URL(string: "https://maps.googleapis.com/maps/api/place/details/json?placeid="+place.placeID!+"&key=AIzaSyCzKfICoaHeoGUtK7IWpqoe5fOXTmhxhsE&language=en")
                    
                    let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
                        guard let data = data else{return}
                        guard let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] else{ return }
                        guard let result = json["result"] as? [String:Any] else{return}
                        guard let addressComponents = result["address_components"] as? [[String:Any]] else {return}
                        for field in addressComponents {
                            if let types = field["types"] as? [String]{
                                if types.contains("locality"){
                                    if let cityName = field["long_name"]as? String{
                                        DispatchQueue.main.async {
                                            self.locationLabel.text = cityName
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    task.resume()
                    
                }
            }
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        UIApplication.shared.isStatusBarHidden = true
        captureSession?.startRunning()
        self.showElements()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        UIApplication.shared.isStatusBarHidden = false
    }
    
    
    lazy var cameraView:UIView = {
        let v = UIView()
        v.backgroundColor = .black
        return v
    }()
    
    let welcomeLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "raleway-bold", size: 20)
        label.textColor = .white
        label.text = "Welcome to Picker".localized()
        return label
    }()
    
    let messageLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "raleway-regular", size: 15)
        label.textColor = AppColors.shared.superLightGray
        label.text = "Enable access so you can start taking photos and videos".localized()
        return label
    }()
    
    let enableCameraAccessBtn:UIButton = {
        let btn = UIButton()
        let attrebutedString =  NSAttributedString(string: "Enable Camera Access".localized(),
                                                   attributes: [NSAttributedString.Key.font : UIFont(name: "raleway-regular", size: 17)!,
                                                                NSAttributedString.Key.foregroundColor : AppColors.shared.blue  ])
        btn.setAttributedTitle(attrebutedString, for: .normal)
        btn.addTargetClosure(closure: { (_) in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        return btn
    }()
    
    let enableMicrophoneAccessBtn:UIButton = {
        let btn = UIButton()
        let attrebutedString =  NSAttributedString(string: "Enable Microphone Access".localized(),
                                                   attributes: [NSAttributedString.Key.font : UIFont(name: "raleway-regular", size: 17)!,
                                                                NSAttributedString.Key.foregroundColor : AppColors.shared.blue  ])
        btn.setAttributedTitle(attrebutedString, for: .normal)
        btn.addTargetClosure(closure: { (_) in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        return btn
    }()
 
    
    
    lazy var switchCameraBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "Switch camera"), for: .normal)
        btn.addTarget(self, action: #selector(swithCamera), for: .touchUpInside)
        btn.adjustsImageWhenHighlighted = false
        return btn
    }()
    
    let sendIndicator:UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        return i
    }()
    
    lazy var flashBtn:UIButton = {
        let btn = UIButton()   
        btn.tintColor = .white
        btn.setImage(#imageLiteral(resourceName: "Flash off"), for: .normal)
        btn.dropShadowLow()
        btn.addTargetClosure(closure: { (_) in
            switch self.photoSettings?.flashMode{
            case .on?:
                 btn.setImage(#imageLiteral(resourceName: "Flash off"), for: .normal)
                 self.photoSettings?.flashMode = .off
            case .off?:
                 btn.setImage(#imageLiteral(resourceName: "Flash on"), for: .normal)
                 self.photoSettings?.flashMode = .on
            case .none:
                break
            case .some(.auto):
                break
            }
        })
        return btn
    }()
    
    
    lazy var progress:KDCircularProgress = {
        let p = KDCircularProgress()
        p.dropShadow()
        p.isUserInteractionEnabled = false
        p.progressThickness = 0.3
        p.trackThickness = 0.3
        p.trackColor = .white
        p.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(startStopRecording)))
        p.isUserInteractionEnabled = true
        p.set(colors: AppColors.shared.blue)
        return p
    }()
    
    let locationImage:UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Location")
        iv.contentMode = .scaleAspectFit
        iv.tintColor = .white
        iv.dropShadowLow()
        return iv
    }()
    
    let locationLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        label.text = "-"
        label.font = UIFont(name: "Raleway-Medium", size: 16)
        label.dropShadowLow()
        return label
    }()
    
    let bottomView:UIImageView = {
        let v = UIImageView()
        v.image = #imageLiteral(resourceName: "Iphone_x_bottom")
        return v
    }()
    
    
    func setupViews(){
        //Views
        view.addSubview(cameraView)
        cameraView.addSubview(welcomeLabel)
        cameraView.addSubview(messageLabel)
        cameraView.addSubview(enableCameraAccessBtn)
        cameraView.addSubview(enableMicrophoneAccessBtn)
        view.addSubview(progress)
        view.addSubview(switchCameraBtn)
        view.addSubview(sendIndicator)
        view.addSubview(flashBtn)
        view.addSubview(locationImage)
        view.addSubview(locationLabel)
        
        
    
        
        //gusters
        cameraView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(captureImage)))
        cameraView.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(cameraZoom(sender:))))
        
        cameraView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        messageLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-32)
            make.right.left.equalToSuperview().inset(16)
        }
        
        welcomeLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(messageLabel.snp.top).offset(-16)
        }
        
        enableCameraAccessBtn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(messageLabel.snp.bottom).offset(16)
        }
        
        enableMicrophoneAccessBtn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(enableCameraAccessBtn.snp.bottom).offset(16)
        }

        progress.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(17)
            } else {
                make.bottom.equalTo(view.snp.bottom).offset(-32)
            }
            make.width.height.equalTo(80)
        }
        
        switchCameraBtn.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                    case 2436:
                        // iPhoneX
                         make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(0)
                    default:
                         make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(8)
                    }
                }
               
            } else {
                make.top.equalTo(view.snp.top).offset(24)
            }
            
            make.right.equalToSuperview().offset(-16)
            make.width.height.equalTo(30)
        }
        
        sendIndicator.snp.makeConstraints { (make) in
            make.top.equalTo(switchCameraBtn.snp.bottom).offset(8)
            make.centerX.equalTo(switchCameraBtn)
        }
        
        flashBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(switchCameraBtn)
            make.right.equalTo(switchCameraBtn.snp.left).offset(-16)
            make.width.height.equalTo(30)
        }
        
        
        locationImage.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                if UIDevice().userInterfaceIdiom == .phone {
                    switch UIScreen.main.nativeBounds.height {
                    case 2436:
                        // iPhoneX
                        make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(0)
                    default:
                        make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(8)
                    }
                }
                
            } else {
                make.top.equalTo(view.snp.top).offset(24)
            }
            make.left.equalToSuperview().offset(16)
            make.width.height.equalTo(25)
        }
        
        
        locationLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(locationImage)
            make.left.equalTo(locationImage.snp.right).offset(4)
        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                // iPhoneX
                view.addSubview(bottomView)
                bottomView.snp.makeConstraints { (make) in
                    make.left.right.bottom.equalToSuperview()
                    make.height.equalTo(34)
                }
            default:
                break;
            }
        }
        
    }
    
   
    
    
    
    func prepareCamera(position:AVCaptureDevice.Position){
        
        
            guard let capDevice = AVCaptureDevice.default(.builtInWideAngleCamera , for: AVMediaType.video, position: position) else{return}
            guard let audDevice = AVCaptureDevice.default(for: AVMediaType.audio) else{return}
            captureDevice = capDevice
            DispatchQueue.main.async {
                self.progress.isUserInteractionEnabled = true
                self.switchCameraBtn.isUserInteractionEnabled = true
                self.enableCameraAccessBtn.isUserInteractionEnabled = false
                self.enableMicrophoneAccessBtn.isUserInteractionEnabled = false
            }
        
        
        
        
            
            do {
                // For background music
//                let sess = AVAudioSession.sharedInstance()
//                if sess.isOtherAudioPlaying {
//                    do{
//                        _ = try sess.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .mixWithOthers)
//                        _ = try sess.setMode(AVAudioSessionModeVideoRecording)
//                        _ = try sess.setActive(true, with: [])
//
//                    }catch {
//                        print(error)
//                    }
//
//                }
                
                videoInput = try AVCaptureDeviceInput(device: captureDevice!)
                audioInput = try AVCaptureDeviceInput(device: audDevice)
                
                captureSession = AVCaptureSession()
                if (captureSession?.canAddInput(videoInput!))!{
                     captureSession?.addInput(videoInput!)
                }
                
                captureSession?.addInput(audioInput!)
                
            } catch let err {
                print(err)
            }
            
            VideoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            VideoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            DispatchQueue.main.async {
                self.VideoPreviewLayer?.frame = self.view.frame
                self.cameraView.layer.addSublayer(self.VideoPreviewLayer!)
            }
        
            captureSession?.sessionPreset = AVCaptureSession.Preset.hd1280x720
        
            
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            
            fileOutput = AVCaptureMovieFileOutput()
            dataOutput = AVCaptureVideoDataOutput()
        
            
            captureSession?.addOutput(capturePhotoOutput!)
            captureSession?.addOutput(fileOutput)
        
            captureSession?.startRunning()
     
    }
    
    @objc func swithCamera(){
        if isRecording{
            
//            guard let capDeviceFront = AVCaptureDevice.default(.builtInWideAngleCamera , for: AVMediaType.video, position: .front) else{return}
//            do {
//                //captureSession.
//                captureSession?.stopRunning()
//                captureSession?.beginConfiguration()
//                captureSession?.removeInput(self.videoInput!)
//
//                videoInput = try AVCaptureDeviceInput(device: capDeviceFront)
//                //captureSession?.addInputWithNoConnections(videoInput!)
//                //let captureConnection2 = AVCaptureConnection(inputPorts: (videoInput?.ports)!, output: fileOutput)
//                captureSession?.addInput(videoInput!)
//
//                captureSession?.commitConfiguration()
//                captureSession?.startRunning()
//                //captureSession?.add(captureConnection2)
//
//                //captureSession?.addOutputWithNoConnections(fileOutput!)
//
//
//            } catch let err {
//                print(err)
//            }
//
            
            //captureSession?.removeOutput(fileOutput!)
        }else{
            captureDevice?.position == .back ? prepareCamera(position: .front) : prepareCamera(position: .back)
        }
    }
    
    
    
    @objc func startStopRecording(){
        if isRecording{
            fileOutput.stopRecording()
            isRecording = false
        }else{
            captureVideo()
            hideElements()
            isRecording = true
        }
    }
    
    func hideElements(){
        tabBarController?.tabBar.isHidden = true
        flashBtn.isHidden = true
        switchCameraBtn.isHidden = true
        for gesture in cameraView.gestureRecognizers!
        {
            if let recognizer = gesture as? UITapGestureRecognizer {
                cameraView.removeGestureRecognizer(recognizer)
            }
        }
    }
    
    func showElements(){
        tabBarController?.tabBar.isHidden = false
        flashBtn.isHidden = false
        switchCameraBtn.isHidden = false
        cameraView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(captureImage)))
    }
    
    @objc func captureVideo(){
        print("VIDEO")
        progress.animate(toAngle: 360, duration: 15) { (true) in
            self.fileOutput.stopRecording()
            self.isRecording = false
            self.progress.angle = 0
        }
       
        let fileName = String(NSDate().timeIntervalSince1970) + ".mov" //"tmp.mov"
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let filePath = documentsURL.appendingPathComponent(fileName)
        
        var videoConnection:AVCaptureConnection?
        
        
        // Add mirrorig to front records
        if captureDevice?.position == .front{
            for connection in self.fileOutput.connections {
                for port in connection.inputPorts {
                    if port.mediaType == AVMediaType.video {
                        videoConnection = connection as AVCaptureConnection
                        
                        if videoConnection!.isVideoMirroringSupported {
                            videoConnection!.isVideoMirrored = true
                        }
                    }
                }
            }
        }
        
        let recordingDelegate:AVCaptureFileOutputRecordingDelegate? = self
        fileOutput.startRecording(to: filePath, recordingDelegate: recordingDelegate!)
    }
    
    
    @objc func captureImage(sender: UITapGestureRecognizer){
        
        
        for gesture in cameraView.gestureRecognizers!
        {
            if let recognizer = gesture as? UITapGestureRecognizer {
                cameraView.removeGestureRecognizer(recognizer)
            }
        }
        
        guard let capturePhotoOutput = self.capturePhotoOutput else { return }
        let settings = AVCapturePhotoSettings()
        settings.isAutoStillImageStabilizationEnabled = true
        settings.isHighResolutionPhotoEnabled = true
        if captureDevice?.position == .front{
            settings.flashMode = .off
        }else{
            settings.flashMode = (self.photoSettings?.flashMode)!
        }
        
        
        capturePhotoOutput.capturePhoto(with: settings, delegate: self)
        
    }
    
    func toggleFlash() {
        if let device = AVCaptureDevice.default(for: AVMediaType.video), device.hasTorch {
            do {
                try device.lockForConfiguration()
                let torchOn = !device.isTorchActive
                try device.setTorchModeOn(level: 1.0)
                device.torchMode = torchOn ? .on : .off
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
    
    @objc func cameraZoom(sender: UIPinchGestureRecognizer) {
        guard let device = captureDevice else { return }
        // Return zoom value between the minimum and maximum zoom values
        func minMaxZoom(_ factor: CGFloat) -> CGFloat {
            return min(min(max(factor, minimumZoom), maximumZoom), device.activeFormat.videoMaxZoomFactor)
        }
        
        func update(scale factor: CGFloat) {
            do {
                try device.lockForConfiguration()
                defer { device.unlockForConfiguration() }
                device.videoZoomFactor = factor
            } catch {
                print("\(error.localizedDescription)")
            }
        }
        
        let newScaleFactor = minMaxZoom(sender.scale * lastZoomFactor)
        
        switch sender.state {
        case .began: fallthrough
        case .changed: update(scale: newScaleFactor)
        case .ended:
            lastZoomFactor = minMaxZoom(newScaleFactor)
            update(scale: lastZoomFactor)
        default: break
        }
        
    }
    
}


extension CameraViewController : AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        cameraView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(captureImage)))
        
        guard error == nil,
            let photoSampleBuffer = photoSampleBuffer else{
                print("Error capturing photo")
                return
        }
        
        guard let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else{return}
        
        let capturedImage = UIImage(data: imageData )
       
        
        if let image = capturedImage{
            print(image)
            
            
            var image2 = image
            let vc = PreviewViewController()
            vc.pickedType = .image
            vc.delegate = self
            if captureDevice?.position == .front{
                image2 = UIImage(cgImage: (image2.cgImage!), scale: (image2.scale), orientation: .leftMirrored)
                
            }
            
            
            guard let data = image2.jpegData(compressionQuality: 1) else {return}
            let fileName = String(NSDate().timeIntervalSince1970) + ".png" //"tmp.mov"
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let filePath = documentsURL.appendingPathComponent(fileName)
            try? data.write(to: filePath)
            //vc.pickedImage.image = image2
            vc.imageUrl = filePath
            vc.location = self.locationLabel.text!
            captureSession?.stopRunning()
            self.present(vc, animated: false, completion: nil)
        }
    }
    
}

extension CameraViewController: AVCaptureDepthDataOutputDelegate{
    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection){
        print(sampleBuffer)
        
    }
}


extension CameraViewController : AVCaptureFileOutputRecordingDelegate {
    

    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        print("Start")
        
        if self.photoSettings?.flashMode == .on{
            if captureDevice?.position == .back{
                toggleFlash()
            }
        }
    }
    
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        
        if self.photoSettings?.flashMode == .on{
            if captureDevice?.position == .back{
                toggleFlash()
            }
        }
        
        guard let data = NSData(contentsOf: outputFileURL as URL) else {
            return
        }
        print("File size before compression: \(Double(data.length / 1048576)) mb")
        
        let vc = PreviewViewController()
        vc.pickedType = .video
        vc.delegate = self
        vc.videoUrl = outputFileURL
        vc.location = self.locationLabel.text!
        captureSession?.stopRunning()
        self.present(vc, animated: false, completion: nil)
    
    }
   
}


extension CameraViewController:PreviewViewControllerDelegate{
    
    func uploadPicked(userId: Int, url: URL, is_video: Bool, location: String, tag: String, caption: String, comments_are_public: Bool, mentioned_usernames: [String]) {
        self.sendIndicator.startAnimating()
        
        if is_video{
            /* Upload video picked */
            
            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
            compressVideo(inputURL: url as URL, outputURL: compressedURL) { (exportSession) in
                guard let session = exportSession else {
                    return
                }
                
                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = NSData(contentsOf: compressedURL) else {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        do{
                            let videoData = try Data(contentsOf: compressedURL)
                            let videoStr = videoData.base64EncodedString(options: .lineLength64Characters)
                            
                            guard let thumbImage = self.getThumbnailFrom(path: url) else {return}
                            guard let thumbData = thumbImage.jpegData(compressionQuality: 0.0) else {return}
                            let thumbStr = thumbData.base64EncodedString(options: .lineLength64Characters)
                            
                            APIClient.uploadPicked(userId: userId, photo: videoStr,thumb: thumbStr, is_video: "1" ,location: location, tag: tag, caption: caption, comments_are_public: comments_are_public , mentioned_usernames: mentioned_usernames) { (error) in
                                
                                DispatchQueue.main.async {
                                    self.sendIndicator.stopAnimating()
                                }
                                
                                GlobalFunctions.updatePicked(profile_user_id: userId, tabBarController: self.tabBarController)
                                
                                if let err = error{
                                    print(err)
                                    
                                    let draftPicked = DraftPicked()
                                    draftPicked.url = url.absoluteString
                                    draftPicked.caption = caption
                                    draftPicked.comments_are_public = comments_are_public
                                    draftPicked.tag = tag
                                    draftPicked.is_video = is_video
                                    draftPicked.location = location
                                    for username in mentioned_usernames{
                                        draftPicked.mentioned_usernames.append(username)
                                    }
                                    
                                    
                                    let realm = try! Realm()
                                    try! realm.write {
                                        realm.add(draftPicked)
                                    }
                                    
                                    let alert = UIAlertController(title: "Connection Lost, Picked moved to draft.".localized(), message: nil, preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                    alert.addAction(action)
                                    self.present(alert, animated: true, completion: nil)
                                    return
                                }
                                
                            }
                        }catch{
                            print(error)
                        }
                        
                    }
                    
                    print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                case .failed:
                    break
                case .cancelled:
                    break
                }
            }
            
            
            
            
            
        }else{
            /* Upload image picked */
            
            do{
                let imageData = try Data(contentsOf: url)
                guard let image = UIImage(data: imageData) else {return}
                guard let imageDataCompressed = image.jpegData(compressionQuality: 0.0) else {return}
                
                let imageStr = imageDataCompressed.base64EncodedString(options: .lineLength64Characters)
                APIClient.uploadPicked(userId: userId, photo: imageStr, thumb:"" , is_video: "0" ,location: location, tag: tag, caption: caption, comments_are_public: comments_are_public , mentioned_usernames: mentioned_usernames) { (error) in
                    
                    DispatchQueue.main.async {
                        self.sendIndicator.stopAnimating()
                    }
                    
                     GlobalFunctions.updatePicked(profile_user_id: userId, tabBarController: self.tabBarController)
                    
                    if let err = error{
                        print(err)
                        
                        let draftPicked = DraftPicked()
                        draftPicked.url = url.absoluteString
                        draftPicked.caption = caption
                        draftPicked.comments_are_public = comments_are_public
                        draftPicked.tag = tag
                        draftPicked.is_video = is_video
                        draftPicked.location = location
                        for username in mentioned_usernames{
                            draftPicked.mentioned_usernames.append(username)
                        }
                        
                        
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(draftPicked)
                        }
                        
                        let alert = UIAlertController(title: "Connection Lost, Picked moved to draft.".localized(), message: nil, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }
            }catch{
                print(error)
            }
        }
        
        
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            print("Finish compress")
            handler(exportSession)
        }
    }
}




    



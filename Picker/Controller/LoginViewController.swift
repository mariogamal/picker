//
//  LoginViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/23/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import Hero
import OneSignal

class LoginViewController: UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        
    }
    
    let scrollView:UIScrollView = {
        let sv = UIScrollView()
        return sv
    }()
    
    let contentView:UIView = {
        let v = UIView()
        return v
    }()
    
    
    let logoImage:UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Login")
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let emailTF:customTextField = {
        let tf = customTextField()
        tf.setLeftIcon(#imageLiteral(resourceName: "Account"))
        tf.placeholder = "Username or email".localized()
        return tf
    }()
    
    let passwordTF:customTextField = {
        let tf = customTextField()
        tf.placeholder = "Password".localized()
        tf.isSecureTextEntry = true
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_lock_outline_36pt"))
        return tf
    }()
    
    let loginBtn:BlueShadowButton = {
        let btn = BlueShadowButton()
        btn.setTitle("Login".localized(), for: .normal)
        btn.addTarget(self, action: #selector(login), for: .touchUpInside)
        return btn
    }()
    
    lazy var  noAccountBtn:UIButton = {
        let btn = UIButton()
        btn.setTitleColor(AppColors.shared.blue, for: .normal)
        btn.titleLabel?.font = UIFont(name: "Raleway-Medium", size: 14)
        btn.setTitle("Don't have an account? register here.".localized(), for: .normal)
        btn.addTargetClosure(closure: { (_) in
            self.contentView.endEditing(true)
            let registerVC = RegisterViewController()
            registerVC.hero.isEnabled = true
            registerVC.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            self.present(registerVC, animated: true, completion: nil)
        })
        return btn
    }()
    
    
    lazy var forgetPasswordBtn:UIButton = {
        let btn = UIButton()
        btn.setTitleColor(AppColors.shared.blue, for: .normal)
        btn.titleLabel?.font = UIFont(name: "Raleway-Medium", size: 14)
        btn.setTitle("Forget your password?".localized(), for: .normal)
        btn.addTargetClosure(closure: { (_) in
            self.contentView.endEditing(true)
            let resetPasswordVC = ResetPasswordViewController()
            resetPasswordVC.hero.isEnabled = true
            resetPasswordVC.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            self.present(resetPasswordVC, animated: true, completion: nil)
            
        })
        return btn
    }()
    

    let indicator:UIActivityIndicatorView = {
        let v = UIActivityIndicatorView()
        v.color = .white
        return v
    }()
    
    let errorLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont(name: "Raleway-Medium", size: 12)
        label.textColor = AppColors.shared.red
        return label
    }()
    
    func setupViews(){
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(logoImage)
        contentView.addSubview(emailTF)
        contentView.addSubview(passwordTF)
        contentView.addSubview(loginBtn)
        contentView.addSubview(forgetPasswordBtn)
        contentView.addSubview(errorLabel)
        view.addSubview(noAccountBtn)
        loginBtn.addSubview(indicator)
        
        
        for textField in [emailTF, passwordTF]{
            if Bundle.main.preferredLocalizations.first == "en"{
                textField.textAlignment = .left
            }else{
                textField.textAlignment = .right
            }
        }
        
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.left.equalTo(view)
        }
        
        
        
        emailTF.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(view.frame.height/2)
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        emailTF.addTarget(self, action: #selector(lowercase), for: .editingChanged)
        
        logoImage.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(emailTF.snp.bottom).offset(-view.frame.height/8)
        }
        
        
        
        passwordTF.snp.makeConstraints { (make) in
            make.top.equalTo(emailTF.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        loginBtn.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTF.snp.bottom).offset(32)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/2)
            make.height.equalTo(view.frame.width/8)
            make.bottom.equalToSuperview().offset(-48)
        }
        
        noAccountBtn.snp.makeConstraints { (make) in
            make.width.equalTo(view.frame.width/1.2)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-16)
        }
        
        indicator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(25)
        }
        
        
        forgetPasswordBtn.snp.makeConstraints { (make) in
            make.top.equalTo(loginBtn.snp.bottom).offset(16)
            make.left.right.equalToSuperview().inset(8)
        }
        
        errorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(forgetPasswordBtn.snp.bottom).offset(16)
            make.left.right.equalToSuperview().inset(8)
        }
    }
    
    
    @objc func lowercase(){
        emailTF.text = emailTF.text?.lowercased()
    }
    
    @objc func login(){
        guard let email = emailTF.text?.lowercased(), let password = passwordTF.text
            else {return}
        
        for textField in [emailTF, passwordTF]{
            if Bundle.main.preferredLocalizations.first == "en"{
                if textField.text == ""{
                    errorLabel.text = textField.placeholder! + " is required"
                    return
                }
            }else{
                if textField.text == ""{
                    errorLabel.text = textField.placeholder! + " مطلوب"
                    return
                }
            }
        }
       
        indicator.startAnimating()
        self.loginBtn.isUserInteractionEnabled = false
        self.loginBtn.setTitle("", for: .normal)
        let token = UserDefaults.standard.value(forKey: "token") as! String
        
        APIClient.login(email: email, password: password, token: token) { (user, error) in
            
            self.indicator.stopAnimating()
            self.view.endEditing(true)
            self.loginBtn.isUserInteractionEnabled = true
            self.loginBtn.setTitle("Login".localized(), for: .normal)
            OneSignal.setSubscription(true)
            
            if let err = error{
                self.errorLabel.text = err
            }else{
                UserDefaults.standard.set(user.id, forKey: "userId")
                if let window = UIApplication.shared.keyWindow{
                    window.rootViewController = appTabBarController(deepLinkId: nil)
                }
                print(user)
            }
        }
        
    }
}

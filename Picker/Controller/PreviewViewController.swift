//
//  ImagePreviewViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/29/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import AVKit
import AVFoundation
import GrowingTextView
import SwiftRichString
import RealmSwift

enum PickedType:String{
    case image = "image"
    case video = "video"
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

protocol PreviewViewControllerDelegate {
    func uploadPicked(userId: Int, url: URL, is_video: Bool ,location: String, tag: String , caption: String , comments_are_public: Bool , mentioned_usernames: [String])
}

class PreviewViewController: UIViewController, UITextFieldDelegate {
    
    var videoUrl:URL?
    var imageUrl:URL?
    var pickedType:PickedType!
    var delegate:PreviewViewControllerDelegate?
    var replayObserver:Any?
    var playerLayer:AVPlayerLayer?
    var videoPlayer:AVPlayer?
    var commentsEnabled: Bool = true
    var mentionedUsers = [String]()
    var location: String = ""
    var hashtagsTableViewHeightConstraint: Constraint!
    var hashtags = [String]()
    var tagText: String = ""{
        didSet{
            tagLabel.text = tagText
            removeTagButton.isHidden = (tagText == "")
        }
    }
    
    var caption: String = ""{
        didSet{
            captionTextView.text = caption
            captionTVCounter.text = "\(caption.count)"
            if caption.count != 0 {
                captionTVCounter.text = String(caption.count)
            }else{
                captionTVCounter.text = ""
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Reply video
        replayObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer?.currentItem, queue: nil) { notification in
            self.videoPlayer?.seek(to: CMTime.zero)
            self.videoPlayer?.play()
        }
        
        if pickedType == .image{
           pickedImage.kf.setImage(with: imageUrl)
        }
        
        locationLabel.text = location
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if pickedType == .video{
            videoPlayer?.play()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        UIApplication.shared.isStatusBarHidden = true

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        UIApplication.shared.isStatusBarHidden = false
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillAppear() {
        //Do something here
    }
    
    @objc func keyboardWillDisappear() {
        hideCaptionTF()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    let pickedImage:UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    lazy var videoView:UIView = {
        let v = UIView()
        return v
    }()
    
    lazy var cancelBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "ic_close_36pt"), for: .normal)
        btn.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return btn
    }()
    
    
    @objc func handleCancel(){
        if self.pickedType! == .video{
            self.videoPlayer?.pause()
            NotificationCenter.default.removeObserver(self.replayObserver ?? "")
            self.videoPlayer?.replaceCurrentItem(with: nil)
            self.dismiss(animated: false, completion:nil)
            
            // Delete file from local storage
            let fileManager = FileManager.default
            if let url = URL(string: self.videoUrl!.absoluteString){
                try? fileManager.removeItem(at: url)
            }
        }
        else{
            self.dismiss(animated: false, completion: nil)
            
            // Delete file from local storage
            let fileManager = FileManager.default
            if let url = URL(string: self.imageUrl!.absoluteString){
                try? fileManager.removeItem(at: url)
            }
        }
    }
    
    lazy var locationImage:UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Location")
        iv.isUserInteractionEnabled = true
        iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleLocationTapped)))
        iv.contentMode = .scaleAspectFit
        iv.tintColor = .white
        iv.dropShadowLow()
        return iv
    }()
    
    lazy var locationLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleLocationTapped)))
        label.text = "Location"
        label.font = UIFont(name: "Raleway-Medium", size: 16)
        label.dropShadowLow()
        return label
    }()
    
    @objc func handleLocationTapped(){
        if locationLabel.text == "-"{
            locationLabel.text = location
        }else{
            locationLabel.text = "-"
        }
    }
    
    
    lazy var tagBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "Tag"), for: .normal)
        btn.addTargetClosure(closure: { (_) in
            let vc = TagViewController()
            vc.tagDelegate = self
            if let location = self.locationLabel.text{
                vc.location = location
            }
            if self.pickedType == .video{
                self.videoPlayer?.pause()
            }
            
            let nav = UINavigationController(rootViewController: vc)
            self.present(nav, animated: true, completion: nil)
        })
        return btn
    }()
    
    lazy var downloadBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "Download"), for: .normal)
        btn.addTargetClosure(closure: { (_) in
            switch self.pickedType{
            case .image?:
                    UIImageWriteToSavedPhotosAlbum(self.pickedImage.image!, nil, nil, nil)
            case .video?:
                if let url = self.videoUrl{
                    UISaveVideoAtPathToSavedPhotosAlbum(url.path, nil, nil, nil)
                }
            case .none:
                break
            }
            self.showSaved()
        })
        return btn
    }()
    
    
    lazy var captionBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "Caption"), for: .normal)
        btn.addTarget(self, action: #selector(showCaptionTF), for: .touchUpInside)
        return btn
    }()
    
    lazy var removeTagButton:UIButton = {
        let btn = UIButton()
        btn.isHidden = true
        btn.tintColor = .white
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "ic_close_36pt"), for: .normal)
        btn.addTargetClosure(closure: { [unowned self] (_) in
            self.tagText = ""
        })
        return btn
    }()
    
    lazy var commentBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "Comment_enabeled"), for: .normal)
        btn.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        return btn
    }()
    
    lazy var hashtagsTableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.layer.cornerRadius = 15
        tableView.clipsToBounds = true
        tableView.separatorInset = .zero
        tableView.separatorColor = AppColors.shared.lightGray
        return tableView
    }()
    
    var tagLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Raleway-Medium", size: 16)
        label.dropShadowLow()
        return label
    }()
    
    lazy var captionTextView:GrowingTextView = {
        let tv = GrowingTextView()
        //tf.placeholder = "Add caption".localized()
        tv.textContainer.maximumNumberOfLines = 10
        tv.textContainer.lineBreakMode = .byTruncatingTail
        tv.alpha = 0
        tv.keyboardType = UIKeyboardType.twitter
        tv.isSelectable = true
        tv.isEditable = true
        tv.font = UIFont(name: "Raleway-Medium", size: 17)
        tv.backgroundColor = .clear
        tv.textColor = .white
        tv.layer.borderColor = UIColor.white.cgColor
        tv.layer.borderWidth = 1
        tv.maxLength = 300
        return tv
    }()
    
    let captionTVCounter:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 17)
        label.textColor = .white
        return label
    }()
    
    lazy var sendBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.backgroundColor = AppColors.shared.blue
        btn.dropShadowLow()
        btn.setImage(#imageLiteral(resourceName: "ic_send"), for: .normal)
        btn.addTarget(self, action: #selector(uploadPicked), for: .touchUpInside)
        return btn
    }()
    
    let sendIndicator:UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        return i
    }()
    
    let bottomShade:UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "dark_shade")
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    @objc func showCaptionTF(){
        captionBtn.alpha = 0
        captionTextView.alpha = 1
        captionTextView.becomeFirstResponder()
    }
    
    func hideCaptionTF(){
        if captionTextView.text == ""{
            captionBtn.alpha = 1
            captionTextView.alpha = 0
        }
    }
    
    @objc func handleComment(){
        commentsEnabled = !commentsEnabled
        if commentsEnabled{
            commentBtn.setImage(#imageLiteral(resourceName: "Comment_enabeled"), for: .normal)
        }else{
            commentBtn.setImage(#imageLiteral(resourceName: "Comment disabeled"), for: .normal)
        }
    }
    
    @objc func uploadPicked(){
        sendIndicator.startAnimating()
        sendBtn.setImage(nil, for: .normal)
        let userId = UserDefaults.standard.integer(forKey: "userId")
        guard let location = locationLabel.text else { return }
        
        switch pickedType! {
        case .image:
            if let url = imageUrl{
                self.dismiss(animated: false) {
                    self.delegate?.uploadPicked(userId: userId, url: url, is_video: false, location: location, tag: self.tagText, caption: self.captionTextView.text ?? "", comments_are_public: self.commentsEnabled, mentioned_usernames: self.mentionedUsers)
                }
                
            }

        case.video:
            if let url = videoUrl{
                self.videoPlayer?.pause()
                NotificationCenter.default.removeObserver(self.replayObserver ?? "")
                self.videoPlayer?.replaceCurrentItem(with: nil)
                self.dismiss(animated: false) {
                    self.delegate?.uploadPicked(userId: userId, url: url, is_video: true, location: location, tag: self.tagText, caption: self.captionTextView.text ?? "", comments_are_public: self.commentsEnabled, mentioned_usernames: self.mentionedUsers)
                }
            }
        }
        
    }

    func setupViews(){
        if let url = videoUrl{
            videoPlayer = AVPlayer(url: url)
            playerLayer = AVPlayerLayer(player: self.videoPlayer)
            playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            playerLayer?.frame = self.view.bounds
            self.view.layer.addSublayer(playerLayer!)
            videoPlayer?.play()
        }else{
            view.addSubview(pickedImage)
            pickedImage.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
            }
        }
        
        view.addSubview(bottomShade)
        bottomShade.snp.makeConstraints { (make) in
            make.bottom.left.right.equalToSuperview()
        }
        
        view.addSubview(sendBtn)
        sendBtn.snp.makeConstraints { (make) in
            make.bottom.right.equalToSuperview().inset(16)
            make.width.height.equalTo(40)
        }
        sendBtn.layer.cornerRadius = 20
        sendBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        sendBtn.addSubview(sendIndicator)
        sendIndicator.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.addSubview(downloadBtn)
        downloadBtn.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(16)
            make.centerY.equalTo(sendBtn.snp.centerY).offset(-4)
            make.width.height.equalTo(35)
        }
        
        view.addSubview(commentBtn)
        commentBtn.snp.makeConstraints { (make) in
            make.left.equalTo(downloadBtn.snp.right).offset(8)
            make.centerY.equalTo(sendBtn.snp.centerY)
            make.width.height.equalTo(35)
        }
        
        view.addSubview(captionBtn)
        captionBtn.snp.makeConstraints { (make) in
            make.left.equalTo(commentBtn.snp.right).offset(8)
            make.centerY.equalTo(sendBtn.snp.centerY).offset(-2)
            make.width.height.equalTo(40)
        }
        
        view.addSubview(captionTextView)
        captionTextView.delegate = self
        captionTextView.snp.makeConstraints { (make) in
            make.bottom.equalTo(commentBtn)
            make.left.equalTo(commentBtn.snp.right).offset(8)
            make.right.equalTo(sendBtn.snp.left).offset(-8)
            //make.height.greaterThanOrEqualTo(40)
        }
        captionTextView.sizeThatFits(CGSize(width: captionTextView.frame.size.width, height: captionTextView.frame.size.height))
        captionTextView.layer.cornerRadius = 18
        
        view.addSubview(hashtagsTableView)
        hashtagsTableView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(captionTextView.snp.top).offset(-24)
            self.hashtagsTableViewHeightConstraint = make.height.equalTo(0).constraint
        }
        
        view.addSubview(captionTVCounter)
        captionTVCounter.snp.makeConstraints { (make) in
            make.left.equalTo(captionTextView).inset(8)
            make.bottom.equalTo(captionTextView.snp.top).offset(-4)
        }
        
        view.addSubview(tagBtn)
        tagBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(24)
            make.right.equalToSuperview().offset(-16)
            make.width.height.equalTo(30)
        }
        
        view.addSubview(tagLabel)
        tagLabel.snp.makeConstraints { (make) in
            make.top.equalTo(tagBtn.snp.bottom).offset(4)
            make.right.equalTo(tagBtn.snp.right)
        }
        
        view.addSubview(removeTagButton)
        removeTagButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(tagLabel)
            make.right.equalTo(tagLabel.snp.left).offset(-8)
            make.height.width.equalTo(15)
        }
        
        view.addSubview(locationLabel)
        locationLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(tagBtn.snp.centerY)
        }
        
        view.addSubview(locationImage)
        locationImage.snp.makeConstraints { (make) in
            make.centerY.equalTo(locationLabel.snp.centerY)
            make.right.equalTo(locationLabel.snp.left).offset(-4)
            make.width.height.equalTo(25)
        }
        
        view.addSubview(cancelBtn)
        cancelBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(16)
            make.width.height.equalTo(30)
        }
    }
}

extension PreviewViewController:UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
        let newLines = text.components(separatedBy: CharacterSet.newlines)
        let linesAfterChange = existingLines.count + newLines.count - 1
        return linesAfterChange <= textView.textContainer.maximumNumberOfLines
    }
    
    func textViewDidChange(_ textView: UITextView) {
        caption = textView.text
        mentionedUsers = []
        for word in textView.text.components(separatedBy: " "){
            if let range = word.range(of: "@") {
                let username = word[range.upperBound...]
                mentionedUsers.append(String(username))
            }
        }
        
        let lastWord = getLastWord(of: textView.text)
        if lastWord.hasPrefix("#"){
            if lastWord.isEmpty{
                removeHashtagsTableView()
                return
            }
            
            APIClient.searchForHashtags(keyword: lastWord) { (hashtags, error) in
                DispatchQueue.main.async {
                    self.hashtags = hashtags
                    self.hashtagsTableView.reloadData()
                    self.hashtagsTableViewHeightConstraint.update(offset: self.hashtagsTableView.contentSize.height)
                }
            }
            
        }else{
            removeHashtagsTableView()
        }
        
    }
    
    fileprivate func removeHashtagsTableView(){
        self.hashtags.removeAll()
        self.hashtagsTableView.reloadData()
        self.hashtagsTableViewHeightConstraint.update(offset: self.hashtagsTableView.contentSize.height)
    }
    
    fileprivate func getLastWord(of sentence: String) -> String{
        let split = sentence.split(separator: " ")
        let lastWord = String(split.suffix(1).joined(separator: [" "]))
        return lastWord
    }
}

extension PreviewViewController: TagViewControllerDelegate{
    func tagSelected(tag: String) {
        tagText = tag
    }
}

extension PreviewViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hashtags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
        }
        cell.textLabel?.text = hashtags[indexPath.row]
        cell.textLabel?.font = UIFont(name: "Raleway-Medium", size: 17)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        caption = removeLastWord(from: captionTextView.text)
        caption += " \(hashtags[indexPath.row]) "
        hashtags.removeAll()
        hashtagsTableView.reloadData()
        hashtagsTableViewHeightConstraint.update(offset: self.hashtagsTableView.contentSize.height)
    }
    
    func removeLastWord(from sentence: String) -> String{
        let myStringWithoutLastWord = sentence.components(separatedBy: " ").dropLast().joined(separator: " ")
        return myStringWithoutLastWord
    }
}


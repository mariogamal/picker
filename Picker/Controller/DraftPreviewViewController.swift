//
//  DraftPreviewViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/29/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import AVKit
import AVFoundation
import GrowingTextView
import SwiftRichString
import RealmSwift


protocol DraftPreviewViewControllerDelegate {
    func delete(url:String)
}

class DraftPreviewViewController: PreviewViewController{
    var previewDelegate:DraftPreviewViewControllerDelegate?
    
    lazy var deleteBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = .white
        btn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
        btn.addTargetClosure(closure: { (_) in
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: { (_) in
                self.dismiss(animated: true, completion: {
                    if self.pickedType == .image{
                        self.previewDelegate?.delete(url: self.imageUrl?.absoluteString ?? "")
                    }else{
                        self.videoPlayer?.pause()
                        self.previewDelegate?.delete(url: self.videoUrl?.absoluteString ?? "")
                    }
                    
                })
            })
            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            
        })
        return btn
    }()
    
    override func setupViews() {
        super.setupViews()
        view.addSubview(deleteBtn)
        deleteBtn.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(16)
            make.centerY.equalTo(sendBtn.snp.centerY)
            make.width.height.equalTo(35)
        }
        downloadBtn.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupViews()
        if captionTextView.text.count > 0{
            captionBtn.alpha = 0
            captionTextView.alpha = 1
        }
    }
    
    override func handleCancel() {
        if self.pickedType! == .video{
            self.videoPlayer?.pause()
            NotificationCenter.default.removeObserver(self.replayObserver ?? "")
            self.videoPlayer?.replaceCurrentItem(with: nil)
            self.dismiss(animated: false, completion:nil)
        }
        else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func uploadPicked() {
        sendIndicator.startAnimating()
        sendBtn.setImage(nil, for: .normal)
        let userId = UserDefaults.standard.integer(forKey: "userId")
        switch pickedType! {
        case .image:
            uploadPicked(userId: userId, url: imageUrl!, is_video: false, location: locationLabel.text ?? "", tag: tagLabel.text ?? "", caption: captionTextView.text ?? "", comments_are_public: commentsEnabled, mentioned_usernames: mentionedUsers)
        case.video:
            uploadPicked(userId: userId, url: videoUrl!, is_video: true, location: locationLabel.text ?? "", tag: tagLabel.text ?? "", caption: captionTextView.text ?? "", comments_are_public: commentsEnabled, mentioned_usernames: mentionedUsers)
        }
    }
    
    func uploadPicked(userId: Int, url: URL, is_video: Bool, location: String, tag: String, caption: String, comments_are_public: Bool, mentioned_usernames: [String]) {
        self.sendIndicator.startAnimating()
        self.sendBtn.setImage(nil, for: .normal)
        if is_video{
            /* Upload video picked */
            
            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
            compressVideo(inputURL: url as URL, outputURL: compressedURL) { (exportSession) in
                guard let session = exportSession else {
                    return
                }
                
                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = NSData(contentsOf: compressedURL) else {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        do{
                            let videoData = try Data(contentsOf: compressedURL)
                            let videoStr = videoData.base64EncodedString(options: .lineLength64Characters)
                            
                            guard let thumbImage = self.getThumbnailFrom(path: url) else {return}
                            guard let thumbData = thumbImage.jpegData(compressionQuality: 0.0) else {return}
                            let thumbStr = thumbData.base64EncodedString(options: .lineLength64Characters)
                            
                            APIClient.uploadPicked(userId: userId, photo: videoStr,thumb: thumbStr, is_video: "1" ,location: location, tag: tag, caption: caption, comments_are_public: comments_are_public , mentioned_usernames: mentioned_usernames) { (error) in
                                
                                DispatchQueue.main.async {
                                    self.sendIndicator.stopAnimating()
                                    self.sendBtn.setImage(#imageLiteral(resourceName: "ic_send"), for: .normal)
                                }
                                
                                if error != nil{
                                    let alert = UIAlertController(title: "Error".localized(), message:nil , preferredStyle: .alert)
                                    let ok = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                                    alert.addAction(ok)
                                    self.present(alert, animated: true, completion: nil)
                                    self.sendBtn.setImage(#imageLiteral(resourceName: "ic_send"), for: .normal )
                                    return
                                }
                                self.videoPlayer?.pause()
                                NotificationCenter.default.removeObserver(self.replayObserver ?? "")
                                self.videoPlayer?.replaceCurrentItem(with: nil)
                                self.previewDelegate?.delete(url: url.absoluteString)
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                        }catch{
                            print(error)
                        }
                        
                    }
                    
                    print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                case .failed:
                    break
                case .cancelled:
                    break
                }
            }
            
        }else{
            /* Upload image picked */
            
            do{
                let imageData = try Data(contentsOf: url)
                guard let image = UIImage(data: imageData) else {return}
                guard let imageDataCompressed = image.jpegData(compressionQuality: 0.0) else {return}
                
                let imageStr = imageDataCompressed.base64EncodedString(options: .lineLength64Characters)
                APIClient.uploadPicked(userId: userId, photo: imageStr, thumb:"" , is_video: "0" ,location: location, tag: tag, caption: caption, comments_are_public: comments_are_public , mentioned_usernames: mentioned_usernames) { (error) in
                    
                    DispatchQueue.main.async {
                        self.sendIndicator.stopAnimating()
                        self.sendBtn.setImage(#imageLiteral(resourceName: "ic_send"), for: .normal)
                    }
                    
                    
                    if error != nil{
                        let alert = UIAlertController(title: "Error".localized(), message:nil , preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
                        alert.addAction(ok)
                        self.present(alert, animated: true, completion: nil)
                        self.sendBtn.setImage(#imageLiteral(resourceName: "ic_send"), for: .normal )
                        return
                    }
                    self.videoPlayer?.pause()
                    self.previewDelegate?.delete(url: url.absoluteString)
                    self.dismiss(animated: true, completion: nil)
                    
                    
                    
                }
            }catch{
                print(error)
            }
        }
    }
    
    
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            print("Finish compress")
            handler(exportSession)
        }
    }
    
}




//
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        statusBar.isHidden = true
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        statusBar.isHidden = false
//        NotificationCenter.default.removeObserver(self)
//    }




//
//    let sendIndicator:UIActivityIndicatorView = {
//        let i = UIActivityIndicatorView()
//        i.style = .white
//        return i
//    }()




//    @objc func handleSend(){
//        sendIndicator.startAnimating()
//        sendBtn.setImage(nil, for: .normal)
//        let userId = UserDefaults.standard.integer(forKey: "userId")
//        switch pickedType! {
//        case .image:
//            //uploadImage(picked: picked)
//            uploadPicked(userId: userId, url: imageUrl!, is_video: false, location: locationLabel.text ?? "", tag: tagLabel.text ?? "", caption: captionTextView.text ?? "", comments_are_public: commentsEnabled, mentioned_usernames: mentionedUsers)
//        case.video:
//            uploadPicked(userId: userId, url: videoUrl!, is_video: true, location: locationLabel.text ?? "", tag: tagLabel.text ?? "", caption: captionTextView.text ?? "", comments_are_public: commentsEnabled, mentioned_usernames: mentionedUsers)
//        }
//
//    }
//
//
//











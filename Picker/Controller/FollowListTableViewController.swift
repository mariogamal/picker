//
//  FollowListTableViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/13/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

enum FollowListType{
    case following, followers
}

class FollowListTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var followListType:FollowListType?
    var userProfile = UserProfile2()
    var followList = [FollowListCellData]()
    var followers = [Follower]()
    private let listCellId = "listCellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .white
        setupViews()
        indicator.startAnimating()
        listTableView.register(FollowListTableViewCell.self, forCellReuseIdentifier: listCellId)
        
        switch followListType! {
        case .following:
            let userId = UserDefaults.standard.integer(forKey: "userId")
            
            APIClient.getFollowing(user_id: userId) { (followings, error) in
                if let err = error{
                    print(err)
                    return
                }
                
                if followings.count == 0{
                    self.indicator.stopAnimating()
                    self.noFollowLabel.text = "No following".localized()
                    self.addNoFollowLabel()
                    return
                }
                
                self.followers.append(contentsOf: followings)
                
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                    self.listTableView.reloadData()
                    self.addTableView()
                }
                print(followings)
            }

        case .followers:
            let userId = UserDefaults.standard.integer(forKey: "userId")
            
            APIClient.getFollowers(user_id: userId) { (followers, error) in
                if let err = error{
                    print(err)
                    return
                }
                
                if followers.count == 0{
                    self.indicator.stopAnimating()
                    self.noFollowLabel.text = "No followers".localized()
                    self.addNoFollowLabel()
                    return
                }
                
                self.followers.append(contentsOf: followers)
                
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                    self.listTableView.reloadData()
                    self.addTableView()
                }
                print(followers)
            }

            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    }
    
    let blueView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.blue
        return v
    }()
    
    lazy var listTableView:UITableView = {
        let tv = UITableView()
        tv.separatorStyle = .singleLine
        tv.separatorColor = AppColors.shared.superLightGray
        tv.delegate = self
        tv.dataSource = self
        return tv
    }()
    
    let titleLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    lazy var backBtn:UIButton = {
        let btn = UIButton()
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_right_36pt"), for: .normal)
        }else{
            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left_36pt"), for: .normal)
        }
        btn.tintColor = .white
        btn.addTargetClosure(closure: { (btn) in
            self.navigationController?.popViewController(animated: true)
        })
        return btn
    }()
    
    let indicator:UIActivityIndicatorView = {
        var i = UIActivityIndicatorView()
        i.style = .gray
        return i
    }()
    
    let noFollowLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label;
    }()
    
    
    func setupViews(){
        view.addSubview(blueView)
        blueView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    //iPhone X, XS
                    make.height.equalTo(88)
                    
                case 2688:
                    //iPhone XS Max
                    make.height.equalTo(88)
                    
                case 1792:
                    //iPhone XR
                    make.height.equalTo(88)
                    
                default:
                    make.height.equalTo(64)
                }
            }
        }
        
        
        blueView.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(8)
            make.height.width.equalTo(35)
        }
        
        blueView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(backBtn)
        }
        
        
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
    }
    
    func addTableView(){
        view.addSubview(listTableView)
        listTableView.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
    }
    
    func addNoFollowLabel(){
        view.addSubview(noFollowLabel)
        noFollowLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listTableView.dequeueReusableCell(withIdentifier: listCellId, for: indexPath) as! FollowListTableViewCell
        cell.configure(follower: followers[indexPath.row])
        cell.cellDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        //vc.userProfile2 = followList[indexPath.row].userProfile!
        vc.profile_user_id = followers[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
 
}


extension FollowListTableViewController: FollowListTableViewCellDelegate{
    
    func acceptBtnPressed(_ targetUserId: Int, cell:FollowListTableViewCell) {
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        if cell.acceptBtn.titleLabel?.text == "Accept".localized(){
            cell.acceptBtn.layer.borderColor = AppColors.shared.red.cgColor
            cell.acceptBtn.setTitleColor(AppColors.shared.red, for: .normal)
            cell.acceptBtn.setTitle("Unaccept".localized(), for: .normal)
        }else{
            cell.acceptBtn.layer.borderColor = AppColors.shared.blue.cgColor
            cell.acceptBtn.setTitleColor(AppColors.shared.blue, for: .normal)
            cell.acceptBtn.setTitle("Accept".localized(), for: .normal)
        }
        
        APIClient.acceptFollower(user_id: targetUserId, follow_id: userId) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
        
        
    }
    
    func followBtnPressed(_ targetUserId: Int, cell: FollowListTableViewCell) {
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        GlobalFunctions.addIndicatorToAllFollowButtons(profile_user_id: targetUserId, tabBarController: tabBarController)
        
//        if cell.followBtn.titleLabel?.text == "Follow".localized(){
//            cell.followBtn.backgroundColor = AppColors.shared.red
//            cell.followBtn.setTitle("Unfollow".localized(), for: .normal)
//        }else{
//            cell.followBtn.backgroundColor = AppColors.shared.blue
//            cell.followBtn.setTitle("Follow".localized(), for: .normal)
//        }
        
        APIClient.follow(user_id: userId, follow_id: targetUserId) { (error) in
            if let err = error{
                print(err)
                return
            }
            
            GlobalFunctions.updateFollowButtons(profile_user_id: targetUserId, tabBarController: self.tabBarController)
            GlobalFunctions.updateTimelinePicked(profile_user_id: userId, tabBarController: self.tabBarController)
        }
    }

}


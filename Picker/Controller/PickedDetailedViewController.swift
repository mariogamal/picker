//
//  PickedDetailedViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/8/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import Hero
import AVFoundation
import Photos


protocol loadMorePickedDelegate {
    func loadMorePicked()
}

protocol PickedDetailedViewControllerDelegate {
    func reloadCollectionData()
    func reload(index: IndexPath, user_id:Int?)
}

class PickedDetailedViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
   
    var delegate:PickedDetailedViewControllerDelegate?

    let pickedDetailedCellId = "pickedDetailedCellId"
    var pickedGroup = PickedGroup()
    var panGR: UIPanGestureRecognizer!
    var selectedIndexPath = IndexPath()
    var indexToScroll:IndexPath?
    var indexPath:IndexPath?
    var profileIndexPath:IndexPath?
    var isCurrentUser = false
    var viewIsAppears = false
    var playerLayer:AVPlayerLayer?
    var downloadService = DownloadService()
    var isFromComments = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        downloadService.downloadsSession = downloadsSession
        panGR = UIPanGestureRecognizer(target: self,action: #selector(handlePan(gestureRecognizer:)))
        view.addGestureRecognizer(panGR)
        setupCollectionView()
        CheckIfAlreadyDownloaded()
        CheckIfCurrentUser()
    }
    
    
    fileprivate func CheckIfCurrentUser() {
        let userId = UserDefaults.standard.integer(forKey: "userId")
        if userId == pickedGroup.user.id{
            isCurrentUser = true
        }
    }
    
    fileprivate func setupCollectionView(){
        collectionView?.register(UINib(nibName: "PickedDetailedCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: pickedDetailedCellId)
        collectionView?.isPagingEnabled = true
        collectionView?.bounces = false
        collectionView?.isScrollEnabled = false
    }
    
    fileprivate func CheckIfAlreadyDownloaded() {
        for (index, picked) in pickedGroup.picked_group.enumerated(){
            do {
                let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsPath, includingPropertiesForKeys: nil)
                fileURLs.forEach { (url) in
                    guard let savedFileName = url.absoluteString.components(separatedBy: "/").last else {return}
                    guard let pickedFileName = picked.url.components(separatedBy: "/").last else {return}
                    if savedFileName == pickedFileName{
                        pickedGroup.picked_group[index].downloadedUrl = url
                    }
                }
            } catch {
                print("Error while enumerating files \(documentsPath.path): \(error.localizedDescription)")
            }
            if picked.downloadedUrl == nil{
                self.downloadService.startDownload(picked)
            }
        }
    }
    
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436, 2688, 1792:
                //iPhone X, XS , XS max
                break
            default:
//                let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                UIApplication.shared.isStatusBarHidden = true
                break
            }
        }
        
        viewIsAppears = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        UIApplication.shared.isStatusBarHidden = true
        viewIsAppears = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let index = indexToScroll{
            self.collectionView?.scrollToItem(at: index, at: UICollectionView.ScrollPosition.right, animated: false)
        }
        
        if let cell = collectionView?.visibleCells.first as? PickedDetailedCollectionViewCell{
            if cell.pickedStruct.is_video{
                cell.player?.play()
            }
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as! PickedDetailedCollectionViewCell
        cell.player?.pause()
        cell.player?.seek(to: CMTime.zero)
        cell.player?.replaceCurrentItem(with: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.selectedIndexPath = indexPath

        //Play video when cell appears if it is already downloaded
        let cell = cell as! PickedDetailedCollectionViewCell
        
        
        if pickedGroup.picked_group[indexPath.item].is_video{
            
            if let downloadedUrl = pickedGroup.picked_group[indexPath.item].downloadedUrl{
                cell.player = AVPlayer(url: downloadedUrl)
                cell.playerLayer = AVPlayerLayer(player:cell.player)
                cell.playerLayer?.videoGravity = .resizeAspectFill
                cell.playerLayer?.frame = .init(x: 0, y: 0, width: view.frame.width, height: view.frame.width * 16/9)
                cell.videoView.layer.addSublayer(cell.playerLayer!)
                cell.player?.play()
                cell.progress.isHidden = true
            }
            
        }else{
            
            if let downloadedUrl = pickedGroup.picked_group[indexPath.item].downloadedUrl{
                cell.pickedImageView.kf.setImage(with: downloadedUrl)
                cell.progress.isHidden = true
            }
        }
        
        if !isCurrentUser{
            APIClient.viewPicked(pickd_id: pickedGroup.picked_group[indexPath.item].id)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickedGroup.picked_group.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pickedDetailedCellId, for: indexPath) as! PickedDetailedCollectionViewCell
        
        cell.usernameLabel.text = pickedGroup.user.name
        if let urlString = pickedGroup.user.profile_image{
            let url = URL(string: urlString)
            cell.profileImageView.kf.setImage(with: url)
        }
        
        cell.cellDelegate = self
        let url = URL(string: pickedGroup.picked_group[indexPath.row].url)!
        cell.configure(pickedStruct: pickedGroup.picked_group[indexPath.item], isCurrentUser: self.isCurrentUser, download: downloadService.activeDownloads[url])
        cell.countLabel.text = "     " + String( pickedGroup.picked_group.count - indexPath.item ) + "     "
        cell.hero.id = String(pickedGroup.picked_group.first?.id ?? 0)
        cell.countLabel.isHidden = isFromComments ? true : false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return view.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // Hero dismiss interactive animation
    @objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
        let translation = panGR.translation(in: nil)
        let progress = translation.y /  view.bounds.height/3
        let cell = self.collectionView?.cellForItem(at: selectedIndexPath) as! PickedDetailedCollectionViewCell
        print(progress)
        //if progress < 0 {return}
        switch panGR.state {
            
        case .began:
            // begin the transition as normal
            if cell.pickedStruct.is_video {
                cell.player?.pause()
            }
            myDismiss()
            
        case .changed:
            // calculate the progress based on how far the user moved
            if progress  <= 0.30 {
                Hero.shared.update(CGFloat(progress))
                let currentPos = CGPoint(x: (collectionView?.center.x)! , y: translation.y/3 + (collectionView?.center.y)!)
                Hero.shared.apply(modifiers: [.position(currentPos)], to: cell)
            }
        default:
            // end the transition when user ended their touch
            if progress  > 0.07 {
                Hero.shared.finish()
                cell.player?.pause()
            }else{
                if cell.pickedStruct.is_video {
                    cell.player?.play()
                }
                Hero.shared.cancel()
            }
            
        }
    }
    
    func myDismiss() {
        if let index = indexPath{
            self.delegate?.reload(index: index, user_id: pickedGroup.user.id)
        }
        self.dismiss(animated: true, completion: nil)
    }

}


extension PickedDetailedViewController:PickedDetailedCollectionViewCellDelegate{
    func profileTapped(userId: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        vc.hero.isEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func like(cell: PickedDetailedCollectionViewCell) {
        
    }
    
    func commentTapped(cell:PickedDetailedCollectionViewCell) {
        let vc = CommentsViewController()
        vc.pickedId = cell.pickedStruct.id
        vc.isCurrentUser = self.isCurrentUser
        vc.hero.isEnabled = true
        vc.delegate = self
        
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        
        if cell.pickedStruct.is_video{
            cell.player?.pause()
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func downloadTapped( cell: PickedDetailedCollectionViewCell) {
        
        if cell.pickedStruct.is_video{
            if let url = cell.pickedStruct.downloadedUrl{
                
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
                }, completionHandler: { succeeded, error in
                    
                    guard error == nil, succeeded else {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.showSaved()
                    }
                    
                })
            }
            
        }else{
            UIImageWriteToSavedPhotosAlbum(cell.pickedImageView.image!, nil, nil, nil)
            self.showSaved()
        }
    }
    
    func deleteTapped(cell: PickedDetailedCollectionViewCell) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        cell.player?.pause()
        
        let  deleteButton = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: { (action) -> Void in
            
            APIClient.deletePicked(picked_id: cell.pickedStruct.id, completion: { (error) in
                
                if let err = error{
                    print(err)
                    return
                }
                
                if self.pickedGroup.picked_group.count > 1{
                    let index = self.pickedGroup.picked_group.index(where: { (p) -> Bool in
                        p.id == cell.pickedStruct.id
                    })
                    self.pickedGroup.picked_group.remove(at: index!)
                    (self.collectionView?.deleteItems(at: [cell.indexPath!]))!
                    self.delegate?.reloadCollectionData()
                }else{
                    self.delegate?.reloadCollectionData()
                    self.dismiss(animated: true, completion: nil)
                }
                
            })
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action) -> Void in
            cell.player?.play()
        })
        
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func moreTapped(cell: PickedDetailedCollectionViewCell) {
        let alertController = UIAlertController(title: "Report spam".localized(), message: nil, preferredStyle: .alert)
        cell.player?.pause()
        
        
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Reason".localized()
        })
        
        let textField = alertController.textFields![0] as UITextField
        let  deleteButton = UIAlertAction(title: "Report".localized(), style: .destructive, handler: { (action) -> Void in
            
            let userId = UserDefaults.standard.integer(forKey: "userId")
            APIClient.reportSpam(user_id: userId, pickd_id: cell.pickedStruct.id, notes: textField.text ?? "", completion: { (error) in
                
                if let err = error{
                    print(err)
                    return
                }
                
                self.showMessage(body: "Reported".localized())
            })
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action) -> Void in
            cell.player?.play()
        })
        
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func rightTap(cell: PickedDetailedCollectionViewCell) {
        let nextIndex = IndexPath(item: selectedIndexPath.row + 1, section: 0)
        if nextIndex.row == pickedGroup.picked_group.count{
            cell.player?.pause()
            myDismiss()
            return
        }
        collectionView?.selectItem(at: nextIndex, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    func leftTap( cell: PickedDetailedCollectionViewCell) {
        let prevIndex = IndexPath(item: selectedIndexPath.row - 1 , section: 0)
        print(prevIndex.row)
        if prevIndex.row < 0 {
            if cell.pickedStruct.is_video{
                cell.player?.seek(to: CMTime.zero)
                cell.player?.play()
            }
            return
        }
        collectionView?.selectItem(at: prevIndex, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    func mentionTapped(username: String) {
        
        APIClient.getUserIdFromUsername(username: username) { (userId, error) in
            if let err = error{
                self.showMessageTop(body: err)
                return
            }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
            vc.profile_user_id = userId
            vc.hero.isEnabled = true
            
            if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
            }else{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            }
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func hashtagTapped(hashTag: String) {
        let vc = HashtagPicksViewController(hashtag: "#" + hashTag)
        let nav = UINavigationController(rootViewController: vc)
        nav.hero.isEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            nav.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            nav.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(nav, animated: true, completion: nil)
    }
}


extension PickedDetailedViewController:URLSessionDownloadDelegate{
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        
        let index = pickedGroup.picked_group.index { (picked: PickedStruct) -> Bool in
            sourceURL.absoluteString == picked.url
        }
      
        let destinationURL = localFilePath(for: sourceURL)
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        
        if let index = index{
            // Check if file is already stored 
            if pickedGroup.picked_group[index].downloadedUrl == nil{
                pickedGroup.picked_group[index].downloadedUrl = destinationURL
                DispatchQueue.main.async {
                    let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                    cell?.progress.isHidden = true
                    cell?.pickedStruct.downloadedUrl = destinationURL
                    if self.viewIsAppears{
                        if self.pickedGroup.picked_group[index].is_video {
                            cell?.player = AVPlayer(url: destinationURL)
                            cell?.playerLayer = AVPlayerLayer(player:cell?.player)
                            cell?.playerLayer?.videoGravity = .resizeAspectFill
                            cell?.playerLayer?.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.width * 16/9)
                            cell?.videoView.layer.addSublayer((cell?.playerLayer!)!)
                            cell?.player?.play()
                        }else{
                            cell?.pickedImageView.kf.setImage(with: destinationURL)
                        }
                    }
                    
                }
            }
        }
        
        
        print("finished downloading")
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        guard let url = downloadTask.originalRequest?.url,
            let download = downloadService.activeDownloads[url]  else { return }
       
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
       
        let index = pickedGroup.picked_group.index { (pick: PickedStruct) -> Bool in
            download.picked.id == pick.id
        }
        
        // Update progress bar
        DispatchQueue.main.async {
            if let index = index{
                let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                cell?.progress.progress = Double(download.progress)
            }
            
        }
        
    }
    
}

extension PickedDetailedViewController:CommentsViewControllerDelegate{
    func decreaseCommentsCountByOne(pickedId: Int) {
        let index = pickedGroup.picked_group.index { (pick: PickedStruct) -> Bool in
            pickedId == pick.id
        }
        
        DispatchQueue.main.async {
            if let index = index{
                let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                if let countString = cell?.commentsCountLabel.text{
                    if let count = Int(countString){
                        cell?.commentsCountLabel.text = String(count-1)
                    }
                }
            }
            
        }
    }
    
    func increaseCommentsCountByOne(pickedId: Int) {
        let index = pickedGroup.picked_group.index { (pick: PickedStruct) -> Bool in
            pickedId == pick.id
        }
    
        DispatchQueue.main.async {
            if let index = index{
                let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                if let countString = cell?.commentsCountLabel.text{
                    if let count = Int(countString){
                        cell?.commentsCountLabel.text = String(count+1)
                    }
                }
               
            }
            
        }
    }
}

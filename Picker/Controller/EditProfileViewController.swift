//
//  EditProfileViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/26/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

enum imageSelectedEnum{
    case cover, profile
}

protocol EditProfileViewControllerDelegate{
    func getUserProfile(userProfile:UserProfile)
}

class EditProfileViewController: UIViewController, UIScrollViewDelegate{

    var imageSelected:imageSelectedEnum?
    let datePicker = UIDatePicker()
    var profileDelegate:EditProfileViewControllerDelegate?
    var oldUsername = ""
    let indecator = UIActivityIndicatorView()
    
    var sevenYearsBefore: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .year, value: -7, to: Date(), options: [])!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        datePicker.backgroundColor = .white
        datePicker.maximumDate = sevenYearsBefore
        
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
    }
    
    var userProfile:UserProfile? {
        didSet{
            if let name = userProfile?.name{nameTF.text = name}
            if let username = userProfile?.username{usernameTF.text = username; oldUsername = username}
            if let bio = userProfile?.bio{bioTF.text = bio}
            
            if let birth = userProfile?.date_of_birth{birthTF.text = birth}
            
            if let profileImageUrl = userProfile?.profile_image{
                let profileUrl = URL(string:profileImageUrl)
                profileImageView.kf.setImage(with: profileUrl)
            }
            if let coverImageUrl = userProfile?.cover_image{
                let coverUrl = URL(string:coverImageUrl)
                coverImageView.kf.setImage(with: coverUrl)
            }
        }
    }
    
    //MARK:- Views setup
    let scrollView:UIScrollView = {
        let sv = UIScrollView()
        sv.alwaysBounceVertical = true
        return sv
    }()
    
    let contentView:UIView = {
        let v = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    let profileImageView:RoundedImageView = {
        let iv = RoundedImageView()
        iv.backgroundColor = AppColors.shared.lightGray
        iv.image = #imageLiteral(resourceName: "Profile_placeholder")
        return iv
    }()
    
    let coverImageView:UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = AppColors.shared.superLightGray
        iv.isUserInteractionEnabled = true
        return iv
    }()
    
    let overlay:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.2)
        v.isUserInteractionEnabled = true
        return v
    }()
    
    let curveImage:UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Profile_curve")
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    let nameTF:customTextField = {
        let tf = customTextField()
        tf.placeholder = "Name".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_person_outline_36pt"))
        return tf
    }()
    
    let usernameTF:customTextField = {
        let tf = customTextField()
        tf.placeholder = "Username".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_person_outline_36pt"))
        return tf
    }()
    
    let bioTF:customTextField = {
        let tf = customTextField()
        tf.placeholder = "Bio".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_priority_high_36pt"))
        return tf
    }()
    
    lazy var birthTF:customTextField = {
        let tf = customTextField()
        tf.inputView = datePicker
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_date_range_36pt"))
        tf.placeholder = "Date of birth".localized() + " " + "(Optional)".localized()
        return tf
    }()
    
    let editCoverBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("Edit cover".localized(), for: .normal)
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    let editProfileBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("Edit profile".localized(), for: .normal)
        btn.setTitleColor(AppColors.shared.blue, for: .normal)
        btn.addTarget(self, action: #selector(handleProfileImage), for: .touchUpInside)
        return btn
    }()
    
    let errorLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont(name: "Raleway-Medium", size: 12)
        label.textColor = AppColors.shared.red
        return label
    }()
    
    
  
    
   
    
   
    
    func setupViews(){
        navigationItem.titleView = editCoverBtn
        editCoverBtn.addTarget(self, action: #selector(handleCoverImage), for: .touchUpInside)
       
        view.backgroundColor = .white
        scrollView.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(handleDone))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(handleCancel))
        
        view.addSubview(coverImageView)
        coverImageView.addSubview(overlay)
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        scrollView.contentInset = UIEdgeInsets(top: 200 - 64, left: 0, bottom: 0, right: 0)
        
        
        contentView.addSubview(curveImage)
        contentView.addSubview(profileImageView)
        contentView.addSubview(editProfileBtn)
        contentView.addSubview(nameTF)
        contentView.addSubview(usernameTF)
        contentView.addSubview(bioTF)
        contentView.addSubview(birthTF)
        contentView.addSubview(errorLabel)
       
    
        
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.left.equalTo(view)
        }
        
        coverImageView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(200)
        }
        
        overlay.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        profileImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(-view.frame.width/7)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(view.frame.width/3.5)
        }
        
        editProfileBtn.snp.makeConstraints { (make) in
            make.top.equalTo(profileImageView.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
        }
        
        nameTF.snp.makeConstraints { (make) in
            make.top.equalTo(editProfileBtn.snp.bottom).offset(32)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        usernameTF.snp.makeConstraints { (make) in
            make.top.equalTo(nameTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        bioTF.snp.makeConstraints { (make) in
            make.top.equalTo(usernameTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
            
        }
        
        birthTF.snp.makeConstraints { (make) in
            make.top.equalTo(bioTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
            
        }
        
        errorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(birthTF.snp.bottom).offset(32)
            make.left.right.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().offset(-24)
        }
        
       
        
    }
   
    //MARK:- Functions
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        birthTF.text = dateFormatter.string(from: sender.date)
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scaleFactor = (-scrollView.contentOffset.y-200)
        if scaleFactor > -200{
            coverImageView.snp.removeConstraints()
            coverImageView.snp.makeConstraints { (make) in
                make.left.top.right.equalToSuperview()
                make.height.equalTo(view.frame.height/2.5 + scaleFactor)
            }
        }
        
        if scaleFactor < -85{
            navigationController?.navigationBar.isTranslucent = true
            if UIDevice().userInterfaceIdiom == .phone {
                if  UIScreen.main.nativeBounds.height ==  2436{
                    navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "iphone_X_top_overlay"), for: UIBarMetrics.default)
                }else{
                    navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "nav_overlay"), for: UIBarMetrics.default)
                }
            }
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.barTintColor = .clear
            
        }else{
            navigationController?.navigationBar.barTintColor = .clear
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            navigationController?.navigationBar.shadowImage = UIImage()
        }
    }
    
    func isValid(username: String) -> Bool {
        
        var returnValue = false
        let emailRegEx = "[^A-Z0-9a-z_]+"
        let countRegEx = "^[A-Z0-9a-z.-_]{5,20}$"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = username as NSString
            let results = regex.matches(in: username, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                let regex = try NSRegularExpression(pattern: countRegEx)
                let nsString = username as NSString
                let results2 = regex.matches(in: username, range: NSRange(location: 0, length: nsString.length))
                if results2.count > 0{
                    returnValue = true
                }
                
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    @objc func handleDone(){
        
        guard let name = nameTF.text, let username = usernameTF.text?.lowercased(), let bio = bioTF.text, let birth = birthTF.text else {return}
        
        if !isValid(username: username){
            errorLabel.text = "Username should be in english only and between 5-20 letters.".localized()
            return
        }
        
        if name == ""{
            errorLabel.text = "Name is required".localized()
            return
        }
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        var imageStr = ""
        
        if let image = profileImageView.image{
            if let imageData = image.jpegData(compressionQuality: 0.0){
                imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            }
        }
        
        var coverStr = ""
        
        if let image = coverImageView.image{
            if let imageData = image.jpegData(compressionQuality: 0.0){
                coverStr = imageData.base64EncodedString(options: .lineLength64Characters)
            }
        }
        
        showIndecator()
        APIClient.updateUserProfile(user_id: userId, username: username, name: name, date_of_birth: birth ,bio: bio , profile_image: imageStr, cover_image: coverStr) { (userProfile, error) in
            
            if let err = error{
                self.errorLabel.text = err
                self.hideIndecator()
            }else{
                self.hideIndecator()
                self.profileDelegate?.getUserProfile(userProfile: userProfile)
                self.dismiss(animated: true, completion: nil)
            }
            
            
            
        }
        
        
    }
    
    func showIndecator(){
        navigationItem.titleView = indecator
        indecator.startAnimating()
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        navigationItem.leftBarButtonItem?.isEnabled = false
    }
    
    func hideIndecator(){
        navigationItem.titleView = editCoverBtn
        indecator.stopAnimating()
        
        navigationItem.rightBarButtonItem?.isEnabled = true
        navigationItem.leftBarButtonItem?.isEnabled = true
    }
    
    @objc func handleCancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @objc func handleProfileImage(){
        imageSelected  = .profile
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func handleCoverImage(){
        imageSelected  = .cover
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }

}

extension EditProfileViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let editedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage{
            switch imageSelected!{
            case .profile:
                 profileImageView.image = editedImage
            case .cover:
                 coverImageView.image = editedImage
            }
           
        }
        else if let chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
                switch imageSelected!{
                case .profile:
                profileImageView.image = chosenImage
                case .cover:
                coverImageView.image = chosenImage
                }
        }
        dismiss(animated:true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}

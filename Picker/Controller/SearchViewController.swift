//
//  PostLineViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import Hero
class SearchViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UISearchBarDelegate{
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var usersCollectionView: UICollectionView!
    @IBOutlet weak var locationPicksCollectionView: UICollectionView!
    @IBOutlet weak var tagPicksCollectionView: UICollectionView!
    @IBOutlet weak var mostViewedCollectionView: UICollectionView!
    @IBOutlet weak var mostViewedIndicator: UIActivityIndicatorView!
    @IBOutlet weak var usersLabel: UILabel!
    @IBOutlet weak var locationsLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var mostViewdPicksLabel: UILabel!
    @IBOutlet weak var mostViewedPicksLabelWrapperView: UIView!
    @IBOutlet weak var usersLabelWrapperView: UIView!
    @IBOutlet weak var locationsLabelWrapperView: UIView!
    @IBOutlet weak var tagsLabelWrapperView: UIView!
    @IBOutlet weak var tagPicksCollectionViewHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var mostViewedPicksHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noPickedLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var refreshControl: UIRefreshControl!

    let usersHorizontalScrollingCellId = "usersHorizontalScrollingCellId"
    let pickedHorizontalScrollingCellId = "pickedHorizontalScrollingCellId"
    var usersList = [UserProfileCellData]()
    var searchTextIsEmpty = true
    var mostViewedPickedGroups = [PickedGroup]()
    var tmpMostViewedPickedGroups = [PickedGroup]()
    var users = [SearchUser](){
        didSet{
            if users.isEmpty{
                usersCollectionView.isHidden = true
                usersLabelWrapperView.isHidden = true
            }else{
                usersCollectionView.isHidden = false
                usersLabelWrapperView.isHidden = false
                usersCollectionView.reloadData()
            }
        }
    }
    var locationPicked = [PickedGroup](){
        didSet{
            if locationPicked.isEmpty{
                locationPicksCollectionView.isHidden = true
                locationsLabelWrapperView.isHidden = true
            }else{
                locationPicksCollectionView.isHidden = false
                locationsLabelWrapperView.isHidden = false
                locationPicksCollectionView.reloadData()
            }
        }
    }
    var tagPicked = [PickedGroup](){
        didSet{
            if tagPicked.isEmpty{
                tagPicksCollectionView.isHidden = true
                tagsLabelWrapperView.isHidden = true
            }else{
                tagPicksCollectionView.isHidden = false
                tagsLabelWrapperView.isHidden = false
                tagPicksCollectionView.reloadData()
                tagPicksCollectionViewHieghtConstraint.constant = tagPicksCollectionView.contentSize.height
            }
        }
    }
    var task:URLSessionTask?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        addTopNavigation()
        setTitle()
        setupSearchBar()
        setupCollectionViews()
        setupText()
        setUsersAndLocationAndTags(isHidden: true)
        navigationController?.isNavigationBarHidden = true
      
        let userId = UserDefaults.standard.integer(forKey: "userId")
        mostViewedIndicator.startAnimating()
        APIClient.getMostViewedPickedGroups(user_id: userId) { (pickedGroups, error) in
            self.mostViewedPickedGroups = pickedGroups
            DispatchQueue.main.async {
                self.mostViewedIndicator.stopAnimating()
                self.mostViewedCollectionView.reloadData()
                self.mostViewedPicksHeightConstraint.constant = self.mostViewedCollectionView.contentSize.height
            }
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        scrollView.insertSubview(refreshControl, at: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.mostViewedPicksHeightConstraint.constant = self.mostViewedCollectionView.contentSize.height
        self.tagPicksCollectionViewHieghtConstraint.constant = self.tagPicksCollectionView.contentSize.height
    }
    
    fileprivate func setMostView(isHidden: Bool){
        mostViewedCollectionView.isHidden = isHidden
        mostViewedPicksLabelWrapperView.isHidden = isHidden
    }
    
    fileprivate func setUsersAndLocationAndTags(isHidden: Bool){
        usersLabelWrapperView.isHidden = isHidden
        usersCollectionView.isHidden = isHidden
        locationsLabelWrapperView.isHidden = isHidden
        locationPicksCollectionView.isHidden = isHidden
        tagsLabelWrapperView.isHidden = isHidden
        tagPicksCollectionView.isHidden = isHidden
    }
    
    fileprivate func setupText(){
        tagsLabel.text = "TAGS".localized()
        locationsLabel.text = "LOCATIONS".localized()
        usersLabel.text = "USERS".localized()
        noPickedLabel.text = "There are no picks related to your search.".localized()
    }
    
    fileprivate func setupSearchBar() {
        searchBar.tintColor = AppColors.shared.blue
        searchBar.placeholder = "Search".localized()
        searchBar.barTintColor = .white
        searchBar.backgroundColor = .white
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
    }
    
    fileprivate func setTitle() {
        viewTitle.text = "Search".localized()
    }
    
    fileprivate func setupCollectionViews() {
        usersCollectionView.register(SearchUserCollectionViewCell.self, forCellWithReuseIdentifier: "SearchUserCollectionViewCell")
        locationPicksCollectionView.register(SearchPickedCollectionViewCell.self, forCellWithReuseIdentifier: "SearchPickedCollectionViewCell")
        tagPicksCollectionView.register(SearchPickedCollectionViewCell.self, forCellWithReuseIdentifier: "SearchPickedCollectionViewCell")
        mostViewedCollectionView.register(SearchPickedCollectionViewCell.self, forCellWithReuseIdentifier: "SearchPickedCollectionViewCell")
    }

    lazy var indicator: UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        i.style = .gray
        return i
    }()
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    
    @objc func hideKeyboard(){
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    
    fileprivate func addTopNavigation(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                //iPhone X, XS
                topViewHeightConstraint.constant = 88
                
            case 2688:
                //iPhone XS Max
                topViewHeightConstraint.constant = 88
                
            case 1792:
                //iPhone XR
                topViewHeightConstraint.constant = 88
                
            default:
                topViewHeightConstraint.constant = 64
            }
        }
    }
    
    func setupViews(){
        searchBar.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().inset(16)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        task?.cancel()
        indicator.startAnimating()
        
        
        if searchText == ""{
            /*
            * clear all data when search text is empty, And show most viewed picks
            */
            users.removeAll()
            tagPicked.removeAll()
            locationPicked.removeAll()
            indicator.stopAnimating()
            searchTextIsEmpty = true
            noPickedLabel.isHidden = true
            setMostView(isHidden: false)
            scrollView.insertSubview(refreshControl, at: 0)
            return
        }else{
            refreshControl.removeFromSuperview()
            setMostView(isHidden: true)
            searchTextIsEmpty = false
        }
        makeRequest(keyword: searchText)
    }
    
    fileprivate func makeRequest(keyword: String){
        let userId = UserDefaults.standard.integer(forKey: "userId")
        let request = try! APIRouter.search(user_id: userId, keyword: keyword).asURLRequest()
        task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse{
                if httpResponse.statusCode == 200{
                    DispatchQueue.main.async {
                        self.indicator.stopAnimating()
                    }
                    do{
                        guard let data = data else {return}
                        let searchResult = try JSONDecoder().decode(SearchResult.self, from: data)
                        print(searchResult)
                        DispatchQueue.main.async {
                            self.users = searchResult.users
                            self.tagPicked = searchResult.tag_picked
                            self.locationPicked = searchResult.location_picked
                            if searchResult.users.isEmpty &&
                                searchResult.tag_picked.isEmpty &&
                                searchResult.location_picked.isEmpty{
                                self.noPickedLabel.isHidden = false
                            }else{
                                self.noPickedLabel.isHidden = true
                            }
                        }
                    }
                    catch{
                        print(error)
                    }
                }
            }
        })
        task?.resume()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.text = nil
        users.removeAll()
        tagPicked.removeAll()
        locationPicked.removeAll()
        indicator.stopAnimating()
        searchTextIsEmpty = true
        noPickedLabel.isHidden = true
        setMostView(isHidden: false)
        scrollView.insertSubview(refreshControl, at: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case usersCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchUserCollectionViewCell", for: indexPath) as! SearchUserCollectionViewCell
            cell.user = users[indexPath.item]
            cell.delegate = self
            return cell
        case tagPicksCollectionView:
            let pickedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchPickedCollectionViewCell", for: indexPath) as! SearchPickedCollectionViewCell
            pickedCell.configure(pickedGroup: tagPicked[indexPath.item])
            pickedCell.user = tagPicked[indexPath.item].user
            pickedCell.searchDelegate = self
            pickedCell.thumbImage.layer.borderWidth = 0
            return pickedCell
        case locationPicksCollectionView:
            let pickedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchPickedCollectionViewCell", for: indexPath) as! SearchPickedCollectionViewCell
            pickedCell.configure(pickedGroup: locationPicked[indexPath.item])
            pickedCell.user = locationPicked[indexPath.item].user
            pickedCell.searchDelegate = self
            pickedCell.thumbImage.layer.borderWidth = 0
            return pickedCell
        case mostViewedCollectionView:
            let pickedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchPickedCollectionViewCell", for: indexPath) as! SearchPickedCollectionViewCell
            pickedCell.configure(pickedGroup: mostViewedPickedGroups[indexPath.item])
            pickedCell.user = mostViewedPickedGroups[indexPath.item].user
            pickedCell.searchDelegate = self
            pickedCell.thumbImage.layer.borderWidth = 0
            return pickedCell
        default:
            return UICollectionViewCell()
        }
        
    }
    
    @objc func openProfile(tap: UITapGestureRecognizer){
        guard let userId = tap.view?.tag else{return}
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case usersCollectionView:
            return users.count
        case locationPicksCollectionView:
            return locationPicked.count
        case tagPicksCollectionView:
            return tagPicked.count
        case mostViewedCollectionView:
            return mostViewedPickedGroups.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case usersCollectionView:
            return CGSize(width: 130, height: 180)
        case locationPicksCollectionView:
            return CGSize(width: 175, height: 300)
        case tagPicksCollectionView, mostViewedCollectionView:
            return CGSize(width: view.frame.width/2 - 24, height: (view.frame.width/2 - 24) + 150)
        default:
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch collectionView {
        case usersCollectionView,locationPicksCollectionView, tagPicksCollectionView:
            return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 10)
        case mostViewedCollectionView:
            return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        default:
            return UIEdgeInsets.zero
        }
        
    }
    
    
    fileprivate func userSelected(user_id: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = user_id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case usersCollectionView:
            userSelected(user_id: users[indexPath.row].id)
        case locationPicksCollectionView:
            pickedGroupSelected(pickedGroup: locationPicked[indexPath.row])
        case tagPicksCollectionView:
            pickedGroupSelected(pickedGroup: tagPicked[indexPath.row])
        case mostViewedCollectionView:
            pickedGroupSelected(pickedGroup: mostViewedPickedGroups[indexPath.row])
        default:
            break
        }
    }
    
    func pickedGroupSelected(pickedGroup: PickedGroup) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let vc = PickedDetailedViewController(collectionViewLayout: layout)
        vc.hero.isEnabled = true
        vc.pickedGroup = pickedGroup
        vc.collectionView?.reloadData()
        present(vc, animated: true, completion: nil)
    }
    
    @objc func onRefresh() {
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.getMostViewedPickedGroups(user_id: userId) { (pickedGroups, error) in
            self.mostViewedPickedGroups = pickedGroups
            print(pickedGroups)
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.mostViewedCollectionView.reloadData()
                self.mostViewedPicksHeightConstraint.constant = self.mostViewedCollectionView.contentSize.height
            }
        }
    }
    
}

extension SearchViewController: SearchPickedCollectionViewCellDelegate{
    func profileSelected(userId: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SearchViewController:SearchUserDelegate{
    func followBtnPressed( targetUserId: Int, cell: SearchUserCollectionViewCell) {
        let userId = UserDefaults.standard.integer(forKey: "userId")
        GlobalFunctions.addIndicatorToAllFollowButtons(profile_user_id: targetUserId, tabBarController: self.tabBarController)
        
        APIClient.follow(user_id: userId, follow_id: targetUserId) { (error) in
            
            if let err = error{
                print(err)
                return
            }
            
            GlobalFunctions.updateFollowButtons(profile_user_id: targetUserId, tabBarController: self.tabBarController)
            GlobalFunctions.updateTimelinePicked(profile_user_id: userId, tabBarController: self.tabBarController)
        }
    }
}

//
//  TestViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController{
    
    private let refreshControl = UIRefreshControl()
    private let followCellId = "followCellId"
    private let commentCellId = "commentCellId"
    private let pickedCellId = "pickedCellId"
    private let replyCellId = "replyCellId"
    var notifications = [NotificationStruct]()
    var didLoad = false
    private var counterForUpdate = 0
    private var isRefreshing = false
    private var isLoading = false
//    private var currentKey = ""
    
    var deepLinkNotificationId: Int?
    var deepLinkNotification: NotificationStruct?
    
    var activityIndicator = UIActivityIndicatorView()
    let effectView = UIView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name("reloadData"), object: nil)
        view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = true
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        refreshControl.tintColor = AppColors.shared.blue
        indicator.startAnimating()
        setupViews()
        
        tableView.separatorStyle = .none
        tableView.register(NotificationFollowTableViewCell.self, forCellReuseIdentifier: followCellId)
        tableView.register(NotificationCommentTableViewCell.self, forCellReuseIdentifier: commentCellId)
        tableView.register(NotificationPickedTableViewCell.self, forCellReuseIdentifier: pickedCellId)
        tableView.register(NotificationReplyTableViewCell.self, forCellReuseIdentifier: replyCellId)
        didLoad = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.items?[3].badgeValue = nil
        
        /**
         * When loading notification view controller from tab bar
         * Check refresher end refreshing
         * Stop activity indicator
         */
        if !isRefreshing{
            indicator.stopAnimating()
        }
        
    
        if let notification = deepLinkNotification {
            deepLinkNotification = nil
            
            switch notification.type {
            case "follow":
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
                vc.profile_user_id = notification.user_id
                self.navigationController?.pushViewController(vc, animated: true)
            case "comment":
                
                let vc = CommentsViewController()

                if let pickedId = notification.picked_id{
                    vc.pickedId = pickedId
                }
                if let commentId = notification.comment_id{
                    vc.commentIdToScrollTo = commentId
                }
                if let pickedUserId = notification.picked_user_id{
                    if pickedUserId == UserDefaults.standard.integer(forKey: "userId"){
                        vc.isCurrentUser = true
                    }else{
                        vc.isCurrentUser = false
                    }
                }
                vc.hero.isEnabled = true
                if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                    vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
                }else{
                    vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
                }
                self.present(vc, animated: true, completion: nil)
                
            case "reply":
                let vc = CommentsViewController()
                
                if let pickedId = notification.picked_id{
                    vc.pickedId = pickedId
                }
                if let commentId = notification.comment_id{
                    vc.commentIdToScrollTo = commentId
                }
                if let pickedUserId = notification.picked_user_id{
                    if pickedUserId == UserDefaults.standard.integer(forKey: "userId"){
                        vc.isCurrentUser = true
                    }else{
                        vc.isCurrentUser = false
                    }
                }
                vc.isNotificationReply = true
                vc.hero.isEnabled = true
                if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                    vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
                }else{
                    vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
                }
                self.present(vc, animated: true, completion: nil)
                
                
            case "picked":
                
                let userId = UserDefaults.standard.integer(forKey: "userId")
                guard let picked_group_id = notification.picked_group_id else {return}
                guard let picked_id = notification.picked_id else {return}
                
                APIClient.getPickedGroup(user_id: userId, picked_group_id: picked_group_id) { (pickedGroup, error) in
                    
                    if let err = error{
                        print(err)
                        self.showMessageTop(body: err)
                        return
                    }
                    
                    let layout = UICollectionViewFlowLayout()
                    layout.scrollDirection = .horizontal
                    let vc = PickedDetailedViewController(collectionViewLayout: layout)
                    vc.hero.isEnabled = true
                    vc.hero.modalAnimationType = .selectBy(presenting: .cover(direction: .up), dismissing: .uncover(direction: .down))
                    vc.pickedGroup = pickedGroup
                    for (index,picked) in (pickedGroup.picked_group.enumerated()){
                        if picked.id == picked_id{
                            let indexPath = IndexPath(row: index, section: 0)
                            vc.indexToScroll = indexPath
                        }
                    }
                    vc.isCurrentUser = false
                    
                    //  The entire picked group exists put picked does not exists
                    if vc.indexToScroll == nil{
                        self.showMessageTop(body: "Picked does not exists".localized())
                    }else{
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                }
                
            default:
                break
            }
        }
        
    }
    
    
    @objc func handleRefresh(){
        if !isRefreshing{
            loadData()
            isRefreshing = true
        }
    }
    
    func incrementBadgeByOne(){
        if let badgeValue = self.tabBarController?.tabBar.items?[3].badgeValue,
            let value = Int(badgeValue) {
            self.tabBarController?.tabBar.items?[3].badgeValue = String(value + 1)
        } else {
            self.tabBarController?.tabBar.items?[3].badgeValue = "1"
        }
    }
    
    
    func loadDeepLink(deepLinkId: Int){
        APIClient.tapNotification(notification_id: deepLinkId) { (error) in
            
            if let err = error{
                print(err)
                return
            }
            
            self.loadData()
        }
    }
    
    // This function is called from tab bar controller
    @objc func loadData(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.isLoading = true
        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        
        APIClient.getUserNotifications(user_id: userId) { (notifications, error) in
            
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if let err = error{
                print(err)
                return
            }
            
            self.notifications.removeAll()
            self.notifications.append(contentsOf: notifications)
            
            if notifications.count == 0 {
                self.tableView.addSubview(self.noNotificationLabel)
                self.noNotificationLabel.snp.makeConstraints({ (make) in
                    make.center.equalToSuperview()
                })
            }else{
                self.noNotificationLabel.removeFromSuperview()
            }
            
        
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.indicator.stopAnimating()
                self.isRefreshing = false
                self.refreshControl.endRefreshing()
            }
            
            
            /* Badge count number */
            self.tabBarController?.tabBar.items?[3].badgeValue = nil
            for (notification) in notifications{
                if !notification.is_seen{
                    self.incrementBadgeByOne()
                }
            }
            
            APIClient.seeNotification(user_id: userId) { (error) in
                if let err = error{
                    print(err)
                    return
                }
            }
            
            print(notifications)
        }

    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .clear
    }
    

    
    let blueView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.blue
        return v
    }()
    
    let viewTitle:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = .white
        label.text = "Notifications".localized()
        return label
    }()
    
    lazy var tableView:UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        return tv
    }()
    
    let indicator:UIActivityIndicatorView = {
        var i = UIActivityIndicatorView()
        i.style = .gray
        return i
    }()
    
    let noNotificationLabel:UILabel = {
        let label = UILabel()
        label.text = "No Notifications.".localized()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    
    
    func setupViews(){
        view.addSubview(blueView)
        blueView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    print("iPhone X, XS")
                    make.height.equalTo(88)
                    
                case 2688:
                    print("iPhone XS Max")
                    make.height.equalTo(88)
                    
                case 1792:
                    print("iPhone XR")
                    make.height.equalTo(88)
                    
                default:
                    make.height.equalTo(64)
                }
            }

        }
        
        blueView.addSubview(viewTitle)
        viewTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(12)
        }
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
        
    }
    
    
    
    func showActivityIndicator() {
    
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
 
        effectView.frame = CGRect(x: view.frame.midX - 23, y: view.frame.midY - 23, width: 46, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        effectView.backgroundColor = AppColors.shared.blue
        effectView.alpha = 0.8
        
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.frame = CGRect(x: view.frame.midX - 23, y: view.frame.midY - 23, width: 46, height: 46)
        
        activityIndicator.startAnimating()
        
        view.addSubview(effectView)
        view.addSubview(activityIndicator)
    }
    
    func hideActivityIndicator(){
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
    }
    
    
    
    
}

extension NotificationViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch notifications[indexPath.row].type{
        case "follow":
            let cell = tableView.dequeueReusableCell(withIdentifier: followCellId, for: indexPath) as! NotificationFollowTableViewCell
            cell.notificationFollowDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        case "comment":
            let cell = tableView.dequeueReusableCell(withIdentifier: commentCellId, for: indexPath) as! NotificationCommentTableViewCell
            cell.notificationCommentDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        case "picked":
            let cell = tableView.dequeueReusableCell(withIdentifier: pickedCellId, for: indexPath) as! NotificationPickedTableViewCell
            cell.notificationPickedDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        case "reply":
            let cell = tableView.dequeueReusableCell(withIdentifier: replyCellId, for: indexPath) as! NotificationReplyTableViewCell
            cell.notificationReplyDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            APIClient.deleteNotification(notification_id: notifications[indexPath.row].id) { (error) in
                
                if let err = error{
                    print(err)
                    return
                }
            }
            
            self.notifications.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        
//        if let notificationId = self.deepLinkNotificationId{
//            let index = notifications.index(where: { (n) -> Bool in
//                n.id == notificationId
//            })
//            self.tableView.selectRow(at: IndexPath(row: index!, section: 0), animated: true, scrollPosition: UITableViewScrollPosition.top)
//        }
//    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastSectionIndex = tableView.numberOfSections - 1
//        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
//        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
//            // print("this is the last cell")
//            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//            spinner.startAnimating()
//            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//
//            self.listTableView.tableFooterView = spinner
//            self.listTableView.tableFooterView?.isHidden = false
//            loadMore()
//        }
//    }
    

}

extension NotificationViewController:NotificationFollowTableViewCellDelegate{
    
    func profileTapped(cell: NotificationFollowTableViewCell) {
        
        cell.backgroundColor = .white
        APIClient.tapNotification(notification_id: cell.notification.id) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func acceptBtnPressed(targetUserId: Int, cell:NotificationFollowTableViewCell) {
        
        if cell.acceptBtn.titleLabel?.text == "Accept".localized(){
            cell.acceptBtn.layer.borderColor = AppColors.shared.red.cgColor
            cell.acceptBtn.setTitleColor(AppColors.shared.red, for: .normal)
            cell.acceptBtn.setTitle("Unaccept".localized(), for: .normal)
        }else{
            cell.acceptBtn.layer.borderColor = AppColors.shared.blue.cgColor
            cell.acceptBtn.setTitleColor(AppColors.shared.blue, for: .normal)
            cell.acceptBtn.setTitle("Accept".localized(), for: .normal)
        }
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.acceptFollower(user_id: targetUserId, follow_id: userId) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
    }
    
    func followBtnPressed(targetUserId: Int, cell: NotificationFollowTableViewCell) {
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        GlobalFunctions.addIndicatorToAllFollowButtons(profile_user_id: targetUserId, tabBarController: tabBarController)
        APIClient.follow(user_id: userId, follow_id: targetUserId) { (error) in
            if let err = error{
                print(err)
                return
            }
            
            GlobalFunctions.updateFollowButtons(profile_user_id: targetUserId, tabBarController: self.tabBarController)
            GlobalFunctions.updateTimelinePicked(profile_user_id: userId, tabBarController: self.tabBarController)
        }
    }
    
}


extension NotificationViewController:NotificationCommentTableViewCellDelegate{
    
    func commentTapped(cell: NotificationCommentTableViewCell) {
        
        cell.backgroundColor = .white
        APIClient.tapNotification(notification_id: cell.notification.id) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
        
        let vc = CommentsViewController()
        if let pickedId = cell.notification?.picked_id{
             vc.pickedId = pickedId
        }
        if let commentId = cell.notification?.comment_id{
            vc.commentIdToScrollTo = commentId
        }
        
        if let pickedUserId = cell.notification?.picked_user_id{
            if pickedUserId == UserDefaults.standard.integer(forKey: "userId"){
                vc.isCurrentUser = true
            }else{
                vc.isCurrentUser = false
            }
        }
        

        vc.hero.isEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
             vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
             vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
     
    func profileTapped(cell: NotificationCommentTableViewCell) {
        cell.backgroundColor = .white
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}


extension NotificationViewController:NotificationReplyTableViewCellDelegate{
    func profileTapped(cell: NotificationReplyTableViewCell) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func commentTapped(cell: NotificationReplyTableViewCell) {
        
        cell.backgroundColor = .white
        APIClient.tapNotification(notification_id: cell.notification.id) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
        
        let vc = CommentsViewController()
        if let pickedId = cell.notification?.picked_id{
            vc.pickedId = pickedId
        }
        if let commentId = cell.notification?.comment_id{
            vc.commentIdToScrollTo = commentId
        }
        if let pickedUserId = cell.notification?.picked_user_id{
            if pickedUserId == UserDefaults.standard.integer(forKey: "userId"){
                vc.isCurrentUser = true
            }else{
                vc.isCurrentUser = false
            }
        }
        
        vc.isNotificationReply = true
        vc.hero.isEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
}


extension NotificationViewController: NotificationPickedTableViewCellDelegate{
    
    func profileTapped(cell: NotificationPickedTableViewCell) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func commentTapped(cell: NotificationPickedTableViewCell) {
        cell.backgroundColor = .white
        APIClient.tapNotification(notification_id: cell.notification.id) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
        
        Spinner.shared.startAnimating()
        let userId = UserDefaults.standard.integer(forKey: "userId")
        guard let picked_group_id = cell.notification?.picked_group_id else {return}
        guard let picked_id = cell.notification?.picked_id else {return}
        
        APIClient.getPickedGroup(user_id: userId, picked_group_id: picked_group_id) { (pickedGroup, error) in
            
            DispatchQueue.main.async {
                Spinner.shared.stopAnimating()
            }
            
            if let err = error{
                print(err)
                self.showMessageTop(body: err)
                return
            }
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            let vc = PickedDetailedViewController(collectionViewLayout: layout)
            vc.hero.isEnabled = true
            vc.hero.modalAnimationType = .selectBy(presenting: .cover(direction: .up), dismissing: .uncover(direction: .down))
            vc.pickedGroup = pickedGroup
            for (index,picked) in (pickedGroup.picked_group.enumerated()){
                if picked.id == picked_id{
                    let indexPath = IndexPath(row: index, section: 0)
                    vc.indexToScroll = indexPath
                }
            }
            vc.isCurrentUser = false
            
            //  The entire picked group exists put picked does not exists
            if vc.indexToScroll == nil{
                self.showMessageTop(body: "Picked does not exists".localized())
            }else{
               self.present(vc, animated: true, completion: nil)
            }
            
            print(pickedGroup)
        }
        
    }
    
    
}





//extension NotificationViewController2: UIScrollViewDelegate{
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == self.listTableView {
//            if scrollView.contentOffset.y < -110 {
//                scrollView.scrollRectToVisible(CGRect(origin: CGPoint(x: 0, y: -110), size: scrollView.frame.size), animated: false)
//                scrollView.scrollRectToVisible(CGRect(origin: CGPoint.zero, size: scrollView.frame.size), animated: true)
//            }
//        }
//    }
//}

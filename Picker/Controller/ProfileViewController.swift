//
//  ViewController.swift
//  TB_TwitterHeader
//
//  Created by Yari D'areglia on 08/12/2016.

import UIKit
import Kingfisher
import OneSignal
import RealmSwift
import Hero

let offset_HeaderStop:CGFloat = 40.0 // At this offset the Header stops its transformations
let offset_B_LabelHeader:CGFloat = 95.0  // At this offset the Black label reaches the Header
let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label

protocol ProfileViewControllerDelegate {
    func followTapped(isFollow: Bool, userId: Int)
}
class ProfileViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var avatarImage:UIImageView!
    @IBOutlet var header:UIView!
    @IBOutlet var headerLabel:UILabel!
    @IBOutlet var headerImageView:UIImageView!
    @IBOutlet var headerBlurImageView:UIImageView!
    var blurredHeaderImageView:UIImageView?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var draftBtn: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var avatarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var headerLabelTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var editFollowBtn: TWTButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var followIndicator: UIActivityIndicatorView!
    @IBOutlet weak var moreIndicator: UIActivityIndicatorView!
    @IBOutlet weak var moreBtn: UIButton!
    
    var panGR: UIPanGestureRecognizer!
    var delegate: ProfileViewControllerDelegate?
    
    
    private let refreshControl = UIRefreshControl()
    let profilePickedCellId = "profilePickedCellId"
    var pickedGroups = [PickedGroup]()
    var locked  = true
    var isCurrentUser = false
    var isTabBarProfile = false
    var profile_user_id = 0
    var current_user_id = 0
   
    
//    let downloadService = DownloadService()
//
//    lazy var downloadsSession: URLSession = {
//        let configuration = URLSessionConfiguration.default
//        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
//    }()
//
    
    var userProfile = UserProfile()
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
   
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadProfileData), name: NSNotification.Name("reloadData"), object: nil)
        
//        downloadService.downloadsSession = downloadsSession
        
        messageLabel.text = ""
        messageLabel.font = UIFont(name: "Raleway-Bold", size: 18)
        nameLabel.text = ""
        usernameLabel.text = ""
        headerLabel.text = ""
        bioLabel.text = ""
        
        current_user_id = UserDefaults.standard.integer(forKey: "userId")
        
        if isTabBarProfile{
            backBtn.isHidden = true
            profile_user_id = current_user_id
        }else{
            backBtn.addTargetClosure(closure: { (_) in
                self.removeHeroIhFromCellsBeforeDismiss()
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            })
        }
        
        // CURRENT USER
        if current_user_id == profile_user_id {
            isCurrentUser = true
        }
        
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            backBtn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_right_36pt"), for: .normal)
        }
        
        if isCurrentUser{
            draftBtn.alpha = 1
            followersLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFollowers)))
            followingLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFollowing)))
        }
        
        panGR = UIPanGestureRecognizer(target: self,action: #selector(handlePan(gestureRecognizer:)))
        view.addGestureRecognizer(panGR)
        
        //downloadService.downloadsSession = downloadsSession
        view.clipsToBounds = true
        header.clipsToBounds = true
        scrollView.delegate = self
        collectionView.register(ProfilePickedCollectionViewCell.self, forCellWithReuseIdentifier: profilePickedCellId)
        
        navigationController?.isNavigationBarHidden = true
        
    
        loadProfileData()
        updateIPhoneXLayout()
        setupHeader()
        setupRefrehser()
        setupUserProfile()
        
  
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let realm = try! Realm()
        let result = realm.objects(DraftPicked.self)
        if result.count > 0 {
            self.draftBtn.tintColor = AppColors.shared.blue
        }else{
            self.draftBtn.tintColor = .darkGray
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadProfileData()
    }
    
    fileprivate func removeHeroIhFromCellsBeforeDismiss(){
        for cell in collectionView.visibleCells as! [ProfilePickedCollectionViewCell]{
            cell.thumbImage.hero.id = ""
        }
    }
    func setupUserProfile(){
//        nameLabel.text = userProfile.name
//        headerLabel.text = userProfile.name
//        usernameLabel.text = "@" + (userProfile.username)
//        bioLabel.text = userProfile.bio
//
//        let profileUrl = URL(string: userProfile.profileImageUrl)
//        avatarImage.kf.setImage(with: profileUrl)
//
//        let coverUrl = URL(string: userProfile.coverImageUrl)
//        headerImageView.kf.setImage(with: coverUrl)
//        headerImageView.kf.setImage(with: coverUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, nil, cache, url) in
//            self.headerBlurImageView?.image = image?.blurredImage(withRadius: 10, iterations: 20, tintColor: UIColor.clear)
//
//        })
        
    }
    
    // Hero dismiss interactive animation
    @objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
        let translation = panGR.translation(in: nil)
        var progress = CGFloat()
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            progress = -translation.x / view.bounds.width
        }else{
            progress = translation.x / view.bounds.width
        }
        
        
        switch panGR.state {
            
        case .began:
            removeHeroIhFromCellsBeforeDismiss()
            dismiss(animated: true, completion: nil)
            
        case .changed:
            // calculate the progress based on how far the user moved
            Hero.shared.update(CGFloat(progress))
        default:
            // end the transition when user ended their touch
            if progress  > 0.25 {
                Hero.shared.finish()
            }else{
                Hero.shared.cancel()
            }
        }
    }
    
    
    func updateIPhoneXLayout(){
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                //iPhone X, XS
                break
                
            case 2688:
                //iPhone XS Max
                break
                
            case 1792:
                //iPhone XR
                break
                
            default:
                headerHeight.constant = headerHeight.constant - 24
                avatarTopConstraint.constant = avatarTopConstraint.constant - 24
                headerLabelTopConstraint.constant = headerLabelTopConstraint.constant - 24
                view.layoutIfNeeded()
            }
        }
        
        
    }
    
    func setupHeader(){
        // Header - Image
        
        headerImageView = UIImageView(frame: header.bounds)
        headerImageView?.contentMode = UIView.ContentMode.scaleAspectFill
        header.insertSubview(headerImageView, belowSubview: headerLabel)
        
        // Header - Blurred Image
        
        headerBlurImageView = UIImageView(frame: header.bounds)
        headerBlurImageView?.contentMode = UIView.ContentMode.scaleAspectFill
        headerBlurImageView?.alpha = 0.0
        header.insertSubview(headerBlurImageView, belowSubview: headerLabel)
        
    }
    
    func setupRefrehser(){
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        scrollView.refreshControl = refreshControl
    }
    
    @objc func handleRefresh(){
       loadProfileData()
    }
    
    @IBAction func draftPressed(_ sender: Any) {
        let vc = DraftViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
//    func downloadPickedGroups(){
//        // Download picked if its not downloaded
//        for pickedGroup in pickedGroups{
//            for picked in pickedGroup.picked_group{
//                if picked.downloadedUrl == nil{
//                    self.downloadService.startDownload(picked)
//                }
//            }
//        }
//    }
    
    @IBAction func morePressed(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)
        alertController.addAction(cancelAction)
        
        if isCurrentUser{
            let logoutAction = UIAlertAction(title: "Logout".localized(), style: .destructive) { (action) in
                self.logout()
            }
            
            let blockedUsersAction = UIAlertAction(title: "Blocked Users".localized(), style: .default) { (action) in
                let vc = BlockedUsersViewController()
                self.present(vc, animated: true, completion: nil)
            }
            
            let changePasswordAction = UIAlertAction(title: "Change password".localized(), style: .default) { (action) in
                let vc = ChangePasswordViewController()
                let nav = UINavigationController(rootViewController: vc)
                self.present(nav, animated: true, completion: nil)
            }
             
            alertController.addAction(blockedUsersAction)
            alertController.addAction(changePasswordAction)
            alertController.addAction(logoutAction)
            present(alertController, animated: true, completion: nil)
        }else{
            
            guard let isBlocked = userProfile.is_blocked else {return}
            let userId = UserDefaults.standard.integer(forKey: "userId")
            let blockAction = UIAlertAction(title: isBlocked ? "Unblock".localized() : "Block".localized(), style: .destructive) { (action) in
                APIClient.blockUser(user_id: userId, blocked_id: self.userProfile.id, completion: { (error) in
                    if let err = error{
                        print(err)
                        return
                    }
                    GlobalFunctions.updateTimelinePicked(profile_user_id: userId, tabBarController: self.tabBarController)
                    GlobalFunctions.updateUserProfile(profile_user_id: userId, tabBarController: self.tabBarController)
                    self.loadProfileData()
                })
            }
            
            alertController.addAction(blockAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    @objc func logout(){
        if let token = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId{
            APIClient.logout(token: token, completion: { (error) in
                if let err = error{
                    print(err)
                    return
                }
                
                UserDefaults.standard.set(0, forKey: "userId")
                OneSignal.setSubscription(false)
                
                let loginVC = LoginViewController()
                loginVC.hero.isEnabled = true
                loginVC.hero.modalAnimationType = .fade
                self.tabBarController?.hero.replaceViewController(with: loginVC)
            })
        }
    }
    
    
    @objc func loadProfileData(){
        
        if !Reachability.isConnectedToNetwork(){
            showMessageTop(body: "No internet connection".localized())
            return
        }
    
        APIClient.getUserProfile(user_id: current_user_id, target_user_id: profile_user_id) { (userProfile, error) in
            
            if let err = error {
                print(err)
                return
            }
            
            if let isBlocked = userProfile.is_blocked{
                if isBlocked{
                    /**
                    * User is blocked
                    */
                    
                    self.userProfile = userProfile
                    self.nameLabel.text = userProfile.name
                    self.headerLabel.text = userProfile.name
                    self.usernameLabel.text = "@" + userProfile.username
                    self.bioLabel.text = userProfile.bio
                    
                    
                    if let profileImage = userProfile.profile_image{
                        if let url = URL(string: profileImage){
                            self.avatarImage.kf.setImage(with: url)
                        }
                    }
                    
                    if let coverImage = userProfile.cover_image{
                        if let url = URL(string: coverImage){
                            self.headerImageView.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, nil, cache, url) in
                                self.headerBlurImageView?.image = image?.blurredImage(withRadius: 10, iterations: 20, tintColor: UIColor.clear)
                            })
                        }
                    }
                    self.editFollowBtn.setTitle("Blocked".localized(), for: .normal)
                    self.editFollowBtn.isUserInteractionEnabled = false
                    self.activityIndicator.stopAnimating()
                    self.messageLabel.text = "User not found".localized()
                    self.pickedGroups.removeAll()
                    self.collectionView.reloadData()
                    return
                }
            }
                /**
                 * User not blocked
                 */
            
                self.userProfile = userProfile
        
                self.followingLabel.text = String(userProfile.following_count) + " " + "Following".localized()
                self.followersLabel.text = String(userProfile.followers_count) + " " + "Followers".localized()
                self.nameLabel.text = userProfile.name
                self.headerLabel.text = userProfile.name
                self.usernameLabel.text = "@" + userProfile.username
                self.bioLabel.text = userProfile.bio
                self.messageLabel.text = ""
        
                if let profileImage = userProfile.profile_image{
                    if let url = URL(string: profileImage){
                        self.avatarImage.kf.setImage(with: url)
                    }
                }
        
                if let coverImage = userProfile.cover_image{
                    if let url = URL(string: coverImage){
                        self.headerImageView.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, nil, cache, url) in
                            self.headerBlurImageView?.image = image?.blurredImage(withRadius: 10, iterations: 20, tintColor: UIColor.clear)
                        })
                    }
                }
        
                if self.isCurrentUser{
                    self.editFollowBtn.setTitle("Edit".localized(), for: .normal)
                    self.editFollowBtn.addTarget(self, action: #selector(self.handleEdit), for: .touchUpInside)
                }else{
                    if let is_follow = userProfile.is_follow{
                        self.editFollowBtn.addTarget(self, action: #selector(self.handleFollowUnfollow), for: .touchUpInside)
                        if is_follow == true{
                            self.editFollowBtn.setTitle("Unfollow".localized(), for: .normal)
                        }else{
                            self.editFollowBtn.setTitle("Follow".localized(), for: .normal)
                            
                        }
                    }
                    
                }
            
            
            
        
        
                APIClient.getUserPicked(user_id: self.current_user_id, target_user_id: self.profile_user_id) { (pickedGroups, error) in
                    
                    
                    if let err = error {
                        print(err)
                        return
                    }
                    self.pickedGroups.removeAll()
                    self.pickedGroups.append(contentsOf: pickedGroups)
                    
                    //self.downloadPickedGroups()
                    
                    if pickedGroups.count == 0{
                        self.messageLabel.text = "There are no picked.".localized()
                    }else{
                        self.messageLabel.text = ""
                    }
                    
                    DispatchQueue.main.async {
                        self.reloadCollectionView()
                    }
                    
                }
        
            }
            
        
    }
    
    
    func reloadCollectionView(){
        self.activityIndicator.stopAnimating()
        self.collectionView.reloadData()
        let lineSpacing = CGFloat(32)
        let rowHeight = (view.frame.width/2 - 24) + lineSpacing  + 117 // 8+20+4 + 12+25+48
        let numberOfLines = CGFloat((pickedGroups.count+1)/2)
        let offset = CGFloat(16)
        self.collectionViewHeight.constant = rowHeight * numberOfLines + offset
        self.collectionView.layoutIfNeeded()
        self.locked = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        print(offset)
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            header.layer.transform = headerTransform
        }
            
        // SCROLL UP/DOWN ------------
            
        else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
            
            //  ------------ Label
            
            let labelTransform = CATransform3DMakeTranslation(0, max(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0)
            headerLabel.layer.transform = labelTransform
            
            //  ------------ Blur
            
            headerBlurImageView?.alpha = min (1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader)
            
            // Avatar -----------
            
            let avatarScaleFactor = (min(offset_HeaderStop+24, offset)) / avatarImage.bounds.height * 1.1 // Slow down the animation
            let avatarSizeVariation = ((avatarImage.bounds.height * (1.0 + avatarScaleFactor)) - avatarImage.bounds.height) / 2.0
            avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
            avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
            
            if offset <= offset_HeaderStop {
                
                if avatarImage.layer.zPosition < header.layer.zPosition{
                    header.layer.zPosition = 0
                }
                
            }else {
                if avatarImage.layer.zPosition >= header.layer.zPosition{
                    header.layer.zPosition = 2
                    backBtn.layer.zPosition = 3
                }
            }
        }
        
        // Apply Transformations
        
        header.layer.transform = headerTransform
        avatarImage.layer.transform = avatarTransform
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if refreshControl.isRefreshing{
            self.refreshControl.endRefreshing()
        }
    }
    
    
    @objc func handleEdit(){
        let vc = EditProfileViewController()
        vc.userProfile = self.userProfile
        vc.profileDelegate = self
        let nav = UINavigationController(rootViewController: vc)
        self.navigationController?.present(nav, animated: true, completion: nil)
        
    }
    
    @objc func handleFollowUnfollow(){
        
        if editFollowBtn.titleLabel?.text == "Follow".localized(){
            // Handle follow
            handleFollow()
        }else if editFollowBtn.titleLabel?.text == "Unfollow".localized(){
            // Handle Unfollow
            handleUnfollow()
        }
        
  
    }
    
    @objc func handleFollow(){
        
//        self.followIndicator.startAnimating()
//        self.editFollowBtn.setTitle("", for: .normal)
        
        GlobalFunctions.addIndicatorToAllFollowButtons(profile_user_id: profile_user_id, tabBarController: tabBarController)
        APIClient.follow(user_id: current_user_id, follow_id: profile_user_id) { (error) in
            
            if let err = error{
                print(err)
                self.editFollowBtn.setTitle("Follow".localized(), for: .normal)
                return
            }
            
            GlobalFunctions.updateFollowButtons(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
            GlobalFunctions.updateTimelinePicked(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
            self.followIndicator.stopAnimating()
            self.editFollowBtn.setTitle("Unfollow".localized(), for: .normal)
            //            self.delegate?.followTapped(isFollow: true, userId: self.profile_user_id)
            //
            //            self.editFollowBtn.removeTarget(self, action:  #selector(self.handleFollow), for: .touchUpInside)
            //            self.editFollowBtn.addTarget(self, action: #selector(self.handleUnfollow), for: .touchUpInside)
            
        }
    }
    
    @objc func handleUnfollow(){
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let unfollow = UIAlertAction(title: "Unfollow".localized(), style: .destructive, handler: { (_) in
            //self.followIndicator.startAnimating()
            //self.editFollowBtn.setTitle("", for: .normal)
            GlobalFunctions.addIndicatorToAllFollowButtons(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
            
            APIClient.follow(user_id: self.current_user_id, follow_id: self.profile_user_id) { (error) in
                
                if let err = error{
                    print(err)
                    self.editFollowBtn.setTitle("Unfollow".localized(), for: .normal)
                    return
                }
                
                
                GlobalFunctions.updateFollowButtons(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
                GlobalFunctions.updateTimelinePicked(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
//                self.followIndicator.stopAnimating()
//                self.delegate?.followTapped(isFollow: false, userId: self.profile_user_id)
//                self.editFollowBtn.setTitle("Follow".localized(), for: .normal)
//
//                self.editFollowBtn.removeTarget(self, action:  #selector(self.handleUnfollow), for: .touchUpInside)
//                self.editFollowBtn.addTarget(self, action: #selector(self.handleFollow), for: .touchUpInside)
            }

            
            
        })
        let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
        alertController.addAction(unfollow)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
        
   
    }
    
    @objc func showFollowers(){
        let vc = FollowListTableViewController()
        vc.titleLabel.text = "Followers".localized()
        vc.followListType = .followers
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func showFollowing(){
        let vc = FollowListTableViewController()
        vc.titleLabel.text = "Following".localized()
        vc.followListType = .following
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}



extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    //MARK:- COLLECTION VIEW METHODS
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickedGroups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2 - 24, height: (view.frame.width/2 - 24) + 108)// 12+16+48+8+20+4)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let profilePickedCell = collectionView.dequeueReusableCell(withReuseIdentifier: profilePickedCellId, for: indexPath) as! ProfilePickedCollectionViewCell
        profilePickedCell.delegate = self
        profilePickedCell.thumbImage.layer.borderWidth = 0
        profilePickedCell.configure(pickedGroup: pickedGroups[indexPath.item])
        if !isCurrentUser{
            profilePickedCell.bookmarkBtn.isHidden = true
        }
        
        return profilePickedCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 32
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 50, right: 16 )
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let vc = PickedDetailedViewController(collectionViewLayout: layout)
        vc.delegate = self
        vc.hero.isEnabled = true
        vc.pickedGroup = pickedGroups[indexPath.item]
        vc.indexPath = indexPath
        vc.isCurrentUser = self.isCurrentUser
        vc.collectionView?.reloadData()
        present(vc, animated: true, completion: nil)
        
    }
  
}


extension ProfileViewController:PickedDetailedViewControllerDelegate{
    
    func reload(index: IndexPath, user_id: Int?) {
        
    }
    
    func reloadCollectionData() {
        loadProfileData()
    }
}


extension ProfileViewController:EditProfileViewControllerDelegate{
    func getUserProfile(userProfile: UserProfile) {
        self.usernameLabel.text = "@" + userProfile.username
        self.bioLabel.text = userProfile.bio
        self.nameLabel.text = userProfile.name
        
        if let profileImage = userProfile.profile_image{
            if let url = URL(string: profileImage){
                self.avatarImage.kf.setImage(with: url)
            }
        }
        
        if let coverImage = userProfile.cover_image{
            if let url = URL(string: coverImage){
                self.headerImageView.kf.setImage(with: url)
            }
        }
        
        self.userProfile = userProfile
        
        //self.setupUserProfile()
    }
}

extension ProfileViewController:ProfilePickedCollectionViewCellDelegate{
    
    func bookmarkPressed(cell: ProfilePickedCollectionViewCell) {
        
        if cell.bookmarkBtn.tintColor == AppColors.shared.lightGray{
            cell.bookmarkBtn.tintColor = AppColors.shared.yellow
            
            APIClient.savePickedGroup(picked_group_id: cell.pickedGroup.picked_group_id, completion: { (error) in
                if let err = error{
                    print(err)
                    return
                }
                
                self.showMessage(body: "Saved".localized())
            })
           
        }else{
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let unsave = UIAlertAction(title: "Unsave".localized(), style: .destructive, handler: { (_) in
                cell.bookmarkBtn.tintColor = AppColors.shared.lightGray
                APIClient.savePickedGroup(picked_group_id: cell.pickedGroup.picked_group_id, completion: { (error) in
                    if let err = error{
                        print(err)
                        return
                    }
                })
            })
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
            alertController.addAction(unsave)
            alertController.addAction(cancel)
            present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
}


//extension ProfileViewController:URLSessionDownloadDelegate{
//
//    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
//        guard let sourceURL = downloadTask.originalRequest?.url else { return }
//        for i in 0..<pickedGroups.count{
//            for j in 0..<pickedGroups[i].picked_group.count{
//                if pickedGroups[i].picked_group[j].url == sourceURL.absoluteString{
//                    let destinationURL = localFilePath(for: sourceURL)
//                    print(destinationURL)
//                    // 3
//                    let fileManager = FileManager.default
//                    try? fileManager.removeItem(at: destinationURL)
//                    do {
//                        try fileManager.copyItem(at: location, to: destinationURL)
//                    } catch let error {
//                        print("Could not copy file to disk: \(error.localizedDescription)")
//                    }
//                    pickedGroups[i].picked_group[j].downloadedUrl = destinationURL
//                }
//            }
//        }
//        print("finished downloading")
//        print(location)
//    }
//
//    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
//        print(Float(totalBytesWritten) / Float(totalBytesExpectedToWrite) * 100)
//    }
//
//}


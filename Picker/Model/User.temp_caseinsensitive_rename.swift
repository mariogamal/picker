//
//  user.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/26/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

class User{
    var id:String?
    var name:String?
    var username:String?
    var dateOfBirth:String?
    var profileImageUrl:String?
}

//
//  SearchResult.swift
//  Picker
//
//  Created by omar mohammed on 6/22/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct SearchResult: Codable {
    var users:[SearchUser] = []
    var tag_picked:[PickedGroup] = []
    var location_picked:[PickedGroup] = []
}

struct SearchUser: Codable {
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var profile_image: String?
    var is_follow: Bool = false
}

//
//  PickedGroupTagBased.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/15/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

struct PickedGroup: Codable {
    var picked_group_id: Int = 0
    var user : UserShort = UserShort()
    var is_saved: Bool = false
    var picked_group: [PickedStruct] = []
}

struct PickedStruct: Codable {
    var id: Int = 0
    var is_video: Bool = false
    var location: String = ""
    var url: String = ""
    var thumb_url: String?
    var tag: String = ""
    var created_at: String?
    var updated_at: String?
    var readable_time: String?
    var caption:String?
    var comments_are_public: Bool = true
    var comments_count: Int = 0
    var user_id: Int = 0
    var picked_group_id: Int = 0
    var downloadedUrl:URL?
    var view_count: Int = 0
    var is_liked: Bool = false
    var likes_count: Int = 0
}


struct UserShort: Codable {
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var profile_image: String?
}

//
//  user.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/26/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

class UserProfile2{
    var id:String = ""
    var name:String = ""
    var username:String = ""
    var email:String = ""
    var bio:String = ""
    var dateOfBirth:String = ""
    var profileImageUrl:String = ""
    var coverImageUrl:String = ""
    var publicPicked:Int = 0
    var privatePicked:Int = 0
    var followingCount:Int = 0
    var followersCount:Int = 0
    var tokens:[String] = []
    
    init() {
    }
    
    init(name:String, username:String, datheOfBirth:String, profileImageUrl:String, bio:String) {
        self.name = name
        self.username = username
        self.dateOfBirth = datheOfBirth
        self.profileImageUrl = profileImageUrl
        self.bio = bio
    }
    
    init(json:[String:Any]) {
        self.id = json["id"] as! String
        self.name = json["name"] as! String
        self.username = json["username"] as! String
        self.email = json["email"] as! String
        self.dateOfBirth = json["dateOfBirth"] as! String
        self.profileImageUrl = json["profileImageUrl"] as! String
        self.coverImageUrl = json["coverImageUrl"] as! String
        self.bio = json["bio"] as! String
        self.publicPicked = json["publicPicked"] as! Int
        self.privatePicked = json["privatePicked"] as! Int
        if let tokens = json["tokens"] {
            for token in tokens as! [String:Any]{
                self.tokens.append(token.key)
            }
        }
        
        if let followingCount = json["followingCount"] as? Int{
            self.followingCount = followingCount
        }
        
        if let followersCount = json["followersCount"] as? Int{
            self.followersCount = followersCount
        }
        
    }
    
    func toDictionary() -> [String:Any]{
        
        return [ "id" : self.id,
                 "name" : self.name,
                 "username" : self.username,
                 "email" : self.email,
                 "dateOfBirth" : self.dateOfBirth,
                 "profileImageUrl" : self.profileImageUrl,
                 "coverImageUrl" : self.coverImageUrl,
                 "bio" : self.bio,
                "publicPicked" : self.publicPicked,
                "privatePicked" : self.privatePicked,
                "followingCount" : self.followingCount,
                "followersCount" : self.followersCount
            ]
    }
}

class UserProfileCellData {
    var userProfile:UserProfile2?
    var isFollow:Bool?
    var isBlockedMe:Bool?
}


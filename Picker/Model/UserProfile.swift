//
//  UserProfile.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 6/20/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct UserProfile:Codable{
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var bio: String?
    var profile_image: String?
    var cover_image: String?
    var email: String = ""
    var date_of_birth: String = ""
    var following_count: Int = 0
    var followers_count: Int = 0
    var is_blocked: Bool?
    var is_follow: Bool?
}

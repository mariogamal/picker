//
//  follower.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 6/21/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct Follower: Codable {
    var id: Int = 0
    var username: String = ""
    var profile_image:String?
    var is_follow: Bool = false
    var is_accepted: Bool?
}
